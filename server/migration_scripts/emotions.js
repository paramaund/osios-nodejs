import Emotion from '../models/emotion';
import async from 'async';

export default ()=> {
  return new Promise((resolve, reject)=> {
    async.each(
      ['happy', 'ok', 'unhappy'],
      (emotion, callback)=> {
        Emotion.create({name: emotion}, (err)=> {
          if (err) {
            return callback(err);
          }
          callback();
        });
      },
      error=> {
        if (error) {
          return reject(error);
        }
        resolve();
      }
    );
  });
};
