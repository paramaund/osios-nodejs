import mysql from 'mysql';
import async from 'async';
import {map, parseInt} from 'lodash';
import User from '../models/user';
import Role from '../models/role';
import PaymentPlan from '../models/payment_plan';

export default ()=> {
  return new Promise((resolve, reject)=> {
    let connection = mysql.createConnection({
      host: 'localhost',
      port: 3307,
      user: 'root',
      password: 'root',
      database: 'osios_new_wrdp59'
    });

    function createUser(userData, index, usersLength, role, callback) {
      let user = Object.assign({}, userData);
      let currentUser = {
        email: user.user_email,
        password: user.user_pass,
        role: role
      };
      connection.query(`select * from wp_usermeta where user_id = ?`, [user.ID], (err2, data)=> {
        if (err2) {
          return callback(err2);
        }
        let result = [...data];
        let firstName = result.find(el=>el.meta_key === 'first_name');
        if (firstName && firstName.meta_value) {
          currentUser.first_name = firstName.meta_value;
        }

        let lastName = result.find(el=>el.meta_key === 'last_name');
        if (lastName && lastName.meta_value) {
          currentUser.last_name = lastName.meta_value;
        }

        let access = result.find(el=>el.meta_key === 'user_access');
        if (access && access.meta_value) {
          currentUser.access = access.meta_value === 'YES';
        }

        let avatar = result.find(el=>el.meta_key === 'avatar');
        if (avatar && avatar.meta_value) {
          currentUser.avatar = avatar.meta_value;
        }

        let companyName = result.find(el=>el.meta_key === 'company_name');
        if (companyName && companyName.meta_value) {
          currentUser.company_name = companyName.meta_value;
        }

        let companyLogo = result.find(el=>el.meta_key === 'company_logo');
        if (companyLogo && companyLogo.meta_value) {
          currentUser.company_logo = companyLogo.meta_value;
        }

        let timezone = result.find(el=>el.meta_key === 'timezone');
        if (timezone && timezone.meta_value) {
          currentUser.timezone = timezone.meta_value;
        }

        let phone = result.find(el=>el.meta_key === 'telephone');
        if (phone && phone.meta_value) {
          currentUser.phone = phone.meta_value;
        }

        let skype = result.find(el=>el.meta_key === 'skype');
        if (skype && skype.meta_value) {
          currentUser.skype = skype.meta_value;
        }

        currentUser.moxtra_unique_id = 'OSIOS' + user.ID;

        let notifications = result.find(el=>el.meta_key === 'notifications');
        if (notifications && notifications.meta_value) {
          currentUser.notifications = map(notifications.meta_value.split(','), parseInt);
        }

        let stripeProfile = result.find(el=>el.meta_key === 'stripe_profile');
        if (stripeProfile && stripeProfile.meta_value) {
          currentUser.stripe_profile = stripeProfile.meta_value;
        }

        let recurringProfile = result.find(el=>el.meta_key === 'recurring_profile');
        if (recurringProfile && recurringProfile.meta_value) {
          currentUser.payment_profile = recurringProfile.meta_value;
        }

        let paymentPlan = result.find(el=>el.meta_key === 'recurring_plan');
        if (paymentPlan && paymentPlan.meta_value) {
          currentUser.payment_plan = paymentPlan.meta_value;
        }

        let paymentExpiry = result.find(el=>el.meta_key === 'trial_expiry');
        if (paymentExpiry && paymentExpiry.meta_value) {
          currentUser.payment_expiry = paymentExpiry.meta_value;
        }

        let createdAt = result.find(el=>el.meta_key === 'account_join');
        if (createdAt && createdAt.meta_value) {
          currentUser.created_at = new Date(createdAt.meta_value);
        }

        async.waterfall([
            callback2=> {
              if (!currentUser.payment_plan) {
                return callback2(null, null);
              }
              PaymentPlan.findOneAndUpdate({key: currentUser.payment_plan}, {
                  $set: {
                    name: currentUser.payment_plan,
                    key: currentUser.payment_plan,
                    isActive: true
                  }
                }, {new: true, upsert: true})
                .exec((error, plan)=> {
                  if (error) {
                    return callback2(error);
                  }
                  callback2(null, plan);
                });
            },
            (plan, callback2)=> {
              currentUser.payment_plan = plan;
              User.create(currentUser, (userErr) => {
                if (userErr) {
                  return callback2(userErr);
                }
                callback2();
              });
            }
          ],
          error=> {
            if (error) {
              return callback(error);
            }
            console.log('users done: ' + (index + 1) + '/' + usersLength);
            callback();
          }
        );
      });
    }

    connection.connect();

    connection.query('SELECT DISTINCT * from wp_users', (err, users) => {
      if (err) {
        return reject(err);
      }

      Role.findOne({name: 'user'}, (roleErr, role) => {
        if (roleErr) {
          return reject(roleErr);
        }
        if (!role) {
          return reject(new Error({
            message: 'Please set up \'user\' role first'
          }));
        }
        let usersLength = users.length;
        async.forEachOf(
          users,
          (el, index, callback) => {
            let data = Object.assign({}, el);
            createUser(data, index, usersLength, role, callback);
          },
          (asyncErr) => {
            if (asyncErr) {
              return reject(asyncErr);
            }
            connection.end();
            resolve();
          });
      });
    });
  });
};
