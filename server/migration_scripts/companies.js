import mysql from 'mysql';
import async from 'async';
import User from '../models/user';
import Company from '../models/company';

export default ()=> {
  return new Promise((resolve, reject)=> {
    let connection = mysql.createConnection({
      host: 'localhost',
      port: 3307,
      user: 'root',
      password: 'root',
      database: 'osios_new_wrdp59'
    });

    function createCompanyByOwner(user, index, callback) {
      // let user = Object.assign({}, userData);
      console.log('user index - ' + index);
      connection.query(`
                SELECT DISTINCT oc.id, company_name as name
                FROM wp_osios_companies as oc, wp_users as u
                WHERE user_email = ? and u.id = oc.owner_id`, [user.email], (compErr, data) => {
        if (compErr) {
          return callback(compErr);
        }
        let companies = [...data];
        console.log('companies count - ' + companies.length);
        async.map(
          companies,
          (companyData, cb) => {
            let company = Object.assign({}, companyData);
            if (!company.name) {
              return cb();
            }
            let currentCompany = {
              name: company.name,
              owner: user
            };
            connection.query(`
                SELECT DISTINCT user_email as email
                FROM wp_osios_company_users as ocu, wp_users as u
                WHERE company_id = ? and u.id = ocu.user_id`, [company.id], (usCompErr, compUsersData) => {
              if (usCompErr) {
                return cb(usCompErr);
              }
              let compUsers = [...compUsersData];
              if (compUsers.length === 0) {
                cb(null, currentCompany);
              } else {
                User.find({email: {$in: compUsers.map(el=>el.email)}})
                  .exec((err, users)=> {
                    if (err) {
                      console.error(err);
                      return cb(err);
                    }
                    currentCompany.users = users;
                    cb(null, currentCompany);
                  });
              }
            });
          },
          (err, resData) => {
            let results = resData.filter(el=>el);
            if (err) {
              return callback(err);
            }

            async.each(
              results,
              (elem, cb)=> {
                let company = Object.assign({}, elem);
                Company.create(company, (compErr2) => {
                  if (compErr2) {
                    return cb(compErr2);
                  }
                  cb();
                });
              },
              (err3)=> {
                if (err3) {
                  return callback(err3);
                }
                callback();
              }
            );
          }
        );
      });
    }

    connection.connect();

    User.find({})
      .exec((userErr, users)=> {
        if (userErr) {
          return reject(userErr);
        }
        console.log('users count - ' + users.length);
        async.forEachOf(
          users,
          createCompanyByOwner,
          (err)=> {
            if (err) {
              return reject(err);
            }
            console.log('companies done!');
            connection.end();
            resolve();
          }
        );
      });
  });
};


