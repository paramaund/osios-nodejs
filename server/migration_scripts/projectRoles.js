import mysql from 'mysql';
import async from 'async';
import ProjectRole from '../models/project_role';

export default (req, res, next)=> {
  return new Promise((resolve, reject)=> {
    let connection = mysql.createConnection({
      host: 'localhost',
      port: 3307,
      user: 'root',
      password: 'root',
      database: 'osios_new_wrdp59'
    });

    connection.connect();

    connection.query('SELECT * from wp_osios_user_roles', (error, rows) => {
      if (error) {
        return reject(error);
      }
      async.each(
        rows,
        (row, callback)=> {
          ProjectRole.create({name: row.role_name}, (err)=> {
            if (err) {
              return callback(err);
            }
            callback();
          });
        },
        error=> {
          if (error) {
            return reject(error);
          }
          connection.end();
          resolve();
        }
      );
    });
  });
};
