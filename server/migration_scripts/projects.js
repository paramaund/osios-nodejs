import mysql from 'mysql';
import async from 'async';
import User from '../models/user';
import Emotion from '../models/emotion';
import ProjectStatus from '../models/project_status';
import ProjectRole from '../models/project_role';
import Project from '../models/project';
import Room from '../models/room';
import RoomPermission from '../models/room_permission';
import Platform from '../models/platform';
import Device from '../models/device';
import head from 'lodash/head';

export default (req, res, next)=> {
  return new Promise((resolve, reject)=> {
    let connection = mysql.createConnection({
      host: 'localhost',
      port: 3307,
      user: 'root',
      password: 'root',
      database: 'osios_new_wrdp59'
    });

    function findRoomOrder(roomId, userId, cb) {
      connection.query(`
            select room_order
            from wp_osios_project_room_order as opro
            where opro.room_id = ? and opro.user_id = ?`, [roomId, userId], (err, resData)=> {
        if (err) {
          console.error(err);
          return cb(err);
        }
        let results = [...resData];
        if (results.length > 0) {
          cb(null, results[0].room_order);
        } else {
          cb(null, 1);
        }
      });
    }

    function createRoom(roomData1, callback) {
      let roomData = Object.assign({}, roomData1);
      async.waterfall(
        [
          (cb1)=> {
            User.findOne({email: roomData.email})
              .exec((err2, user)=> {
                if (err2) {
                  console.error(err2);
                  return cb1(err2);
                }
                cb1(null, user);
              });
          },
          (user, cb2)=> {
            Room.create({
              name: roomData.name,
              project: roomData1.project,
              moxtra_binder_id: roomData.binder_id,
              author: user
            }, (err3, newRoom)=> {
              if (err3) {
                console.error(err3);
                return cb2(err3);
              }
              cb2(null, newRoom);
            });
          },
          (room, cb3)=> {
            connection.query(`
            select permission, user_email as email
            from wp_osios_project_room_permissions as oprp, wp_users as u
            where oprp.room_id = ? and u.id = oprp.user_id`, [roomData.id], (err, resData)=> {
              if (err) {
                console.error(err);
                return callback(err);
              }
              let results = [...resData];
              async.each(
                results,
                (el, cb)=> {
                  let userData = Object.assign({}, el);
                  User.findOne({email: userData.email})
                    .exec((err4, user)=> {
                      if (err4) {
                        console.error(err4);
                        return cb(err4);
                      }
                      if (!user) {
                        return cb();
                      }
                      RoomPermission.findOne({name: userData.permission.toLowerCase()})
                        .exec((err5, permission)=> {
                          if (err5) {
                            console.error(err5);
                            return cb(err5);
                          }
                          findRoomOrder(roomData.id, userData.id, (err7, order)=> {
                            if (err7) {
                              console.error(err7);
                              return cb(err7);
                            }
                            room.users.push({permission: permission, user, order});
                            return cb();
                          });
                        });
                    });
                },
                (err1)=> {
                  if (err1) {
                    console.error(err1);
                    return cb3(err1);
                  }
                  cb3(null, room);
                }
              );
            });
          }
        ],
        (wfErr, room)=> {
          if (wfErr) {
            console.error(wfErr);
            return callback(wfErr);
          }
          connection.query(`
        select session_key
        from wp_osios_project_scheduled_meet as opsm
        where room_id = ?`, [roomData.id], (err1, resData)=> {
            if (err1) {
              console.error(err1);
              return callback(err1);
            }
            room.scheduled_meets = resData.map(el=>Number.parseInt(el.session_key));
            room.save((err5)=> {
              if (err5) {
                console.error(err5);
                return callback(err5);
              }
              callback(null, room);
            });
          });
        }
      );
    }

    function createRooms(projId, project, callback) {
      connection.query(`
    select res1.id, res1.name, res1.binder_id, u.user_email as email
    from (select opr.room_name as name, opr.binder_id, opr.id, opr.room_author
      from wp_osios_project_rooms as opr
      where opr.project_id = ?) as res1 left join wp_users as u on u.id = res1.room_author`, [projId], (err, resData)=> {
        if (err) {
          console.error(err);
          return callback(err);
        }
        let results = [...resData];
        let roomsLength = results.length;
        async.forEachOf(
          results,
          (el, index, cb)=> {
            let data = Object.assign({}, el, {project: project});
            createRoom(data, (err2, room)=> {
              if (err2) {
                console.error(err2);
                return cb(err2);
              }
              console.log('project - ' + project._id + ' rooms done - ' + (index + 1) + '/' + roomsLength);
              cb();
            });
          },
          (err1)=> {
            if (err1) {
              console.error(err1);
              return callback(err1);
            }
            callback();
          }
        );
      });
    }

    function createProject(projData, index, projectsLength, callback) {
      let project = Object.assign({}, projData);
      let currentProject = {};
      currentProject.created_at = new Date(project.created_at);
      currentProject.icon = project.icon;
      currentProject.users = [];
      User.findOne({email: project.email})
        .exec((usErr, user)=> {
          if (usErr) {
            console.error(usErr);
            return callback(usErr);
          }
          currentProject.owner = user;
          Emotion.findOne({name: project.emotion === 'smile' ? 'happy' : (project.emotion === 'frown' ? 'unhappy' : (project.emotion ? 'ok' : 'happy'))})
            .exec((emErr, emotion)=> {
              if (emErr) {
                console.error(emErr);
                return callback(emErr);
              }
              currentProject.emotion = emotion;
              ProjectStatus.findOne({name: (project.status ? (project.status.toLowerCase() === 'complete' ? 'completed' : project.status) : 'deleted').toLowerCase()})
                .exec((stErr, status)=> {
                  if (stErr) {
                    console.error(stErr);
                    return callback(stErr);
                  }
                  if (!status) {
                    console.log('no status');
                  }
                  currentProject.status = status;
                  connection.query(
                    `SELECT DISTINCT u.id, u.user_email as email, our.role_name
                  from wp_osios_permissions as operm, wp_osios_user_roles as our, wp_users as u
                  where operm.project_id = ? and our.role_id = operm.user_role and u.id = operm.user_id`, [project.id], (err2, resultsData) => {
                      if (err2) {
                        console.error(err2);
                        return callback(err2);
                      }
                      let results = [...resultsData];

                      async.map(
                        results,
                        (elemData, cb)=> {
                          let elem = Object.assign({}, elemData);
                          User.findOne({email: elem.email})
                            .exec((err, user2)=> {
                              if (err) {
                                console.error(err);
                                return cb(err);
                              }
                              if (!user2) {
                                console.log('no user2');
                                return cb();
                              }
                              ProjectRole.findOne({name: elem.role_name.toLowerCase()})
                                .exec((err4, role)=> {
                                  if (err4) {
                                    console.error(err4);
                                    return cb(err4);
                                  }
                                  if (!role) {
                                    console.log('no role');
                                    return cb();
                                  }
                                  connection.query(`
                                  select DISTINCT list_order
                                  from wp_osios_project_order as opo
                                  where opo.user_id = ? and opo.project_id = ? limit 1`, [elem.id, project.id],
                                    (err5, orderData)=> {
                                      if (err5) {
                                        console.error(err5);
                                        return cb(err5);
                                      }
                                      let orderResults = [...orderData];
                                      if (orderResults.length > 0) {
                                        let order = orderResults[0].list_order;
                                        cb(null, {user: user2, role, order});
                                      } else {
                                        cb(null, {user: user2, role, order: 1});
                                      }
                                    });
                                });
                            });
                        },
                        (err3, results2)=> {
                          if (err3) {
                            console.error(err3);
                            return callback(err3);
                          }
                          currentProject.users = results2;

                          connection.query(`
                        select res1.changes, res1.edit_date, u.user_email as email
                        from (select changes, edit_date, user_id
                          from wp_osios_project_brief as opb
                          where opb.project_id = ? order by edit_date ASC) as res1 left join wp_users as u on u.id = res1.user_id`, [projData.id], (err, resData)=> {
                            if (err) {
                              console.error(err);
                              return callback(err);
                            }
                            let results3 = [...resData];
                            if (results3.length > 0) {
                              let split = head(results3).changes.toString().split('~~');
                              let t = split.find(elem=>elem.startsWith('project_platform'));
                              let originPlatform;
                              if (t) {
                                originPlatform = t.split('||')[2];
                              }
                              let t2 = split.find(elem=>elem.startsWith('project_device'));
                              let originDevice;
                              if (t2) {
                                originDevice = t2.split('||')[2];
                              }
                              async.map(
                                results3,
                                (el, cb)=> {
                                  let scope = Object.assign({}, el);
                                  let currentScope = {
                                    files: [],
                                    colors: [],
                                    platforms: [],
                                    devices: []
                                  };
                                  User.findOne({email: scope.email})
                                    .exec((err4, author)=> {
                                      if (err4) {
                                        console.error(err4);
                                        return cb(err4);
                                      }
                                      currentScope.created_by = author;
                                      currentScope.created_at = new Date(scope.edit_date);
                                      let scopeData = scope.changes.toString().split('~~');
                                      scopeData.forEach(elem => {
                                        let data = elem.split('||');
                                        if (data.length < 3) {
                                          return;
                                        }
                                        switch (data[0]) {
                                          case 'post_title':
                                          {
                                            currentScope.name = data[2] || 'Unknown';
                                          }
                                            break;
                                          case 'post_content':
                                          {
                                            currentScope.description = data[2];
                                          }
                                            break;
                                          case 'project_customer':
                                          {
                                            currentScope.intended_users = data[2];
                                          }
                                            break;
                                          case 'project_more':
                                          {
                                            currentScope.design_direction = data[2];
                                          }
                                            break;
                                          case 'project_tech_requirements':
                                          {
                                            currentScope.tech_requirements = data[2];
                                          }
                                            break;
                                          case 'project_additional_requirements':
                                          {
                                            currentScope.notes = data[2];
                                          }
                                            break;
                                          case 'project_fonts':
                                          {
                                            currentScope.fonts = data[2];
                                          }
                                            break;
                                          default:
                                          {
                                            if (data[0].startsWith('project_platform')) {
                                              if (data[2] !== '') {
                                                currentScope.platforms.push(data[2]);
                                              } else {
                                                currentScope.platforms.push(originPlatform);
                                              }
                                            }
                                            if (data[0].startsWith('project_device')) {
                                              if (data[2] !== '') {
                                                currentScope.devices.push(data[2]);
                                              } else {
                                                currentScope.devices.push(originDevice);
                                              }
                                            }
                                            if (data[0].startsWith('project_color') && data[2] !== '') {
                                              let color = data[2].startsWith('#') ? data[2] : `#${data[2]}`;
                                              currentScope.colors.push(color);
                                            }
                                            if (data[0].match(/^design_item\d/)) {
                                              currentScope.files.push(data[2]);
                                            }
                                          }
                                            break;
                                        }
                                      });
                                      async.parallel(
                                        [
                                          cb2=> {
                                            Platform.find({name: {$in: currentScope.platforms}})
                                              .exec((err5, platforms)=> {
                                                if (err5) {
                                                  console.error(err5);
                                                  return cb2(err5);
                                                }
                                                currentScope.platforms = platforms;
                                                cb2();
                                              });
                                          },
                                          cb2=> {
                                            Device.find({key: {$in: currentScope.devices}})
                                              .exec((err5, devices)=> {
                                                if (err5) {
                                                  console.error(err5);
                                                  return cb2(err5);
                                                }
                                                currentScope.devices = devices;
                                                cb2();
                                              });
                                          }
                                        ],
                                        (err5)=> {
                                          if (err5) {
                                            console.error(err5);
                                            return cb(err5);
                                          }
                                          currentScope.files = currentScope.files.filter(el2=>el2).map(el2=> {
                                            let name = el2.substr(el2.lastIndexOf('/') + 1);
                                            return {name: name !== '' ? name : 'Unknown', url: el2};
                                          });
                                          cb(null, currentScope);
                                        }
                                      );
                                    });
                                },
                                (err4, results4)=> {
                                  if (err4) {
                                    console.error(err4);
                                    return callback(err4);
                                  }
                                  currentProject.scopes = results4.filter(el3=>el3);

                                  Project.create(currentProject, (err5, newProject)=> {
                                    if (err5) {
                                      console.error(err5);
                                      return callback(err5);
                                    }

                                    createRooms(project.id, newProject, (err)=> {
                                      if (err) {
                                        console.error(err);
                                        return callback(err);
                                      }
                                      console.log('projects done: ' + (index + 1) + '/' + projectsLength);
                                      callback();
                                    });
                                  });
                                }
                              );
                            } else {
                              connection.query(`
                            select *
                            from wp_postmeta as pm
                            where post_id = ?`, [projData.id], (err1, resData2)=> {
                                if (err1) {
                                  console.error(err1);
                                  return callback(err1);
                                }
                                let results5 = [...resData2];
                                async.waterfall(
                                  [
                                    (cb)=> {
                                      let currentScope = {
                                        files: [],
                                        colors: [],
                                        platforms: [],
                                        devices: [],
                                        name: 'Unknown'
                                      };
                                      results5.forEach(el=> {
                                        let scope = Object.assign({}, el);

                                        currentScope.created_by = project.owner;
                                        currentScope.created_at = project.created_at;
                                        switch (scope.meta_key) {
                                          case 'project_title':
                                          {
                                            currentScope.name = scope.meta_value || 'Unknown';
                                          }
                                            break;
                                          case 'project_description':
                                          {
                                            currentScope.description = scope.meta_value;
                                          }
                                            break;
                                          case 'project_customer':
                                          {
                                            currentScope.intended_users = scope.meta_value;
                                          }
                                            break;
                                          case 'project_more':
                                          {
                                            currentScope.design_direction = scope.meta_value;
                                          }
                                            break;
                                          case 'project_tech_requirements':
                                          {
                                            currentScope.tech_requirements = scope.meta_value;
                                          }
                                            break;
                                          case 'project_additional_requirements':
                                          {
                                            currentScope.notes = scope.meta_value;
                                          }
                                            break;
                                          case 'project_fonts':
                                          {
                                            currentScope.fonts = scope.meta_value;
                                          }
                                            break;
                                          default:
                                          {
                                            if (scope.meta_key.startsWith('project_platform') && scope.meta_value && scope.meta_value !== '') {
                                              currentScope.platforms.push(scope.meta_value);
                                            }
                                            if (scope.meta_key.startsWith('project_device') && scope.meta_value && scope.meta_value !== '') {
                                              currentScope.devices.push(scope.meta_value);
                                            }
                                            if (scope.meta_key.startsWith('project_color') && scope.meta_value && scope.meta_value !== '') {
                                              let color = scope.meta_value.startsWith('#') ? scope.meta_value : `#${scope.meta_value}`;
                                              currentScope.colors.push(color);
                                            }
                                            if (scope.meta_key.match(/^design_item\d/)) {
                                              currentScope.files.push(scope.meta_value);
                                            }
                                          }
                                            break;
                                        }
                                      });
                                      cb(null, currentScope);
                                    },
                                    (currentScope, cb)=> {
                                      async.parallel(
                                        [
                                          cb2=> {
                                            Platform.find({name: {$in: currentScope.platforms}})
                                              .exec((err5, platforms)=> {
                                                if (err5) {
                                                  console.error(err5);
                                                  return cb2(err5);
                                                }
                                                currentScope.platforms = platforms;
                                                cb2();
                                              });
                                          },
                                          cb2=> {
                                            Device.find({key: {$in: currentScope.devices}})
                                              .exec((err5, devices)=> {
                                                if (err5) {
                                                  console.error(err5);
                                                  return cb2(err5);
                                                }
                                                currentScope.devices = devices;
                                                cb2();
                                              });
                                          }
                                        ],
                                        (err5)=> {
                                          if (err5) {
                                            console.error(err5);
                                            return cb(err5);
                                          }
                                          currentScope.files = currentScope.files.filter(el2=>el2).map(el2=> {
                                            let name = el2.substr(el2.lastIndexOf('/') + 1);
                                            return {name: name !== '' ? name : 'Unknown', url: el2};
                                          });
                                          cb(null, [currentScope]);
                                        }
                                      );
                                    }],
                                  (err4, results4)=> {
                                    if (err4) {
                                      console.error(err4);
                                      return callback(err4);
                                    }
                                    currentProject.scopes = results4.filter(el3=>el3);

                                    Project.create(currentProject, (err5, newProject)=> {
                                      if (err5) {
                                        console.error(err5);
                                        return callback(err5);
                                      }

                                      createRooms(project.id, newProject, (err6)=> {
                                        if (err6) {
                                          console.error(err6);
                                          return callback(err6);
                                        }
                                        console.log('projects done: ' + (index + 1) + '/' + projectsLength);
                                        callback();
                                      });
                                    });
                                  }
                                );
                              });
                            }
                          });
                        }
                      );
                    });
                });
            });
        });
    }

    connection.connect();


    async.waterfall([
        callback=> {
          connection.query(
            'select DISTINCT permission from wp_osios_project_room_permissions',
            (error, permissions)=> {
              if (error) {
                return callback(error);
              }
              async.each(
                permissions,
                (permission, callback2)=> {
                  RoomPermission.create({name: permission.permission}, error=> {
                    if (error) {
                      return callback2(error);
                    }
                    callback2();
                  });
                },
                error=> {
                  if (error) {
                    return callback(error);
                  }
                  callback();
                }
              );
            }
          );
        }
      ],
      error=> {
        if (error) {
          return reject(error);
        }
        connection.query(
          `select res2.id, res2. email,res2.name,res2.created_at,res2.status, opsi.icon_image as icon,res2.emotion
          from (select res3.id, res3.email, res3.name, res3.created_at, ops.status,res3.emotion
          from (select user_email as email, res1.id, res1.name, res1.created_at,res1.emotion
                from(SELECT op.project_id as id, p.post_title as name, p.post_date as created_at, op.owner_id, p.emotion
                    from (SELECT p.id, p.post_title, p.post_date, ope.emotion
          from wp_posts as p left join wp_osios_project_emotions as ope on ope.project_id = p.id) as p , wp_osios_projects as op where op.project_id = p.id) as res1 left join wp_users as u on u.id = res1.owner_id) as res3
          left join wp_osios_project_status as ops on ops.project_id = res3.id) as res2 left join wp_osios_project_screen_icon as opsi on res2.id = opsi.project_id`,
          (error, projects) => {
            if (error) {
              return reject(error);
            }
            console.log('projects: ' + projects.length);
            let projectsLength = projects.length;
            async.forEachOf(
              projects,
              (data, index, callback)=> {
                let elem = Object.assign({}, data);
                createProject(elem, index, projectsLength, callback);
              },
              (err)=> {
                if (err) {
                  return reject(err);
                }
                console.log('projects done!');
                connection.end();
                resolve();
              }
            );
          });
      });
  });
};
