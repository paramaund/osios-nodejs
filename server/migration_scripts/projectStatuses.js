import ProjectStatus from '../models/project_status';
import async from 'async';

export default ()=> {
  return new Promise((resolve, rejcet)=> {
    async.each(
      ['working', 'completed', 'deleted', 'problem', 'not started'],
      (status, callback)=> {
        ProjectStatus.create({name: status}, error=> {
          if (error) {
            return callback(error);
          }
          callback();
        });
      },
      error=> {
        if (error) {
          return rejcet(error);
        }
        resolve();
      });
  });
};
