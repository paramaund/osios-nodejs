import Platform from '../models/platform';

export default (req, res, next)=> {
  ['ios', 'android'].forEach((element, index, arr)=> {
    let elem = element;
    Platform.create({name: elem}, (err)=> {
      if (err) {
        console.error(err);
        return next(err);
      }
      if (index + 1 === arr.length) {
        res.end('done');
      }
    });
  });
};
