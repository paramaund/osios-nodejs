import async from 'async';
import Role from '../models/role';

function createRole(name) {
  return new Promise((resolve, reject)=> {
    Role.create({name: name}, (err)=> {
      if (err) {
        return reject(err);
      }
      resolve();
    });
  });
}

export default ()=> {
  return new Promise((resolve, reject)=> {
    async.parallel([
        callback=> {
          createRole('admin')
            .then(()=>callback())
            .catch(error=>callback(error));
        },
        callback=> {
          createRole('user')
            .then(()=>callback())
            .catch(error=>callback(error));
        }
      ],
      error=> {
        if (error) {
          return reject(error);
        }
        console.log('roles have been created');
        resolve();
      }
    );
  });
};
