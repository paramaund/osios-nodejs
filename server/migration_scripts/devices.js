import Device from '../models/device';
import Platform from '../models/platform';
import async from 'async';

function createDevices(platform) {
  return new Promise((resolve, reject)=> {
    let devices = {
      ios: [
        {
          name: 'iPhone 5/SE',
          key: 'iphone'
        },
        {
          name: 'iPhone 6',
          key: 'iphone6'
        },
        {
          name: 'iPad (non native)',
          key: 'ipad1'
        },
        {
          name: 'iPad (native)',
          key: 'ipad2'
        },
        {
          name: 'Apple Watch',
          key: 'applewatch'
        }
      ],
      android: [
        {
          name: 'Phone',
          key: 'phone'
        },
        {
          name: 'Tablet',
          key: 'tablet'
        }
      ]
    };

    async.each(
      devices[platform.name],
      (device, callback)=> {
        Device.create({name: device.name, key: device.key, platform: platform},
          error=> {
            if (error) {
              return callback(error);
            }
            callback();
          });
      },
      error=> {
        if (error) {
          return reject(error);
        }
        resolve();
      }
    );
  });
}

export default ()=> {
  return new Promise((resolve, reject)=> {
    async.waterfall([
        callback=> {
          async.parallel([
              callback2=> {
                Platform.findOneAndUpdate({name: 'ios'}, {name: 'ios'}, {new: true, upsert: true})
                  .exec((error, platform)=> {
                    if (error) {
                      return callback2(error);
                    }
                    callback2(null, platform);
                  });
              },
              callback2=> {
                Platform.findOneAndUpdate({name: 'android'}, {name: 'android'}, {new: true, upsert: true})
                  .exec((error, platform)=> {
                    if (error) {
                      return callback2(error);
                    }
                    callback2(null, platform);
                  });
              }
            ],
            (error, results)=> {
              if (error) {
                return callback(error);
              }
              callback(null, results);
            }
          );
        },
        (platforms, callback)=> {
          async.each(
            platforms,
            (platform, callback2)=> {
              createDevices(platform)
                .then(()=>callback2())
                .catch(error=>callback2(error));
            },
            error=> {
              if (error) {
                return callback(error);
              }
              callback();
            }
          );
        }
      ],
      error=> {
        if (error) {
          return reject(error);
        }
        resolve();
      }
    );
  });
};
