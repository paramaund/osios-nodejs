import async from 'async';
import PaymentPlan from '../models/payment_plan';

export default ()=> {
  return new Promise((resolve, reject)=> {
    async.each(
      ['TestFreeTrial', 'Pay_Level2'],
      (plan, callback)=> {
        PaymentPlan.create({name: plan, key: plan}, (err)=> {
          if (err) {
            return callback(err);
          }
          callback();
        });
      },
      error=> {
        if (error) {
          return reject(error);
        }
        resolve();
      }
    );
  });
};
