import async from 'async';
import * as moxtra from '../utils/moxtra';
import Room from '../models/room';
import RoomPermission from '../models/room_permission';
import flatten from 'lodash/flatten';
import maxBy from 'lodash/maxBy';
import sortBy from 'lodash/sortBy';

function getRoomMaxOrder(user, project) {
  return new Promise((resolve, reject)=> {
    Room.find({users: {$elemMatch: {user: user.user}}, project: project})
      .exec((error, rooms)=> {
        if (error) {
          return reject(error);
        }
        if (rooms.length === 0) {
          return resolve(null, -1);
        }
        let results = flatten(rooms.map(room=>room.users));
        let maxOrderUser = maxBy(results, 'order');
        resolve(null, maxOrderUser.order);
      });
  });
}

function mapUsers(users, project) {
  return new Promise((resolve, reject)=> {
    async.map(
      users,
      (user, callback)=> {
        async.waterfall([
            callback2=> {
              if (user.permission) {
                return callback2(null, user);
              }
              RoomPermission.findOne({name: 'notify'})
                .exec((error, permission)=> {
                  if (error) {
                    return callback2(error);
                  }
                  callback2(null, Object.assign(user, {permission}));
                });
            },
            (user, callback2)=> {
              getRoomMaxOrder(user, project)
                .then(order=>callback2(null, Object.assign(user, {order: order + 1})))
                .catch(error=>callback2(error));
            }
          ],
          (error, user)=> {
            if (error) {
              return callback(error);
            }
            callback(null, user);
          });
      },
      (error, users)=> {
        if (error) {
          return reject(error);
        }
        resolve(users);
      }
    );
  });
}

export function createRoom(options) {
  return new Promise((resolve, reject)=> {
    Room.create(options, (error, room)=> {
      if (error) {
        return reject(error);
      }
      console.log('Room has been created: ');
      console.dir(room);
      resolve(room);
    });
  });
}

export function createDefaultRoom(project, token) {
  return new Promise((resolve, reject)=> {
    let name = 'Development';
    async.waterfall([
        callback=> {
          moxtra.createMoxtraBinder(name, token)
            .then(response=> {
              console.log({message: 'moxtra binder has been created', response});
              callback(null, response.data.id);
            })
            .catch(error=>callback(error));
        },
        (binderId, callback)=> {
          mapUsers(project.users.map(user=> {
            return {
              user: user.user
            };
          }), project)
            .then(users=>callback(null, {binderId, users}))
            .catch(error=>callback(error));
        }
      ],
      (error, results)=> {
        if (error) {
          return reject(error);
        }
        createRoom({
          name: name,
          author: project.owner,
          project: project,
          moxtra_binder_id: results.binderId,
          users: results.users
        })
          .then(room=>resolve(room))
          .catch(error=>reject(error));
      }
    );
  });
}

export function create(req, res, next) {
  async.waterfall([
      callback=> {
        moxtra.createMoxtraBinder(req.body.name, req.session.moxtraAccessToken)
          .then(response=>callback(null, response.data.id))
          .catch(error=>callback(error));
      },
      (binderId, callback)=> {
        mapUsers([...req.body.users, {user: req.user}], req.body.projectId)
          .then(users=>callback(null, {binderId, users}))
          .catch(error=>callback(error));
      }
    ],
    (error, results)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      createRoom({
        name: req.body.name,
        author: req.user,
        project: req.body.projectId,
        moxtra_binder_id: results.binderId,
        users: results.users
      })
        .then(room=>res.json({message: 'Room has been successfully created.', room}))
        .catch(error=>next(error));
    }
  );
}

export function fetch(req, res, next) {
  let project = req.query.project;
  Room.find({users: {$elemMatch: {user: req.user}}, project: project})
    .populate({
      path: 'author',
      select: '_id first_name last_name email avatar'
    })
    .populate({
      path: 'users.user',
      select: '_id first_name last_name email avatar'
    })
    .populate('users.permission')
    .exec((error, rooms)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({
        rooms: sortBy(rooms, (room=> {
          let userForOrder = room.users.find(elem=>elem.user._id.id === req.user._id.id);
          return userForOrder.order;
        }))
      });
    });
}

export function fetchOne(req, res, next) {
  let id = req.params.id;
  Room.findById(id)
    .populate({
      path: 'author',
      select: '_id first_name last_name email avatar'
    })
    .populate({
      path: 'users.user',
      select: '_id first_name last_name email avatar'
    })
    .populate('users.permission')
    .exec((error, room)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      if (!room) {
        return next(new Error('No room was found with such id.'));
      }
      res.json({room});
    });
}

export function remove(req, res, next) {
  let id = req.params.id;
  Room.remove({_id: id})
    .exec(error=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({id});
    });
}

export function changeOrder(req, res, next) {
  let newOrderRooms = req.body.rooms;
  Room.find({_id: {$in: newOrderRooms.map(elem=>elem.room)}})
    .exec((error, rooms)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      async.each(
        rooms,
        (room, callback)=> {
          let element = newOrderRooms.find(elem=>elem.room === room._id.toString());
          let user = room.users.find(user=>user.user.id === req.user._id.id);
          if (user) {
            user.order = element.order;
            room.save(error=> {
              if (error) {
                return callback(error);
              }
              callback();
            });
          } else {
            callback(new Error('Oops, something went wrong..'));
          }
        },
        error=> {
          if (error) {
            console.error(error);
            return next(error);
          }
          res.end();
        }
      );
    });
}
