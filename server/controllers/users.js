import async from 'async';
import url from 'url';
import shortid from 'shortid';
import {removeUserHiddenFields} from '../utils/utils';
import User from '../models/user';
import Project from '../models/project';
import Role from '../models/role';
import Company from '../models/company';
import * as projects from '../controllers/projects';
import sendMail from '../utils/mail';
import signupHtml from '../config/mail_templates/signup';
import * as moxtra from '../utils/moxtra';
import {moxtra as moxtraSecrets, mail as mailSecrets} from '../config/secrets';
import omit from 'lodash/omit';
import isEqual from 'lodash/isEqual';
import uniqBy from 'lodash/uniqBy';
import uniqWith from 'lodash/uniqWith';
import flatten from 'lodash/flatten';

export function create(req, res, next) {
  async.waterfall(
    [
      callback=> {
        Role.findOne({name: 'user'})
          .exec((err, role)=> {
            if (err) {
              return callback(err);
            }
            if (!role) {
              Role.create({name: 'user'}, (err, role)=> {
                if (err) {
                  return callback(err);
                }
                callback(null, role);
              });
            } else {
              callback(null, role);
            }
          });
      },
      (role, callback)=> {
        if (!req.body.password) {
          req.body.password = shortid.generate();
        }
        let user = new User(req.body);
        user.role = role;
        user.moxtra_unique_id = moxtraSecrets.userPrefix + shortid.generate();
        user.access = req.body.isSuperUser;
        user.save(err=> {
          if (err) {
            return callback(err);
          }
          callback(null, user);
        });
      },
      (user, callback)=> {
        moxtra.getMoxtraUserAccessToken(user.moxtra_unique_id, user.email)
          .then(response=> {
            req.session.moxtraAccessToken = response.access_token;
            callback(null, user);
          })
          .catch(error=> {
            callback(error);
          });
      },
      (user, callback)=> {
        if (req.body.company) {
          Company.findById(req.body.company._id)
            .exec((error, company)=> {
              if (error) {
                return callback(error);
              }
              company.users.push(user);
              company.save(error=> {
                if (error) {
                  return callback(error);
                }
                callback(null, user);
              });
            });
        } else {
          req.logIn(user, err=> {
            if (err) {
              return callback(err);
            }
            callback(null, user);
          });
        }
      }
    ],
    (err, user)=> {
      if (err) {
        console.error(err);
        return next(err);
      }
      async.parallel([
          callback2=> {
            let loginUrl = url.format({
              protocol: req.protocol,
              host: req.get('host'),
              pathname: 'login'
            });
            let msg = signupHtml.replace(/\[loginUrl]/g, loginUrl).replace(/\[email]/g, user.email).replace(/\[password]/g, req.body.password);
            sendMail({
              to: 'abvgdeyyyka@gmail.com',
              subject: `OSIOS - Registration`,
              text: msg.replace(/<.*>/g, ''),
              html: msg
            })
              .then(()=> {
                return callback2();
              })
              .catch((err)=> {
                return callback2(err);
              });
          },
          callback2=> {
            sendMail({
              to: mailSecrets.address,
              subject: `OSIOS - New Registration`,
              text: `A new user has signed up! Email: ${user.email}`,
              html: `<html><body><p>A new user has signed up!</p><p>Email: ${user.email}</p></body></html>`
            })
              .then(()=> {
                return callback2();
              })
              .catch((err)=> {
                return callback2(err);
              });
          }
        ],
        error=> {
          if (error) {
            console.error(error);
            return;
          }
          console.log('Email has been sent!');
        });

      projects.createDefaultProject(user, req.session.moxtraAccessToken);

      if (req.body.isSuperUser) {
        res.json({
          user: removeUserHiddenFields(user),
          message: 'Account has been successfully created.'
        });
      } else {
        next();
      }
    }
  );
}

export function fetch(req, res, next) {
  Company.find({owner: req.user._id}, 'users')
    .populate({
      path: 'users',
      select: '_id first_name last_name avatar email'
    })
    .exec((error, companies)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      let users = uniqBy(flatten(companies.map(company=>company.users)), '_id');
      res.json({allUsers: users});
    });
}

export function fetchById(req, res, next) {
  let id = req.params.id;
  if (id === 'me') {
    return res.json({user: removeUserHiddenFields(req.user)});
  }
  User.findById(id, '-password -stripe_profile -payment_profile -access')
    .populate('role')
    .populate('payment_plan')
    .exec((err, user)=> {
      if (err) {
        console.error(err);
        return next(err);
      }
      if (!user) {
        res.statusCode = 404;
        return res.json({message: 'User not found.'});
      }
      res.json({user});
    });
}

export function updateById(req, res, next) {
  let id = req.params.id;
  if (id === 'me') {
    id = req.user._id;
  }
  async.waterfall([
      callback=> {
        User.findOneAndUpdate({_id: id}, {$set: omit(req.body, ['access', 'payment_expiry', 'password'])}, {new: true})
          .populate('role')
          .populate('payment_plan')
          .exec((error, user)=> {
            if (error) {
              return callback(error);
            }
            if (!user) {
              return callback(new Error('User not found!'));
            }
            if (user.avatar) {
              moxtra.uploadUserProfilePicture(user.moxtra_unique_id, user.avatar, req.app.get('protocol') + req.headers.host)
                .then(response=> {
                  return callback(null, user);
                })
                .catch(error=> {
                  return callback(error);
                })
            }
          });
      },
      (user, callback)=> {
        if (!req.body.password) {
          return callback(null, user);
        }
        user.password = req.body.password;
        user.save(error=> {
          if (error) {
            return callback(error);
          }
          callback(null, user);
        });
      }
    ],
    (error, user)=> {
      if (error) {
        console.error(error);
        return next(error);
      }

      res.json({
        user: removeUserHiddenFields(user),
        message: 'Your profile has been updated.'
      });
    }
  );
}

export function deleteById(req, res, next) {
  User.remove({_id: req.user._id})
    .exec(error=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({message: 'User has been successfully deleted.'});
    });
}

export function getStatistics(req, res, next) {
  let id = req.params.id;
  if (id === 'me') {
    id = req.user._id;
  }
  async.waterfall([
      callback=> {
        Project.find({owner: id})
          .exec((error, projects)=> {
            if (error) {
              return callback(error);
            }
            callback(null, projects);
          });
      }
    ],
    (error, projects)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({
        statistics: {
          projects: projects.length,
          collaborators: uniqWith(flatten(projects.map(project=>project.users)), (el1, el2)=>isEqual(el1.user, el2.user)).length - 1
        }
      });
    }
  );
}
