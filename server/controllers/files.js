import easyimg from 'easyimage';
import async from 'async';
import fs from 'fs';
import path from 'path';

export function crop(req, res, next) {
  let file = req.file;
  let props = req.body.props;
  async.waterfall(
    [
      callback=> {
        easyimg.info(file.path)
          .then(data=>callback(null, data))
          .catch(error=>callback(error));
      },
      (fileInfo, callback)=> {
        let options = {
          src: req.file.path,
          dst: 'uploads/users/' + req.user._id + '/' + fileInfo.name,
          cropwidth: 300,
          cropheight: 300,
          width: 300,
          height: 300
        };
        if (props) {
          options = Object.assign({}, options, props);
        } else if (fileInfo.width > fileInfo.height) {
          options.height = '300^';
        } else {
          options.width = '300^';
        }

        easyimg.rescrop(options)
          .then(image=> {
            callback(null, image);
          })
          .catch(error=> {
              callback(error);
            }
          );
      }
    ],
    (error, image)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({url: `/${image.path}`});
      fs.exists(file.path, exists=> {
        if (exists) {
          fs.unlink(file.path);
        }
      });
    }
  )
  ;
}

export function upload(req, res, next) {
  let files = req.files;
  async.map(
    files,
    (file, callback)=> {
      let newPath = file.path + file.originalname;
      fs.rename(file.path, newPath, ()=> {
        callback(null, {
          name: file.originalname,
          url: `/${newPath}`,
          size: file.size
        });
      });
    },
    (error, files)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({
        files: files
      });
    }
  );
}
