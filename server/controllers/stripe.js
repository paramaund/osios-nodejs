import async from 'async';
import User from '../models/user';
import PaymentPlan from '../models/payment_plan';
import * as stripeUtils from '../utils/stripe';
import {removeUserHiddenFields} from '../utils/utils';

export function subscribeStripe(req, res, next) {
  if (req.body.isSuperUser) {
    return next();
  }

  async.waterfall([
      callback=> {
        PaymentPlan.findOne({key: req.body.plan})
          .exec((error, plan)=> {
            if (error) {
              return callback(error);
            }
            if (!plan) {
              return callback(new Error('There is no such payment plan.'));
            }
            callback(null, plan);
          });
      },
      (plan, callback)=> {
        if (!req.user.stripe_profile) {
          stripeUtils.createCustomer({
              user: req.user
            })
            .then(customer=> {
              return callback(null, customer, plan);
            })
            .catch(error=> {
              return callback(error);
            })
        } else {
          stripeUtils.retrieveCustomer(req.user.stripe_profile)
            .then(customer=> {
              return callback(null, customer, plan);
            })
            .catch(error=> {
              return callback(error);
            });
        }
      },
      (customer, plan, callback)=> {
        stripeUtils.createSubscription({
            customer,
            plan: req.body.plan,
            source: req.body.token.id,
            tax: req.body.tax
          })
          .then(results=> {
            results.plan = plan;
            results.customer = customer;
            return callback(null, results);
          })
          .catch(error=> {
            return callback(error);
          })
      }
    ],
    (error, results)=> {
      if (error) {
        console.error(error);
        return next(error);
      }

      let data = {
        stripe_profile: results.customer.id,
        payment_profile: results.subscription.id,
        payment_plan: results.plan,
        payment_expiry: results.trial_end
      };

      User.findOneAndUpdate({_id: req.user._id}, {$set: data}, {new: true})
        .populate('role')
        .populate('payment_plan')
        .exec((err, user)=> {
          if (err) {
            console.error(err);
            return next(err);
          }
          res.json({
            user: removeUserHiddenFields(user),
            message: 'Subscription has been successfully created.'
          });
        });
    });
}

export function getToken(req, res, next) {
  if (req.body.isSuperUser) {
    return next();
  }
  stripeUtils.createToken({
      card: req.body.card,
      month: req.body.month,
      year: req.body.year,
      cvc: req.body.cvc
    })
    .then(token=> {
      req.body.token = token;
      next();
    })
    .catch(error=> {
      console.error(error);
      return next(error);
    });
}
