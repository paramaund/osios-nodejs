import passport from 'passport';
import async from 'async';
import * as moxtra from '../utils/moxtra';
import {removeUserHiddenFields} from '../utils/utils';

export function login(req, res, next) {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      console.error(err);
      return next(err);
    }
    if (!user) {
      res.statusCode = 401;
      return res.json(info);
    }
    async.waterfall([
        callback=> {
          req.logIn(user, err => {
            if (err) {
              return callback(err);
            }
            callback();
          });
        },
        callback=> {
          moxtra.getMoxtraUserAccessToken(user.moxtra_unique_id, user.firstName ? user.firstName : user.email, user.lastName)
            .then(response=> {
              req.session.moxtraAccessToken = response.access_token;
              callback();
            })
            .catch(error=> {
              callback(error);
            });
        }
      ],
      error=> {
        if (error) {
          console.error(error);
          return next(error);
        }
        return res.json({user: removeUserHiddenFields(user)});
      });
  })(req, res, next);
}

export function logout(req, res, next) {
  req.logOut();
  res.redirect('/');
}

export function ensureAuthenticated(req, res, next) {
  if (!req.isAuthenticated()) {
    return res.status(401).json({message: 'Not Authorized'});
  }
  next();
}
