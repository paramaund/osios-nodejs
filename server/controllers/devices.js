import async from 'async';
import Device from '../models/device';

export function fetch(req, res, next) {
  Device.find()
    .populate('platform')
    .exec((error, devices)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({devices});
    });
}
