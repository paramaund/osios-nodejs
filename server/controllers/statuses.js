import async from 'async';
import ProjectStatus from '../models/project_status';

export function fetch(req, res, next) {
  ProjectStatus.find()
    .exec((error, statuses)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({statuses});
    });
}
