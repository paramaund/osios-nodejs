import async from 'async';
import Platform from '../models/platform';

export function fetch(req, res, next) {
  Platform.find()
    .exec((error, platforms)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({platforms});
    });
}
