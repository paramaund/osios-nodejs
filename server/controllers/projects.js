import async from 'async';
import fs from 'fs';
import path from 'path';
import Project from '../models/project';
import Emotion from '../models/emotion';
import ProjectStatus from '../models/project_status';
import Platform from '../models/platform';
import Device from '../models/device';
import ProjectRole from '../models/project_role';
import {createDefaultRoom} from '../controllers/rooms';
import sendMail from '../utils/mail';
import last from 'lodash/last';
import head from 'lodash/head';
import sortBy from 'lodash/sortBy';
import maxBy from 'lodash/maxBy';
import countBy from 'lodash/countBy';
import flatten from 'lodash/flatten';

function getProjectMaxOrderByStatus(status, user) {
  return new Promise((resolve, reject)=> {
    async.waterfall([
        callback3=> {
          ProjectStatus.findOne({name: status})
            .exec((error, status)=> {
              if (error) {
                return callback3(error);
              }
              callback3(null, status);
            });
        },
        (status, callback3)=> {
          Project.find({users: {$elemMatch: {user: user}}, status: status})
            .exec((error, projects)=> {
              if (error) {
                return callback3(error);
              }
              if (projects.length === 0) {
                return callback3(null, 0);
              }
              let results = flatten(projects.map(proj=>proj.users)).filter(elem=>elem.user.id === user._id.id);
              let maxOrderUser = maxBy(results, 'order');
              callback3(null, maxOrderUser.order);
            });
        }
      ],
      (error, order)=> {
        if (error) {
          return reject(error);
        }
        resolve(order);
      });
  });
}

export function getTotalActiveProjectsCount(owner) {
  return new Promise((resolve, reject)=> {
    async.waterfall([
        callback2=> {
          ProjectStatus.find({name: {$in: ['completed', 'deleted']}})
            .exec((error, statuses)=> {
              if (error) {
                return callback2(error);
              }
              callback2(null, statuses);
            });
        },
        (statuses, callback2)=> {
          Project.count({owner: owner, status: {$nin: statuses}})
            .exec((error, count)=> {
              if (error) {
                return callback2(error);
              }
              callback2(null, count);
            });
        }
      ],
      (error, count)=> {
        if (error) {
          return reject(error);
        }
        resolve(count);
      });
  });
}

export function addTotalActiveProjectsCount(req, res, next) {
  getTotalActiveProjectsCount(req.user)
    .then(count=>res.json(Object.assign(res.locals.data || {}, {total: count})))
    .catch(error=> {
      console.error(error);
      next(error);
    });
}

export function createDefaultProject(owner, token) {
  async.parallel([
      callback=> {
        Emotion.findOne({name: 'happy'})
          .exec((error, emotion)=> {
            if (error) {
              return callback(error);
            }
            callback(null, emotion);
          });
      },
      callback=> {
        async.map(
          [{user: owner, role: 'owner'}],
          (user, callback2)=> {
            async.waterfall([
                callback3=> {
                  ProjectRole.findOne({name: user.role})
                    .exec((error, projectRole)=> {
                      if (error) {
                        return callback3(error);
                      }
                      callback3(null, projectRole);
                    });
                },
                (projectRole, callback3)=> {
                  Project.find({users: {$elemMatch: {user: user.user._id}}})
                    .exec((error, projects)=> {
                      if (error) {
                        return callback3(error);
                      }
                      callback3(null, {user: user.user, role: projectRole, order: projects.length + 1});
                    });
                }
              ],
              (error, result)=> {
                if (error) {
                  return callback2(error);
                }
                callback2(null, result);
              });
          },
          (error, users)=> {
            if (error) {
              return callback(error);
            }
            callback(null, users);
          }
        );
      },
      callback=> {
        ProjectStatus.findOne({name: 'working'})
          .exec((error, status)=> {
            if (error) {
              return callback(error);
            }
            callback(null, status);
          });
      },
      callback=> {
        Platform.find({name: 'ios'})
          .exec((error, platforms)=> {
            if (error) {
              return callback(error);
            }
            callback(null, platforms);
          });
      },
      callback=> {
        Device.find({key: 'iphone'})
          .exec((error, devices)=> {
            if (error) {
              return callback(error);
            }
            callback(null, devices);
          });
      }
    ],
    (error, results)=> {
      createProject({
        owner: owner,
        emotion: results[0],
        users: results[1],
        status: results[2],
        scopes: [{
          created_by: owner,
          name: 'Demo Project',
          platforms: results[3],
          devices: results[4],
          description: 'We’ve created this Demo Project so you can have a play, and see how OSIOS works before you’ve even created your first project. ' +
          'This Demo project describes an app produced for our client ‘Linda’, who wants a personal development app to support her business. And her project manager, and main point-of-contact is ‘Gemma’, our project manager. ' +
          'OSIOS can be used to design and manage any type of mobile app project, for any major platform, or device.',
          intended_users: 'This app will appeal to english speaking adults, primarily between 30 and 50 years of age.',
          design_direction: 'Make it ‘Fresh’',
          colors: ['#D4FF50'],
          fonts: 'Fonts Desired',
          tech_requirements: '',
          notes: '',
          files: []
        }]
      }, token)
        .then(project=> {
          console.dir(project);
        })
        .catch(error=> {
          console.dir(error);
        });
    });
}

export function createProject(options, token) {
  return new Promise((resolve, reject)=> {
    async.waterfall([
        callback=> {
          Project.create(options, (error, project)=> {
            if (error) {
              return callback(error);
            }
            callback(null, project);
          });
        },
        (project, callback)=> {
          createDefaultRoom(project, token)
            .then(room=>callback(null, project))
            .catch(error=>callback(error));
        }
      ],
      (error, project)=> {
        if (error) {
          return reject(error);
        }
        resolve(project);
      });
  });
}

export function create(req, res, next) {
  if (req.user.payment_plan && req.user.totalProjects >= req.user.payment_plan.projects) {
    return next(new Error('You\'ve used all limits of your account. Please upgrade your account to be able to add more projects.'));
  }
  async.parallel([
      callback=> {
        Emotion.findOne({name: 'happy'})
          .exec((error, emotion)=> {
            if (error) {
              return callback(error);
            }
            callback(null, emotion);
          });
      },
      callback=> {
        async.map(
          [...req.body.users, {user: req.user, role: {name: 'owner'}}],
          (user, callback2)=> {
            async.waterfall([
                callback3=> {
                  if (user.role._id) {
                    return callback3(null, user.role);
                  }
                  ProjectRole.findOne({name: user.role.name})
                    .exec((error, role)=> {
                      if (error) {
                        return callback3(error);
                      }
                      callback3(null, role);
                    });
                },
                (role, callback3)=> {
                  getProjectMaxOrderByStatus('working', req.user)
                    .then(order=>callback3(null, {role, user: user.user, order: order + 1}))
                    .catch(error=>callback3(error));
                }
              ],
              (error, user)=> {
                if (error) {
                  return callback2(error);
                }
                callback2(null, user);
              });
          },
          (error, users)=> {
            if (error) {
              return callback(error);
            }
            callback(null, users);
          }
        );
      },
      callback=> {
        ProjectStatus.findOne({name: 'working'})
          .exec((error, status)=> {
            if (error) {
              return callback(error);
            }
            callback(null, status);
          });
      },
      callback=> {
        let scope = req.body.scopes.find(elem=>elem.isNew) || {platforms: []};
        Platform.find({_id: {$in: scope.platforms.map(platform=>platform._id)}})
          .exec((error, platforms)=> {
            if (error) {
              return callback(error);
            }
            callback(null, platforms);
          });
      },
      callback=> {
        let scope = req.body.scopes.find(elem=>elem.isNew) || {devices: []};
        Device.find({_id: {$in: scope.devices.map(device=>device._id)}})
          .exec((error, devices)=> {
            if (error) {
              return callback(error);
            }
            callback(null, devices);
          });
      }
    ],
    (error, results)=> {
      let scope = req.body.scopes.find(elem=>elem.isNew) || {};
      createProject({
        owner: req.user,
        emotion: results[0],
        users: results[1],
        status: results[2],
        scopes: [{
          created_by: req.user,
          name: scope.name,
          platforms: results[3],
          devices: results[4],
          description: scope.description,
          intended_users: scope.intended_users,
          design_direction: scope.design_direction,
          colors: scope.colors,
          fonts: scope.fonts,
          tech_requirements: scope.tech_requirements,
          notes: scope.notes,
          files: scope.files
        }]
      }, req.session.moxtraAccessToken)
        .then(project=> {
          res.json({
            message: 'Project has been successfully created.',
            project: project
          });
          if (req.body.option && req.body.option.key === 'send') {
            async.each(
              req.body.users.filter(user=>user.role.name === 'lead client'),
              (user, callback)=> {
                sendMail({
                  to: user.user.email,
                  subject: `OSIOS - Project has been created.`,
                  text: `Project ${scope.name} has been created. Please complete project info.`,
                  html: `<html><body><p>Project ${scope.name} has been created. Please complete project info.</p></body></html>`
                })
                  .then(()=> {
                    return callback();
                  })
                  .catch((err)=> {
                    return callback(err);
                  });
              },
              error=> {
                if (error) {
                  console.error(error);
                }
              }
            );
          }
        })
        .catch(error=> {
          return next(error);
        });
    });
}

function findProjectsByStatus(limit, offset, user, searchOptions) {
  return new Promise((resolve, reject)=> {
    async.waterfall([
        callback=> {
          Project.find(searchOptions)
            .limit(limit)
            .skip(offset)
            .populate({
              path: 'scopes',
              // options: {
              //   sort: 'created_at',
              //   limit: 1
              // },
              populate: 'devices platforms'
            })
            .populate({
              path: 'users.user',
              select: '_id first_name last_name email avatar',
              populate: 'role payment_plan'
            })
            .populate('users.role emotion status')
            .populate({
              path: 'scopes.created_by',
              select: '_id first_name last_name email avatar'
            })
            .populate({
              path: 'owner',
              select: '_id first_name last_name email avatar',
              populate: 'role payment_plan'
            })
            .exec((error, projects)=> {
              if (error) {
                return callback(error);
              }
              callback(null, sortBy(projects, (project=> {
                let userForOrder = project.users.find(elem=>elem.user._id.id === user._id.id);
                return userForOrder.order;
              })));
            });
        }
      ],
      (error, projects)=> {
        if (error) {
          return reject(error);
        }
        resolve(projects);
      });
  });
}

export function fetch(req, res, next) {
  let status = req.query.status;
  let limit = Number(req.query.limit || 10);
  let offset = Number(req.query.offset || 0);
  let statuses = [];
  if (!status || status === 'active') {
    statuses = ['working', 'problem', 'not started'];
  } else if (!Array.isArray(status)) {
    statuses = [status];
  } else {
    statuses = status;
  }
  let isOwner = req.query.isOwner === 'true';

  async.map(
    statuses,
    (status, callback)=> {
      async.waterfall([
          callback2=> {
            ProjectStatus.findOne({name: status})
              .exec((error, status)=> {
                if (error) {
                  return callback2(error);
                }
                callback2(null, status);
              });
          },
          (status, callback2)=> {
            let searchOptions = {users: {$elemMatch: {user: req.user}}, status: status};
            if (!isOwner) {
              searchOptions.owner = {$ne: req.user};
            } else {
              searchOptions.owner = req.user;
            }
            Project.count(searchOptions)
              .exec((error, count)=> {
                if (error) {
                  return callback2(error);
                }
                callback2(null, {searchOptions, count, status});
              });
          },
          (results, callback2)=> {
            findProjectsByStatus(limit, offset, req.user, results.searchOptions)
              .then(projects=>callback2(null, {
                status: results.status.name,
                totalCount: results.count,
                projects,
                isOwner
              }))
              .catch(error=>callback2(error));
          }
        ],
        (error, results)=> {
          if (error) {
            return callback(error);
          }
          callback(null, results);
        });
    },
    (error, results)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json(results);
    }
  );
}

export function mapFiles(req, res, next) {
  if (!req.body.scopes) {
    return next();
  }
  let scope = req.body.scopes.find(elem=>elem.isNew);
  if (!scope) {
    return next();
  }
  let files = scope.files || {files: []};
  async.map(
    files,
    (file, callback)=> {
      let oldPath = file.url.slice(1);
      if (oldPath.startsWith('uploads/')) {
        return callback(null, file);
      }
      let folder = path.join('uploads/users', req.user._id.toString());

      let pathRes = folder.split('/');
      let folderPath = '';
      pathRes.forEach(elem=> {
        folderPath = path.join(folderPath, elem);
        if (!fs.existsSync(folderPath)) {
          fs.mkdirSync(folderPath);
        }
      });

      let newPath = path.join(folder, file.name);
      fs.rename(oldPath, newPath, ()=> {
        callback(null, {
          name: file.name,
          url: `/${newPath}`,
          size: file.size
        });
      });
    },
    (error, files)=> {
      if (error) {
        return next(error);
      }
      scope.files = files;
      next();
    }
  );
}

function mapUsers(users, me) {
  return new Promise((resolve, reject)=> {
    async.map(
      users,
      (user, callback2)=> {
        async.waterfall([
            callback3=> {
              if (user.role._id) {
                return callback3(null, user.role);
              }
              ProjectRole.findOne({name: user.role.name})
                .exec((error, role)=> {
                  if (error) {
                    return callback3(error);
                  }
                  callback3(null, role);
                });
            },
            (role, callback3)=> {
              if (user.order) {
                return callback3(null, {role, user: user.user, order: user.order});
              }
              getProjectMaxOrderByStatus('working', me)
                .then(order=>callback3(null, {role, user: user.user, order: order}))
                .catch(error=>callback3(error));
            }
          ],
          (error, user)=> {
            if (error) {
              return callback2(error);
            }
            callback2(null, user);
          });
      },
      (error, users)=> {
        if (error) {
          return reject(error);
        }
        resolve(users);
      }
    );
  });
}

export function update(req, res, next) {
  const id = req.params.id;
  async.waterfall([
      callback=> {
        if (!req.body.users) {
          return callback(null, null);
        }
        mapUsers(req.body.users, req.user)
          .then(users=>callback(null, users))
          .catch(error=>callback(error));
      },
      (users, callback)=> {
        if (users) {
          req.body.users = users;
        }
        if (req.body.scopes) {
          let scope = req.body.scopes.find(elem=>elem.isNew);
          if (scope) {
            delete scope._id;
            scope.created_by = req.user;
            scope.created_at = Date.now();
          }
        }
        if (req.body.status) {
          if (req.body.status.name === 'deleted') {
            req.body.deleted_at = Date.now();
            req.body.deleted_by = req.user;
          } else {
            req.body.deleted_at = null;
            req.body.deleted_by = null;
            req.body.deleted_reason = null;
          }
        }
        Project.findOneAndUpdate({_id: id}, {$set: req.body}, {new: true})
          .populate({
            path: 'users.user',
            select: '_id first_name last_name email avatar',
            populate: 'role payment_plan'
          })
          .populate({
            path: 'scopes',
            // options: {
            //   sort: 'created_at',
            //   limit: 1
            // },
            populate: 'devices platforms'
          })
          .populate('users.role emotion status')
          .populate({
            path: 'scopes.created_by',
            select: '_id first_name last_name email avatar'
          })
          .populate({
            path: 'owner',
            select: '_id first_name last_name email avatar',
            populate: 'role payment_plan'
          })
          .exec((error, project)=> {
            if (error) {
              return callback(error);
            }
            callback(null, project);
          });
      }
    ],
    (error, project)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({project, message: 'Project has been successfully updated.'});
    }
  );
}

export function fetchStatistics(req, res, next) {
  let status = req.query.status;

  let statuses = [];
  if (!status || status === 'active') {
    statuses = ['working', 'problem', 'not started'];
  } else if (!Array.isArray(status)) {
    statuses = [status];
  } else {
    statuses = status;
  }

  async.waterfall([
      callback=> {
        ProjectStatus.find({name: {$in: statuses}})
          .exec((error, statuses)=> {
            if (error) {
              return callback(error);
            }
            callback(null, statuses);
          });
      },
      (statuses, callback)=> {
        Project.find({
            users: {$elemMatch: {user: req.user}},
            status: {$in: statuses}
          }, '_id scopes.name scopes.created_at created_at', {sort: 'created_at'})
          .exec((error, projects)=> {
            if (error) {
              return callback(error);
            }
            callback(null, projects);
          });
      }
    ],
    (error, projects)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      if (projects.length === 0) {
        res.locals.data = {status, statistics: {newest: {}, oldest: {}}};
        return next();
      }
      let newest = last(projects);
      newest = {name: last(sortBy(newest.scopes, 'created_at')).name, _id: newest._id};
      let oldest = head(projects);
      oldest = {name: last(sortBy(oldest.scopes, 'created_at')).name, _id: oldest._id};
      let statistics = {newest, oldest};

      res.locals.data = {status, statistics};
      next();
    });
}

export function fetchById(req, res, next) {
  Project.findById(req.params.id)
    .populate({
      path: 'users.user',
      select: '_id first_name last_name email avatar',
      populate: 'role payment_plan'
    })
    .populate('users.role emotion status scopes.platforms')
    .populate({
      path: 'scopes.created_by',
      select: '_id first_name last_name email avatar'
    })
    .populate({
      path: 'scopes.devices',
      populate: {
        path: 'platform',
        model: 'Platform'
      }
    })
    .populate({
      path: 'owner',
      select: '_id first_name last_name email avatar',
      populate: 'role payment_plan'
    })
    .exec((error, project)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({project});
    });
}

export function changeOrder(req, res, next) {
  let newOrderProjects = req.body.projects;
  Project.find({_id: {$in: newOrderProjects.map(elem=>elem.project)}})
    .exec((error, projects)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      async.each(
        projects,
        (project, callback)=> {
          let element = newOrderProjects.find(elem=>elem.project === project._id.toString());
          let user = project.users.find(user=>user.user.id === req.user._id.id);
          if (user) {
            user.order = element.order;
            project.save(error=> {
              if (error) {
                return callback(error);
              }
              callback();
            });
          } else {
            callback(new Error('Oops, something went wrong..'));
          }
        },
        error=> {
          if (error) {
            console.error(error);
            return next(error);
          }
          res.end();
        }
      );
    });
}
