import {mail} from '../config/secrets';
import sendMail from '../utils/mail';

export function contact(req, res, next) {
  sendMail({
    to: mail.address,
    subject: req.body.topic,
    text: req.body.message,
    from: {
      name: req.body.name,
      address: req.body.email
    }
  })
    .then(()=> {
      res.json({message: 'Your message has been sent.'});
    })
    .catch(error=> {
      console.error(error);
      return next(error);
    });
}
