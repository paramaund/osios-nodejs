import async from 'async';
import Company from '../models/company';
import Project from '../models/project';

export function fetch(req, res, next) {
  let projectId = req.query.project;
  let defaultProject = {users: []};
  async.waterfall([
      callback=> {
        if (!projectId) {
          return callback(null, defaultProject);
        }
        Project.findById(projectId)
          .exec((error, project)=> {
            if (error) {
              return callback(error);
            }
            callback(null, project || defaultProject);
          });
      },
      (project, callback)=> {
        let projectUsersIds = project.users.map(user=>user.user);
        let searchOptions = {owner: req.user};
        if (projectId) {
          searchOptions = Object.assign(searchOptions, {users: {$in: projectUsersIds}});
        }
        Company.find(searchOptions)
          .populate({
            path: 'users',
            match: {_id: {$in: projectUsersIds}},
            select: '_id first_name last_name email avatar'
          })
          .exec((error, companies)=> {
            if (error) {
              return callback(error);
            }
            callback(null, companies);
          });
      }
    ],
    (error, companies)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({companies});
    }
  );
}

export function create(req, res, next) {
  req.body.owner = req.user;
  Company.create(req.body, (error, company)=> {
    if (error) {
      console.error(error);
      return next(error);
    }
    res.json({message: 'New company has been successfully created.', company});
  });
}
