import async from 'async';
import RoomPermission from '../models/room_permission';

export function fetch(req, res, next) {
  RoomPermission.find()
    .exec((error, permissions)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({permissions});
    });
}
