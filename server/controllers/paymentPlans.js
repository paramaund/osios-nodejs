import PaymentPlan from '../models/payment_plan';

export function create(req, res, next) {
  PaymentPlan.create(req.body, error=> {
    if (error) {
      console.error(error);
      return next(error);
    }
    res.json({message: 'Plan has been successfully created!'});
  });
}

export function fetch(req, res, next) {
  PaymentPlan.find({isActive: true})
    .exec((error, plans)=> {
      if (error) {
        console.error(error);
        return next(error);
      }
      res.json({plans: plans});
    });
}

