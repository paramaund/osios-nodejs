import 'babel-polyfill';
import http from 'http';
import https from 'https';
import express from 'express';
import passport from 'passport';
import webpack from 'webpack';
import config from '../webpack/webpack.config.dev.js';
import secrets from './config/secrets';
import fs from 'fs';
import path from 'path';

let app = express();
let compiler = webpack(config);

let isDev = process.env.NODE_ENV === 'development';

if (isDev) {
  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
  }));

  app.use(require('webpack-hot-middleware')(compiler));
}

// Bootstrap passport config
require('./config/passport')(app, passport);

// Bootstrap application settings
require('./config/express')(app, passport);

// Bootstrap routes
require('./config/routes')(app, passport);


// error handlers

app.use((err, req, res, next)=> {
  switch (err.name) {
    case 'ValidationError':
    {
      let errorsFields = Object.keys(err.errors);
      if (errorsFields.length > 0) {
        return next(err.errors[errorsFields[0]]);
      }
    }
      break;
    case 'MongoError':
    {
      if (err.code === 11000 && err.message.indexOf('email') !== -1) {
        err.message = 'This email is already in use!';
        return next(err);
      }
    }
      break;
    default:
      next(err);
  }
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});

if (app.get('env') === 'development') {
  app.set('protocol', 'http://');
  http.createServer(app).listen(app.get('port'));
} else {
  app.set('protocol', 'https://');
  let httpsOptions = {
    key: fs.readFileSync(path.join(__dirname, '../server/config/ssl/key.key')),
    cert: fs.readFileSync(path.join(__dirname, '../server/config/ssl/cert.crt')),
  };
  https.createServer(httpsOptions, app).listen(443);
}
// app.listen(app.get('port'));
