import nodemailer from 'nodemailer';
import isObject from 'lodash/isObject';
import {mail} from '../config/secrets';

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport();

export default function sendMail(options) {
  if (!options || !isObject(options)) {
    return Promise.reject(new Error('Mail options are required.'));
  }
  let from = {
    name: mail.name,
    address: mail.address
  };
  if (options.from) {
    from = options.from;
  }
// setup e-mail data with unicode symbols
  let mailOptions = {
    from: from,// sender address
    to: options.to, // list of receivers
    subject: options.subject, // Subject line
    text: options.text, // plaintext body
    html: options.html // html body
  };

// send mail with defined transport object
  return new Promise((resolve, reject)=> {
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return reject(error);
      }
      console.log('Message sent! ');
      resolve();
    });
  })
}
