import requestPromise from 'request-promise';
import async from 'async';
import FormData from 'form-data';
import {moxtra} from '../config/secrets';

const request = requestPromise.defaults({
  strictSSL: false,
  rejectUnauthorized: false
});

export function createMoxtraBinder(name, token) {
  let options = {
    name
  };
  return request({
    method: 'POST',
    qs: {
      access_token: token
    },
    uri: moxtra.apiUrl + '/v1/me/binders',
    body: options,
    json: true
  });
}

export function getMoxtraUserAccessToken(uniqueId, firstName, lastName) {
  if (!uniqueId) {
    return Promise.reject(new Error('User\'s moxtra unique id is required! Please contact our administrator.'));
  }

  let options = {
    client_id: moxtra.clientId,
    client_secret: moxtra.clientSecret,
    grant_type: 'http://www.moxtra.com/auth_uniqueid',
    uniqueid: uniqueId,
    timestamp: Date.now().valueOf()
  };
  if (firstName) {
    options.firstname = firstName;
  }
  if (lastName) {
    options.lastname = lastName;
  }
  return request({
    method: 'POST',
    uri: moxtra.apiUrl + '/oauth/token',
    form: options,
    json: true
  });
}

export function uploadUserProfilePicture(uniqueId, imgUrl, hostName) {
  if (!uniqueId) {
    return Promise.reject(new Error('User\'s moxtra unique id is required! Please contact our administrator.'));
  }
  return new Promise((resolve, reject)=> {
    async.waterfall([
        callback2=> {
          // TO DO: fix image loading
          request({
            method: 'GET',
            uri: hostName + imgUrl
          }).then(response=>callback2(null, response))
            .catch(error=>callback2(error));
        },
        (file, callback2)=> {
          getMoxtraUserAccessToken(uniqueId)
            .then(response=>callback2(null, response.access_token, file))
            .catch(error=>callback2(error));
        },
        (token, file, callback2)=> {
          let data = new FormData();
          data.append('file', file);
          callback2(null, request({
            method: 'POST',
            qs: {
              access_token: token
            },
            uri: moxtra.apiUrl + '/v1/me/picture',
            form: data,
            json: true
          }));
        }
      ],
      (error, promise)=> {
        if (error) {
          return reject(error);
        }
        promise
          .then(response=>resolve(response))
          .catch(error=>reject(error));
      }
    );
  });
}
