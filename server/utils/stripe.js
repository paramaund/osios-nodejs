import moment from 'moment';
import stripeApi from 'stripe';
import isObject from 'lodash/isObject';
import {stripe as stripeSecrets} from '../config/secrets';

const stripe = stripeApi(stripeSecrets.sKey);

export function createCustomer(options) {
  if (!isObject(options)) {
    return Promise.reject(new Error('Options object is required!'));
  }
  return new Promise((resolve, reject)=> {
    stripe.customers.create({
      email: options.user.email,
      description: options.user.email,
      source: options.token // obtained with Stripe.js
    }, (err, customer) => {
      if (err) {
        return reject(err);
      }
      resolve(customer);
    });
  });
}

export function createSubscription(options) {
  if (!isObject(options)) {
    return Promise.reject(new Error('Options object is required!'));
  }
  let trialEnd = moment().add(60, 'days');
  let subscrOptions = {
    customer: options.customer.id,
    plan: options.plan,
    trial_end: trialEnd.unix(),
    source: options.source
  };
  if (options.tax) {
    subscrOptions.tax_percent = 20;
  }
  return new Promise((resolve, reject)=> {
    stripe.subscriptions.create(subscrOptions, (err, subscription) => {
        if (err) {
          return reject(err);
        }
        resolve({subscription, trial_end: trialEnd});
      }
    );
  });
}

export function createToken(options) {
  if (!isObject(options)) {
    return Promise.reject(new Error('Options object is required!'));
  }
  return new Promise((resolve, reject)=> {
    stripe.tokens.create({
      card: {
        number: options.card,
        exp_month: options.month,
        exp_year: options.year,
        cvc: options.cvc
      }
    }, (err, token) => {
      if (err) {
        return reject(err);
      }
      resolve(token);
    });
  });
}

export function retrieveCustomer(customerID) {
  if (!customerID) {
    return Promise.reject(new Error('Customer id is required!'));
  }
  return new Promise((resolve, reject)=> {
    stripe.customers.retrieve(
      customerID,
      function (err, customer) {
        if (err) {
          return reject(err);
        }
        return resolve(customer);
      }
    );
  });
}
