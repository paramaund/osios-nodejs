import omit from 'lodash/omit';

export function removeUserHiddenFields(user) {
  let result = Object.assign({}, user.toJSON());
  result = omit(result, ['password', 'stripe_profile', 'payment_profile', 'access']);
  return result;
}
