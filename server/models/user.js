import mongoose from 'mongoose';
// import bcrypt from 'bcrypt';
import hasher from 'wordpress-hash-node';
const {Schema} = mongoose;
const {ObjectId} = Schema.Types;

const UserSchema = new Schema({
  email: {type: String, required: true, trim: true, unique: true},
  password: {type: String, required: true, trim: true},
  first_name: {type: String, trim: true},
  last_name: {type: String, trim: true},
  role: {type: ObjectId, ref: 'Role', required: true},
  access: {type: Boolean, default: false},
  avatar: {type: String},
  company_name: {type: String},
  company_logo: {type: String},
  timezone: {type: String},
  phone: {type: String},
  skype: {type: String},
  moxtra_unique_id: {type: String, unique: true},
  notifications: [{type: Number}],
  stripe_profile: {type: String},
  payment_profile: {type: String},
  payment_plan: {type: ObjectId, ref: 'PaymentPlan'},
  payment_expiry: {type: Date, default: Date.now()},
  created_at: {type: Date, default: Date.now()}
});

UserSchema.pre('save', function (next) {
  let user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  let hash = hasher.HashPassword(user.password);
  // override the cleartext password with the hashed one
  user.password = hash;
  next();
});

UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  let isMatch = hasher.CheckPassword(candidatePassword, this.password);
  cb(null, isMatch);
};


const User = mongoose.model('User', UserSchema);

export default User;
