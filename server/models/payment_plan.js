import mongoose from 'mongoose';
const {Schema} = mongoose;

const PaymentPlanSchema = new Schema({
  name: {type: String, required: true, trim: true},
  key: {type: String, required: true, unique: true, trim: true},
  description: {type: String},
  price: {type: Number, default: 0},
  projects: {type: Number, default: 1},
  storage_space: {type: Number, default: 5},
  isActive: {type: Boolean, default: true}
});

export default mongoose.model('PaymentPlan', PaymentPlanSchema);
