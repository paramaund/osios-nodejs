import mongoose from 'mongoose';
const {Schema} = mongoose;

const RoomPermissionSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: true
  }
});

export default mongoose.model('RoomPermission', RoomPermissionSchema);
