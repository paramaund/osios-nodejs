import mongoose from 'mongoose';
const {Schema} = mongoose;
const {ObjectId} = Schema.Types;

const ScreenSchema = new Schema({
  name: {type: String, required: true, trim: true},
  orientation: {type: Boolean, default: false},
  position: {type: Number},
  history: [
    {
      image: {type: String},
      author: {type: ObjectId, ref: 'User'},
      created_at: {type: Date, default: Date.now()}
    }
  ],
  links: [
    {
      axisx: {type: Number, default: 0},
      axisy: {type: Number, default: 0},
      width: {type: Number, default: 0},
      height: {type: Number, default: 0},
      title: {type: String, required: true}
    }
  ],
  comments: [
    {
      text: {type: String},
      author: {type: ObjectId, ref: 'User', required: true},
      created_at: {type: Date, default: Date.now()},
      file: {
        name: {type: String, required: true},
        url: {type: String, required: true},
        size: {type: Number, default: 0}
      }
    }
  ]
});


export default mongoose.model('Screen', ScreenSchema);
