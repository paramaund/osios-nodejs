import mongoose from 'mongoose';
const {Schema} = mongoose;
const {ObjectId} = Schema.Types;

const DeviceSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: true
  },
  key: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true
  },
  platform: {
    type: ObjectId,
    ref: 'Platform',
    required: true
  }
});

export default mongoose.model('Device', DeviceSchema);
