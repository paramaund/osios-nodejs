import mongoose from 'mongoose';
const {Schema} = mongoose;
const {ObjectId} = Schema.Types;

const MilestoneSchema = new Schema({
  name: {type: String, required: true, trim: true},
  assigned_to: {type: ObjectId, ref: 'User'},
  due_date: {type: Date, default: Date.now()},
  start_date: {type: Date},
  status: {type: ObjectId, ref: 'MilestoneStatus', required: true},
  created_at: {type: Date, default: Date.now()},
  tasks: [{type: ObjectId, ref: 'Task'}]
});

export default mongoose.model('Milestone', MilestoneSchema);
