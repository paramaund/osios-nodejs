import mongoose from 'mongoose';
const {Schema} = mongoose;

const EmotionSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: true
  }
});

export default mongoose.model('Emotion', EmotionSchema);
