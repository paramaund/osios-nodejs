import mongoose from 'mongoose';
const {Schema} = mongoose;
const {ObjectId} = Schema.Types;

const RoomSchema = new Schema({
  name: {type: String, required: true, trim: true},
  author: {type: ObjectId, ref: 'User'},
  project: {type: ObjectId, ref: 'Project'},
  moxtra_binder_id: {type: String, required: true},
  users: [
    {
      permission: {type: ObjectId, ref: 'RoomPermission', required: true},
      user: {type: ObjectId, ref: 'User', required: true},
      order: {type: Number, default: 0}
    }
  ],
  scheduled_meets: [
    {type: Number}
  ],
  created_at: {type: Date, default: Date.now()}
});

export default mongoose.model('Room', RoomSchema);
