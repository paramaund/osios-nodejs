import mongoose from 'mongoose';
const {Schema} = mongoose;

const MilestoneStatusSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: true
  }
});

export default mongoose.model('MilestoneStatus', MilestoneStatusSchema);
