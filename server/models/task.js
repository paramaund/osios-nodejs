import mongoose from 'mongoose';
const {Schema} = mongoose;
const {ObjectId} = Schema.Types;

const TaskSchema = new Schema({
  name: {type: String, required: true, trim: true},
  description: {type: String},
  author: {type: ObjectId, ref: 'User'},
  due_date: {type: Date, default: Date.now()},
  start_date: {type: Date},
  status: {type: ObjectId, ref: 'MilestoneStatus', required: true},
  created_at: {type: Date, default: Date.now()},
  issue: {
    text: {type: String},
    updated_at: {
      type: Date, default: Date.now(),
      author: {type: ObjectId, ref: 'User'}
    }
  }
});

export default mongoose.model('Task', TaskSchema);
