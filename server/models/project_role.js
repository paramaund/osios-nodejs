import mongoose from 'mongoose';
const {Schema} = mongoose;

const ProjectRoleSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: true
  }
});

export default mongoose.model('ProjectRole', ProjectRoleSchema);
