import mongoose from 'mongoose';
const {Schema} = mongoose;
const {ObjectId} = Schema.Types;

const ProjectSchema = new Schema({
  owner: {type: ObjectId, ref: 'User'},
  emotion: {type: ObjectId, ref: 'Emotion', required: true},
  users: [
    {
      role: {type: ObjectId, ref: 'ProjectRole', required: true},
      user: {type: ObjectId, ref: 'User', required: true},
      order: {type: Number, default: 0}
    }
  ],
  status: {type: ObjectId, ref: 'ProjectStatus', required: true},
  created_at: {type: Date, default: Date.now()},
  deleted_at: {type: Date},
  deleted_by: {type: ObjectId, ref: 'User'},
  deleted_reason: {type: String},
  icon: {type: String},
  milestones: [{type: ObjectId, ref: 'Milestone'}],
  screens: [{type: ObjectId, ref: 'Screen'}],
  scopes: [
    {
      created_at: {type: Date, default: Date.now()},
      created_by: {type: ObjectId, ref: 'User'},
      name: {type: String, required: true, trim: true},
      platforms: [{type: ObjectId, ref: 'Platform'}],
      devices: [{type: ObjectId, ref: 'Device'}],
      description: {type: String, trim: true},
      intended_users: {type: String, trim: true},
      design_direction: {type: String, trim: true},
      colors: [{type: String, trim: true}],
      fonts: {type: String, trim: true},
      tech_requirements: {type: String, trim: true},
      notes: {type: String, trim: true},
      files: [{
        name: {type: String, required: true},
        url: {type: String, required: true},
        size: {type: Number, default: 0}
      }]
    }
  ]
});

export default mongoose.model('Project', ProjectSchema);
