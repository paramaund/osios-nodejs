import mongoose from 'mongoose';
const {Schema} = mongoose;
const {ObjectId} = Schema.Types;

const CompanySchema = new Schema({
  name: {type: String, required: true, trim: true},
  description: {type: String},
  owner: {type: ObjectId, ref: 'User', required: true},
  users: [{type: ObjectId, ref: 'User'}]
});

export default mongoose.model('Company', CompanySchema);
