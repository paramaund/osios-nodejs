import mongoose from 'mongoose';
const {Schema} = mongoose;

const ProjectStatusSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    unique: true
  }
});

export default mongoose.model('ProjectStatus', ProjectStatusSchema);
