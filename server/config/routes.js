/**
 * Routes for express app
 */
import App from '../../public/assets/app.server';
import * as auth from '../controllers/auth';
import users from './routers/users';
import subscribe from './routers/subscribe';
import projects from './routers/projects';
import companies from './routers/companies';
import paymentPlans from './routers/paymentPlans';
import rooms from './routers/rooms';
import * as files from '../controllers/files';
import * as messages from '../controllers/messages';
import * as statuses from '../controllers/statuses';
import * as platforms from '../controllers/platforms';
import * as devices from '../controllers/devices';
import * as permissions from '../controllers/permissions';
import multer from 'multer';
import async from 'async';
import usersMigrationHandler from '../migration_scripts/users';
import rolesMigrationHandler from '../migration_scripts/roles';
import projectRolesMigrationHandler from '../migration_scripts/projectRoles';
import projectStatusesMigrationHandler from '../migration_scripts/projectStatuses';
import devicesMigrationHandler from '../migration_scripts/devices';
import emotionsMigrationHandler from '../migration_scripts/emotions';
import projectsMigrationHandler from '../migration_scripts/projects';
import companiesMigrationHandler from '../migration_scripts/companies';
import paymentPlansMigrationHandler from '../migration_scripts/paymentPlans';

module.exports = (app, passport) => {
  // This is where the magic happens. We take the locals data we have already
  // fetched and seed our stores with data.
  // App is a function that requires store data and url to initialize and return the React-rendered html string

  app.get('/api/setup', (req, res, next)=> {
    async.waterfall([
        callback=> {
          rolesMigrationHandler()
            .then(()=>callback())
            .catch(error=>callback(error));
        },
        callback=> {
          projectRolesMigrationHandler()
            .then(()=>callback())
            .catch(error=>callback(error));
        },
        callback=> {
          projectStatusesMigrationHandler()
            .then(()=>callback())
            .catch(error=>callback(error));
        },
        callback=> {
          devicesMigrationHandler()
            .then(()=>callback())
            .catch(error=>callback(error));
        },
        callback=> {
          emotionsMigrationHandler()
            .then(()=>callback())
            .catch(error=>callback(error));
        },
        callback=> {
          paymentPlansMigrationHandler()
            .then(()=>callback())
            .catch(error=>callback(error));
        },
        callback=> {
          usersMigrationHandler()
            .then(()=>callback())
            .catch(error=>callback(error));
        },
        callback=> {
          companiesMigrationHandler()
            .then(()=>callback())
            .catch(error=>callback(error));
        },
        callback=> {
          projectsMigrationHandler()
            .then(()=>callback())
            .catch(error=>callback(error));
        }
      ],
      error=> {
        if (error) {
          console.error(error);
          return next(error);
        }
        res.end('migrations done!');
      });
  });

  app.post('/login', auth.login);
  app.get('/logout', auth.ensureAuthenticated, auth.logout);
  app.post('/api/contact', messages.contact);
  app.get('/api/statuses', statuses.fetch);
  app.get('/api/platforms', platforms.fetch);
  app.get('/api/devices', devices.fetch);
  app.get('/api/permissions', permissions.fetch);
  app.use('/api', users);
  app.use('/api', paymentPlans);
  app.use('/api', auth.ensureAuthenticated, rooms);
  app.use('/api', auth.ensureAuthenticated, projects);
  app.use('/api', auth.ensureAuthenticated, subscribe);
  app.use('/api', auth.ensureAuthenticated, companies);
  app.post('/api/uploads/crop', auth.ensureAuthenticated, multer({
    dest: './temp/',
    fileFilter: (req, file, cb)=> {
      let acceptedExtentions = ['.jpg', '.jpeg', '.png', '.ico'];
      if (acceptedExtentions.some(ext=>file.originalname.endsWith(ext))) {
        return cb(null, true);
      }
      cb(null, false);
    }
  }).single('file'), files.crop);
  app.post('/api/uploads/files', auth.ensureAuthenticated, multer({
    dest: './temp/',
    // fileFilter: (req, file, cb)=> {
    //   let acceptedExtentions = ['.jpg', '.jpeg', '.png', '.psd', '.pdf'];
    //   if (acceptedExtentions.some(ext=>file.originalname.endsWith(ext))) {
    //     return cb(null, true);
    //   }
    //   cb(null, false);
    // }
  }).array('files', 5), files.upload);

  app.get('*', (req, res, next) => {
    App(req, res);
  });
};
