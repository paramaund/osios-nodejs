/*
 Configuring local strategy to authenticate strategies
 Code modified from : https://github.com/madhums/node-express-mongoose-demo/blob/master/config/passport/local.js
 */

import {Strategy as LocalStrategy} from 'passport-local';
import User from '../../models/user';

/*
 By default, LocalStrategy expects to find credentials in parameters named username and password.
 If your site prefers to name these fields differently, options are available to change the defaults.
 */
module.exports = new LocalStrategy({
  usernameField: 'email'
}, (email, password, done)=> {
  User.findOne({email: email})
    .populate('role')
    .populate('payment_plan')
    .exec((err, user) => {
      if (err) {
        console.error(err);
        return done(err);
      }
      if (!user) {
        return done(null, false, {message: 'Incorrect email.'});
      }
      user.comparePassword(password, (err, ismatch)=> {
        if (err) {
          console.error(err);
          return done(err);
        }
        if (!ismatch) {
          return done(null, false, {message: 'Incorrect password.'});
        }
        return done(null, user);
      });
    });
});
