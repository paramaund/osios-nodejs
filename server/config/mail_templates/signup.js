const template = `
<html lang="en">
<head>
  <meta charset="UTF-8">
</head>
<body>
<p>Thanks for joining us-
<strong>Your mobile projects just got a lot lot easier-</strong></p>

<div>Your login email: [email]</div>
<div>Your login password: [password]</div>
<div>Your login URL: <a href="[loginUrl]">[loginUrl]/</a>.</div>

<p>Industry isights, OSIOS tips and unique bonuses will be sent to you over the coming days. Your first message is on it's
way already. Please check your email for more details (please check your email folders including your junk folder if you
can’t find them).</p>

<div>Simon</div>
<div>OSIOS Co-founder</div>
</body>
</html>`;

export default template;
