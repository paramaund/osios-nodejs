/* Initializing passport.js */
import User from '../models/user';
import local from './passport/local';
import async from 'async';
import {getTotalActiveProjectsCount} from '../controllers/projects';

/*
 * Expose
 */
module.exports = (app, passport, config) => {
  // Configure Passport authenticated session persistence.
  //
  // In order to restore authentication state across HTTP requests, Passport needs
  // to serialize users into and deserialize users out of the session.  The
  // typical implementation of this is as simple as supplying the user ID when
  // serializing, and querying the user record by ID from the database when
  // deserializing.
  passport.serializeUser((user, done) => {
    // console.log('serialize');
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    async.parallel([
        callback=> {
          getTotalActiveProjectsCount(id)
            .then(count=>callback(null, count))
            .catch(error=>callback(error));
        },
        callback=> {
          User.findById(id, '-password')
            .populate('role')
            .populate('payment_plan')
            .exec((err, user) => {
              if (err) {
                return callback(err);
              }
              callback(null, user);
            });
        }
      ],
      (error, results)=> {
        if (error) {
          console.error(error);
          return done(error);
        }
        done(null, Object.assign(results[1], {totalProjects: results[0]}));
      });
  });

  // use the following strategies
  passport.use(local);
};
