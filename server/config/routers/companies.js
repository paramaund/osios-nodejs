import express from 'express';
import * as companies from '../../controllers/companies';

let router = express.Router();

router.route('/companies')
  .post(companies.create)
  .get(companies.fetch);

export default router;
