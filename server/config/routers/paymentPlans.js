import express from 'express';
import * as plans from '../../controllers/paymentPlans';

let router = express.Router();

router.route('/payment-plans')
  .post(plans.create)
  .get(plans.fetch);

export default router;
