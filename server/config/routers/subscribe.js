import express from 'express';
import * as stripe from '../../controllers/stripe';

let router = express.Router();

router.route('/subscribe')
  .post(stripe.getToken, stripe.subscribeStripe);

export default router;
