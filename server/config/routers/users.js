import express from 'express';
import * as stripe from '../../controllers/stripe';
import * as users from '../../controllers/users';
import * as auth from '../../controllers/auth';

let router = express.Router();

router.route('/users')
  .post(stripe.getToken, users.create, auth.ensureAuthenticated, stripe.subscribeStripe)
  .get(auth.ensureAuthenticated, users.fetch);

router.route('/users/:id')
  .get(auth.ensureAuthenticated, users.fetchById)
  .put(auth.ensureAuthenticated, users.updateById)
  .delete(auth.ensureAuthenticated, users.deleteById);

router.route('/users/:id/statistics')
  .get(auth.ensureAuthenticated, users.getStatistics);

export default router;
