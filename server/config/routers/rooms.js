import express from 'express';
import * as rooms from '../../controllers/rooms';

let router = express.Router();

router.route('/rooms')
  .get(rooms.fetch)
  .post(rooms.create);

router.route('/rooms/order')
  .post(rooms.changeOrder);

router.route('/rooms/:id')
  .get(rooms.fetchOne)
  .delete(rooms.remove);

export default router;
