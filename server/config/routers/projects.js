import express from 'express';
import * as projects from '../../controllers/projects';

let router = express.Router();

router.route('/projects')
  .get(projects.fetch)
  .post(projects.mapFiles, projects.create);

router.route('/projects/statistics')
  .get(projects.fetchStatistics, projects.addTotalActiveProjectsCount);

router.route('/projects/order')
  .post(projects.changeOrder);

router.route('/projects/:id')
  .put(projects.mapFiles, projects.update)
  .get(projects.fetchById);

export default router;
