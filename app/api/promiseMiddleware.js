import {ACTION_REQUEST, ACTION_SUCCESS, ACTION_FAILURE} from '../constants/index';

/**
 * Redux middleware to handle promises
 * As seen in: https://github.com/caljrimmer/isomorphic-redux-app
 */
export default function promiseMiddleware() {
  return next => action => {
    const {type, payload, ...restAction} = action;

    if (!payload) return next(action);
    const {promise, ...restPayload} = payload;
    if (!promise) return next(action);

    const SUCCESS = type + ACTION_SUCCESS;
    const REQUEST = type + ACTION_REQUEST;
    const FAILURE = type + ACTION_FAILURE;
    next({...restAction, type: REQUEST, payload: {...restPayload}});
    return promise
      .then(res => {
        next({...restAction, payload: res.data, type: SUCCESS});
        return true;
      })
      .catch(error => {
        next({...restAction, error: true, payload: error, type: FAILURE});
        console.log(error);
        return false;
      });
  };
}
