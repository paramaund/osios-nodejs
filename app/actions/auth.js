// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import {makeRequest} from '../api/server';
import {createAction} from '../utils/actions';
import {push} from 'react-router-redux';
import isObject from 'lodash/isObject';
import * as users from './users';

export const loginRequest = createAction('USER_LOGIN_REQUEST');
export const loginSuccess = createAction('USER_LOGIN_SUCCESS');
export const loginFailure = createAction('USER_LOGIN_FAILURE', data=> {
  return isObject(data) ? data : {message: data};
});

export const logoutRequest = createAction('USER_LOGOUT_REQUEST');
export const logoutSuccess = createAction('USER_LOGOUT_SUCCESS');
export const logoutFailure = createAction('USER_LOGOUT_FAILURE', data=> {
  return isObject(data) ? data : {message: data};
});

export function login(user) {
  return dispatch => {
    dispatch(loginRequest());

    return makeRequest('post', '/login', user)
      .then(response => {
        dispatch(loginSuccess());
        dispatch(users.fetchMeInfo.request());
        dispatch(users.fetchMeInfo.success(response.data));
        dispatch(push('/'));
      })
      .catch(err => {
        if (err.data && err.data.message) {
          dispatch(loginFailure(err.data.message));
        } else {
          dispatch(loginFailure('Oops! Something went wrong!'));
        }
      });
  };
}

export function logout() {
  return dispatch => {
    dispatch(logoutRequest());

    return makeRequest('get', '/logout')
      .then(() => {
        dispatch(logoutSuccess());
        // dispatch(users.fetchMeInfo.request());
        dispatch(push('/login'));
      })
      .catch(err => {
        if (err.data && err.data.message) {
          dispatch(logoutFailure(err.data.message));
        } else {
          dispatch(logoutFailure('Oops! Something went wrong!'));
        }
      });
  };
}