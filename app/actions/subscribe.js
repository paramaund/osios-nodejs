// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import {makeRequest} from '../api/server';
import {createActionAsync} from '../utils/actions';

const API_ENDPOINT = '/api/subscribe';

// export const updateMe = createActionAsync('USER_UPDATE_CURRENT', makeRequest.bind(null, 'put', API_ENDPOINT + '/me'));
export const create = createActionAsync('CREATE_SUBSCRIPTION', makeRequest.bind(null, 'post', API_ENDPOINT));
