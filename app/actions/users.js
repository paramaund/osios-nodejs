// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import {makeRequest} from '../api/server';
import * as auth from '../actions/auth';
import {push} from 'react-router-redux';
import isObject from 'lodash/isObject';
import {createActionAsync, createAction} from '../utils/actions';

const API_ENDPOINT = '/api/users';

export const confirmSuccess = createAction('CONFIRM_SUCCESS');

export const createRequest = createAction('USER_CREATE_REQUEST');
export const createSuccess = createAction('USER_CREATE_SUCCESS');
export const createFailure = createAction('USER_CREATE_FAILURE', data=> {
  return isObject(data) ? data : {message: data};
});

// Note this can be extracted out later
/**
 * Utility function to make AJAX requests using isomorphic fetch.
 * You can also use jquery's $.ajax({}) if you do not want to use the
 * /fetch API.
 * @param method Object Data you wish to pass to the server
 * @param data String HTTP method, e.g. post, get, put, delete
 * @param id String id
 * @return Promise
 */

export const updateMe = createActionAsync('USER_UPDATE_CURRENT', makeRequest.bind(null, 'put', API_ENDPOINT + '/me'));
export const fetchMeInfo = createActionAsync('USER_FETCH_CURRENT', makeRequest.bind(null, 'get', API_ENDPOINT + '/me'));
export const fetch = createActionAsync('USERS_FETCH', makeRequest.bind(null, 'get', API_ENDPOINT));
export const deleteMe = createActionAsync('USER_DELETE_CURRENT', makeRequest.bind(null, 'delete', API_ENDPOINT + '/me'));
export const getStatisticsMe = createActionAsync('USER_STATISTICS_CURRENT', makeRequest.bind(null, 'get', API_ENDPOINT + '/me/statistics'));

export function create(user) {
  return dispatch => {
    dispatch(createRequest());

    return makeRequest('post', API_ENDPOINT, user)
      .then(response => {
        dispatch(createSuccess(response.data));
        if (!user.company) {
          dispatch(auth.loginRequest());
          dispatch(auth.loginSuccess());
          dispatch(fetchMeInfo.request());
          dispatch(fetchMeInfo.success(response.data));
          dispatch(push('/'));
        }
      })
      .catch(err => {
        let msg = 'Oops! Something went wrong!';
        if (err.data && err.data.message) {
          msg = err.data.message;
        }
        dispatch(createFailure(msg));
      });
  };
}


