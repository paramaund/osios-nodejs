// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import {makeRequest} from '../api/server';
import {createActionAsync} from '../utils/actions';

const API_ENDPOINT = '/api/payment-plans';

// export const updateMe = createActionAsync('USER_UPDATE_CURRENT', makeRequest.bind(null, 'put', API_ENDPOINT + '/me'));
export const fetch = createActionAsync('FETCH_PAYMENT_PLANS', makeRequest.bind(null, 'get', API_ENDPOINT));
