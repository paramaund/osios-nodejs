// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import {makeRequest} from '../api/server';
import {createActionAsync, createAction} from '../utils/actions';

const API_ENDPOINT = '/api/projects';

export const currentProjectFieldChange = createAction('CURRENT_PROJECT_FIELD_CHANGE');
export const currentProjectScopeFieldChange = createAction('CURRENT_PROJECT_SCOPE_FIELD_CHANGE');

export const create = createActionAsync('PROJECT_CREATE', makeRequest.bind(null, 'post', API_ENDPOINT));

export const update = createActionAsync('PROJECT_UPDATE', makeRequest.bind(null, 'put'));
export function updateById(id, data, options) {
  return update(API_ENDPOINT + '/' + id, data, options);
}

export const changeOrder = createActionAsync('CHANGE_PROJECTS_ORDER', makeRequest.bind(null, 'post', '/api/projects/order'));
export const fetchStatuses = createActionAsync('FETCH_PROJECT_STATUSES', makeRequest.bind(null, 'get', '/api/statuses'));
export const fetch = createActionAsync('FETCH_PROJECTS', makeRequest.bind(null, 'get', API_ENDPOINT, null));
export function fetchByParams(params) {
  return fetch({params});
}
export const fetchOne = createActionAsync('FETCH_PROJECT', makeRequest.bind(null, 'get'));
export function fetchById(id) {
  return fetchOne(API_ENDPOINT + '/' + id);
}

export const fetchStatistics = createActionAsync('FETCH_PROJECTS_STATISTICS', makeRequest.bind(null, 'get', API_ENDPOINT + '/statistics', null));
export function fetchProjectsStatistics(params) {
  return fetchStatistics({params});
}
export const currentProjectFileUpload = createActionAsync('CURRENT_PROJECT_FILE_UPLOAD', makeRequest.bind(null, 'post', '/api/uploads/files'));
