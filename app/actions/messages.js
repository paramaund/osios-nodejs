// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import {makeRequest} from '../api/server';
import {createActionAsync} from '../utils/actions';

// If this is a test, we must use an absolute url
const API_ENDPOINT = '/api/contact';

export const contact = createActionAsync('CONTACT_US', makeRequest.bind(null, 'post', API_ENDPOINT));
