// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import {makeRequest} from '../api/server';
import {createActionAsync} from '../utils/actions';

const API_ENDPOINT = '/api/devices';

export const fetch = createActionAsync('FETCH_DEVICES', makeRequest.bind(null, 'get', API_ENDPOINT));
