// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import {makeRequest} from '../api/server';
import {createActionAsync, createAction} from '../utils/actions';

const API_ENDPOINT = '/api/rooms';

export const confirmSuccess = createAction('CONFIRM_SUCCESS');

export const create = createActionAsync('ROOM_CREATE', makeRequest.bind(null, 'post', API_ENDPOINT));
export const remove = createActionAsync('ROOM_DELETE', makeRequest.bind(null, 'delete'));
export function deleteById(id) {
  return remove(`${API_ENDPOINT}/${id}`);
}

export const update = createActionAsync('ROOM_UPDATE', makeRequest.bind(null, 'put'));
export function updateById(id, data, options) {
  return update(API_ENDPOINT + '/' + id, data, options);
}

export const changeOrder = createActionAsync('CHANGE_ROOMS_ORDER', makeRequest.bind(null, 'post', API_ENDPOINT + '/order'));
export const fetch = createActionAsync('FETCH_ROOMS', makeRequest.bind(null, 'get', API_ENDPOINT, null));
export function fetchByProject(params) {
  return fetch({params});
}

export const fetchOne = createActionAsync('FETCH_ROOM', makeRequest.bind(null, 'get'));
export function fetchById(id) {
  return fetchOne(API_ENDPOINT + '/' + id);
}

export const fetchPermissions = createActionAsync('FETCH_ROOMS_PERMISSIONS', makeRequest.bind(null, 'get', '/api/permissions', null));
// export function fetchProjectsStatistics(params) {
//   return fetchStatistics({params});
// }
// export const currentProjectFileUpload = createActionAsync('CURRENT_PROJECT_FILE_UPLOAD', makeRequest.bind(null, 'post', '/api/uploads/files'));
