// Including es6-promise so isomorphic fetch will work
import 'es6-promise';
import {makeRequest} from '../api/server';
import {createActionAsync} from '../utils/actions';

const API_ENDPOINT = '/api/companies';

export const fetch = createActionAsync('FETCH_COMPANIES', makeRequest.bind(null, 'get', API_ENDPOINT));
export const create = createActionAsync('CREATE_COMPANIES', makeRequest.bind(null, 'post', API_ENDPOINT));

export function fetchByProject(params) {
  return fetch(null, {params});
}
