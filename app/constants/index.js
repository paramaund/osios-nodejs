export const TEST_URL = 'http://localhost:3000';

export const ACTION_REQUEST = '_REQUEST';
export const ACTION_SUCCESS = '_SUCCESS';
export const ACTION_FAILURE = '_FAILURE';

export const ISBROWSER = typeof window !== 'undefined';
export const HOST = ISBROWSER ? window.location.origin : '';