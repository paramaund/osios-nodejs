import {createAction as actionCreate} from 'redux-actions';
import isObject from 'lodash/isObject';

export function createAction(type, callbackFunc = payload=>payload) {
  const actionCreator = actionCreate(type, callbackFunc);

  actionCreator.getType = () => type;
  actionCreator.toString = () => type;

  return actionCreator;
}

export function createActionAsync(description, api) {
  let actions = {
    request: createAction(`${description}_REQUEST`),
    success: createAction(`${description}_SUCCESS`),
    failure: createAction(`${description}_FAILURE`, error=> {
      return isObject(error) ? error : {message: error};
    })
  };

  let actionAsync = (...args) => {
    return (dispatch) => {
      dispatch(actions.request());
      return api(...args)
        .then(res => {
          dispatch(actions.success(res.data));
        })
        .catch(err => {
          let msg = 'Oops! Something went wrong!';
          if (err.data && err.data.message) {
            msg = err.data.message;
          }
          dispatch(actions.failure(msg));
        });
    };
  };

  actionAsync.request = actions.request;
  actionAsync.success = actions.success;
  actionAsync.failure = actions.failure;
  return actionAsync;
}
