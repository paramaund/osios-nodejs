import React, {Component, PropTypes} from 'react';
import {Row, Col} from 'react-bootstrap';
import debounce from 'lodash/debounce';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

class AccountProjects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      sections: ['Plan', 'Design', 'Dev', 'Deploy']
    };

    this.fieldDebounceWrapper = debounce((value, fieldName)=>this.setState({[fieldName]: value}), 200);
    this.userFieldDebounceWrapper = debounce((value, fieldName)=>this.setState({user: Object.assign({}, this.state.user, {[fieldName]: value})}), 200);
    this.submitDebounceWrapper = debounce(this.onSubmit.bind(this), 200);
  }

  componentWillMount() {
    const {currentUser} = this.props;
    if (currentUser) {
      this.state.user = Object.assign({}, currentUser);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.slideIndex === nextProps.slideIndex;
  }

  onSubmit() {
    // const {messagesActions} = this.props;
    // messagesActions.contact({
    //   name: this.state.user.name,
    //   topic: this.state.topics[this.state.topic],
    //   message: this.state.message,
    //   email: this.state.user.email
    // });
    // this.state.message = '';
    // this.state.topic = 0;
    // this.refs.message.input.refs.input.value = '';
    // this.refs.message.setState({hasValue: false});
    // this.forceUpdate();
  }

  render() {
    const {currentUser} = this.props;
    return (
      <div style={{marginTop: 20}}>
        <div style={{padding: 20, border: '1px solid #d1d2d3'}}>
          <form onSubmit={event => { event.preventDefault(); this.submitDebounceWrapper(); }}>
            <Row>
              <Col xs={12} sm={6}>
                <a role="button"><h3>Milestone Sections (max 11 chars)</h3></a>
              </Col>
              <Col xs={12} sm={6}>
                {this.state.sections.map((section, index)=>
                  <Row key={index}>
                    <Col xs={12}>
                      <TextField
                        fullWidth
                        required
                        disabled
                        hintText={'Section' + (index + 1)}
                        floatingLabelText={'Section' + (index + 1)}
                        onChange={event=>this.userFieldDebounceWrapper(event.target.value, 'section' + (index + 1))}
                        defaultValue={section}
                      />
                    </Col>
                  </Row>)}
              </Col>
            </Row>
            <Row>
              <Col xs={12} className="text-center">
                <RaisedButton
                  type="submit"
                  label="Save"
                  primary={true}
                  disabled
                  style={{display: 'none', marginTop: 20}}
                  labelStyle={{fontWeight: 900}}
                />
              </Col>
            </Row>
          </form>
        </div>
      </div>
    );
  }
}

AccountProjects.propTypes = {
  currentUser: PropTypes.object.isRequired,
  slideIndex: PropTypes.number.isRequired
};

export default AccountProjects;
