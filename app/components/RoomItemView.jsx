import React, { Component, PropTypes } from 'react';
import { Row, Col, Image, Tooltip, OverlayTrigger, Popover } from 'react-bootstrap';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import FlatButton from 'material-ui/FlatButton';
import defaultAvatar from '../assets/img/default-user.png';
import { LinkContainer } from 'react-router-bootstrap';
import uniqBy from 'lodash/uniqBy';

class RoomItemView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(nextProps) {
  }

  onEmotionMouseEnter() {
  }

  onDeleteCancel() {
    this.refs.deletePopover.hide();
  }

  onDeleteSubmit() {
    const {roomsActions, room} = this.props;
    roomsActions.deleteById(room._id);
    this.refs.deletePopover.hide();
  }

  render() {
    const {room, projectId} = this.props;
    let DnDElement = (
      <OverlayTrigger placement="top"
                      overlay={<Tooltip id="move">Drag & Drop</Tooltip>}>
        <FontIcon role="button"
                  style={{color: '#AAAAAA', fontSize: 18,width: 40, height: 40, padding: 10}}
                  className="draggable-handler glyphicon glyphicon-move"
                  onClick={()=> {console.log('click'); }}/>
      </OverlayTrigger>
    );

    let deletePopover = (
      <Popover id="deletePopover" title="Are you sure you want to delete this room?">
        <Row>
          <Col xs={12}>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={6}>
            <FlatButton label="Delete now" primary={true}
                        onClick={this.onDeleteSubmit.bind(this)}/>
          </Col>
          <Col xs={12} sm={6}>
            <FlatButton label="Cancel"
                        onClick={this.onDeleteCancel.bind(this)}
                        secondary={true}/>
          </Col>
        </Row>
      </Popover>
    );
    let deleteElement = (
      <OverlayTrigger trigger="manual" placement="left"
                      overlay={deletePopover} ref="deletePopover">

        <OverlayTrigger placement="top"
                        overlay={<Tooltip id="delete">Delete Room</Tooltip>}>
          <IconButton iconStyle={{color: '#AAAAAA', fontSize: 18}}
                      style={{width: 40, height: 40, padding: 10}}
                      iconClassName="glyphicon glyphicon-trash"
                      onClick={()=>this.refs.deletePopover.show()}/>
        </OverlayTrigger>
      </OverlayTrigger>
    );
    let roomNameElement = <span role="button" style={{fontSize: '2.5rem'}}>{room.name}</span>;
    return (
      <div
        style={{padding: 20, marginTop: 20, borderRadius: 35, border: '2px solid #26abe2'}}
        className="draggable project-item">
        <Row>
          <Col xs={12}>
            <Row>
              <Col xs={8}>
                <div style={{fontSize: 40, lineHeight: '40px'}}>
                  {projectId ?
                    (
                      <LinkContainer to={{ pathname: `/projects/${projectId}/rooms/${room._id}` }}>
                        {roomNameElement}
                      </LinkContainer>
                    ) : roomNameElement}
                </div>
              </Col>
              <Col xs={4} className="project-item-bar">
                <div className="text-right">
                  {DnDElement}
                  {deleteElement}
                </div>
              </Col>
            </Row>
            <hr style={{border: '1px solid #CCC', marginTop: 5, marginBottom: 5}}/>
            <Row>
              <Col xs={4}>
                Unread messages
              </Col>
              <Col xs={8} className="text-right">
                {uniqBy(room.users, 'user._id').map((user, index)=> {
                  let userName = `${user.user.first_name || ''}${user.user.first_name ? ' ' : ''}${user.user.last_name || ''}`;
                  if (userName.trim() === '') {
                    userName = user.user.email;
                  }
                  return (
                    <OverlayTrigger key={index} placement="top" rootClose
                                    overlay={<Tooltip id={user._id}>{userName}</Tooltip>}>
                      <Image src={user.user.avatar || defaultAvatar} responsive circle thumbnail
                             style={{width: 45, height: 45}}/>
                    </OverlayTrigger>
                  );
                })}
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

RoomItemView.propTypes = {
  room: PropTypes.object.isRequired,
  projectId: PropTypes.string.isRequired,
  roomsActions: PropTypes.object.isRequired
};

export default RoomItemView;
