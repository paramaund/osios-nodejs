import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Navbar, NavItem, Image} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import NavbarCollapse from './NavbarCollapse';
import logo from '../assets/img/logo.png';
import defaultAvatar from '../assets/img/default-user.png';
import * as auth from '../actions/auth';

class NavigationPublic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpanded: false,
      logoImg: logo,
      logoStyles: {height: '100%'}
    };
    this.setLogoImage(props.currentUser.company_logo);
  }

  componentWillReceiveProps(nextProps) {
    const {currentUser} = nextProps;
    this.setLogoImage(currentUser.company_logo);
  }

  onNavItemSelect() {
    this.setState({isExpanded: false});
  }

  onToggleClick() {
    this.setState({isExpanded: !this.state.isExpanded});
  }

  render() {
    const {authenticated, currentUser, authActions, location} = this.props;
    let navbarCollapse;
    let navbarToggle;
    let avatar = defaultAvatar;

    if (authenticated) {
      if (currentUser.avatar && currentUser.avatar !== '') {
        avatar = currentUser.avatar;
      }
      navbarCollapse = (
        <NavbarCollapse
          location={location}
          authActions={authActions}
          avatar={avatar}
          onNavItemSelect={this.onNavItemSelect.bind(this)}/>
      );

      navbarToggle = <Navbar.Toggle/>;
    }

    return (
      <Navbar style={{borderBottom: '2px solid #7f3f98'}} className="top-navbar"
              onToggle={this.onToggleClick.bind(this)} expanded={this.state.isExpanded}>
        <div style={{position: 'relative'}}>
          <Navbar.Header>
            <Navbar.Brand>
              <LinkContainer to={{ pathname: '/projects' }} className="responsive-logo">
                <NavItem eventKey={0} style={{listStyle: 'none'}}>
                  <Image src={this.state.logoImg} responsive style={this.state.logoStyles}/>
                </NavItem>
              </LinkContainer>
            </Navbar.Brand>
            {navbarToggle}
          </Navbar.Header>
          {navbarCollapse}
        </div>
      </Navbar>
    );
  }

  setLogoImage(logoImg) {
    const {authenticated} = this.props;

    if (logoImg && authenticated) {
      this.state.logoImg = logoImg;
      this.state.logoStyles = {maxHeight: 75};
    } else {
      this.state.logoImg = logo;
      this.state.logoStyles = {height: '100%'};
    }
  }
}

NavigationPublic.propTypes = {
  authActions: PropTypes.object.isRequired,
  authenticated: PropTypes.bool.isRequired,
  currentUser: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired
};

function select(state) {
  return {
    authenticated: state.auth.authenticated,
    currentUser: state.users.currentUser,
    location: state.location
  };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(auth, dispatch)
  };
}

export default connect(select, mapDispatchToProps)(NavigationPublic);
