import React, {Component, PropTypes} from 'react';
import Formsy from 'formsy-react';
import {FormsyText, FormsySelect} from 'formsy-material-ui/lib';
import {Row, Col} from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';

class AccountConnect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      topic: 0,
      topics: [
        'I\'ve found the problem with OSIOS',
        'I have a suggestion',
        'I\'d like to discuss a business partnership',
        'Other topic'
      ]
    };
  }

  componentWillMount() {
    const {currentUser} = this.props;
    if (currentUser) {
      this.state.user = Object.assign({}, currentUser, {name: `${currentUser.first_name || ''}${currentUser.first_name ? ' ' : ''}${currentUser.last_name || ''}`});
    }
  }

  componentWillReceiveProps(nextProps) {
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.slideIndex === nextProps.slideIndex;
  }

  onFieldChange(value, fieldName) {
    this.setState({[fieldName]: value});
  }

  onEnableButton() {
    this.setState({
      canSubmit: true
    });
  }

  onDisableButton() {
    this.setState({
      canSubmit: false
    });
  }

  onSubmit(data, reset, invalidate) {
    const {messagesActions} = this.props;
    messagesActions.contact(Object.assign(data, {
      topic: this.state.topics[data.topic]
    }));
    reset();
    this.refs.message.setState({value: ''});
    this.refs.topic.setState({value: 0});
  }

  render() {
    const {currentUser} = this.props;
    let userName = `${currentUser.first_name || ''}${currentUser.first_name ? ' ' : ''}${currentUser.last_name || ''}`;
    return (
      <div>
        <Row style={{marginTop: 20, display: 'flex'}}>
          <Col xs={12} sm={6}>
            <div className="text-center" style={{padding: 20, height: '100%', border: '1px solid #d1d2d3'}}>
              <h1>FAQ</h1>
              <div style={{marginBottom: 20}}>Visit our FAQ page for techniques, features and answers.</div>
              <RaisedButton label="FAQ Page"
                            primary={true}
                            linkButton
                            href="https://osios.net/faq"/>
            </div>
          </Col>
          <Col xs={12} sm={6}>
            <div className="text-center" style={{padding: 20, height: '100%', border: '1px solid #d1d2d3'}}>
              <h1>YouTube Channel</h1>
              <div style={{marginBottom: 20}}>
                Visit our OSIOS YouTube channel for user videos, techniques, features, reviews and more...
              </div>
              <RaisedButton label="OSIOS YouTube"
                            primary={true}
                            linkButton
                            href="https://www.youtube.com/playlist?list=PLRAN2sT1CzXq4LiUNptmzD4rrbbndDPQ1&ytsession=FmXR3jvHk3OhG_tm_HviDfNRV6uQN6w2FpF7XdNhOPQvGxr35DTWgPP4DQBTxZ19jtEh5baa8wb34e6dxiGq4Fri1Lw_8Nl_dsqQVTVrnyM76Z2lCVkZjfKexIEYwtx-XFUCvYjuUIvNMXXti189p0yTNrzHYLDSaGvlM5k2Dh8P1UG-ydGyGPN2mxXw0c0kp7-ziN-TDe0"/>
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={6} smOffset={3}>
            <div style={{padding: 20, marginTop: 20, border: '1px solid #d1d2d3'}}>
              <h1 className="text-center">Contact Us</h1>
              <Formsy.Form
                noValidate
                onValid={this.onEnableButton.bind(this)}
                onInvalid={this.onDisableButton.bind(this)}
                onValidSubmit={this.onSubmit.bind(this)}
                // onInvalidSubmit={this.onInvalidSubmit.bind(this)}
                style={{marginBottom: 20, marginTop: 20}}
              >
                <Row>
                  <Col xs={10} xsOffset={1}>
                    <FormsyText
                      name="name"
                      fullWidth
                      required
                      hintText="Name"
                      floatingLabelText="Your Name"
                      defaultValue={userName}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col xs={10} xsOffset={1}>
                    <FormsyText
                      name="email"
                      fullWidth
                      required
                      type="email"
                      hintText="Email"
                      floatingLabelText="Your Email"
                      defaultValue={currentUser.email}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col xs={10} xsOffset={1}>
                    <FormsySelect
                      name="topic"
                      ref="topic"
                      value={this.state.topic}
                      onChange={(event, index, value)=>this.setState({topic: value})}
                      fullWidth
                    >
                      {this.state.topics.map((topic, index)=>
                        <MenuItem key={index} value={index} primaryText={topic}/>
                      )}
                    </FormsySelect>
                  </Col>
                </Row>
                <Row>
                  <Col xs={10} xsOffset={1}>
                    <FormsyText
                      name="message"
                      ref="message"
                      fullWidth
                      required
                      multiLine={true}
                      hintText="Message"
                      floatingLabelText="Your Message"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col xs={10} xsOffset={1}>
                    <RaisedButton
                      type="submit"
                      label="Send"
                      disabled={!this.state.canSubmit}
                      primary={true}
                      fullWidth
                      style={{width: '100%', marginTop: 20}}
                      labelStyle={{fontWeight: 900}}
                    />
                  </Col>
                </Row>
              </Formsy.Form>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

AccountConnect.propTypes = {
  slideIndex: PropTypes.number.isRequired,
  currentUser: PropTypes.object.isRequired,
  messagesActions: PropTypes.object.isRequired,
};

export default AccountConnect;
