import React, { Component, PropTypes } from 'react';
import { Row, Col, Image, Tooltip, OverlayTrigger, Popover } from 'react-bootstrap';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import FlatButton from 'material-ui/FlatButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import defaultAvatar from '../assets/img/default-user.png';
import defaultIcon from '../assets/img/thumbnail.png';
import happyIcon from '../assets/img/emotions/happy1.png';
import unhappyIcon from '../assets/img/emotions/unhappy1.png';
import okIcon from '../assets/img/emotions/ok1.png';
import { LinkContainer } from 'react-router-bootstrap';
import last from 'lodash/last';
import head from 'lodash/head';
import capitalize from 'lodash/capitalize';

class ProjectItemView extends Component {
  constructor(props) {
    super(props);
    this.deleteReasons = ['completed', 'terminated at clients request', 'terminated at developers request'];
    this.state = {
      emotion: this.getEmotionIconDesign(props.project),
      status: props.project.status._id,
      deleteReason: head(this.deleteReasons)
    };
  }

  componentWillReceiveProps(nextProps) {
    const {project} = nextProps;
    this.state.emotion = this.getEmotionIconDesign(project);
    this.state.status = project.status._id;
  }

  onEmotionMouseEnter() {
    const {project} = this.props;
    let leadClient = project.users.find(user=>user.role.name === 'lead client');
    if (leadClient) {
      this.setState({emotion: Object.assign(this.state.emotion, {emotionImage: leadClient.user.avatar || defaultAvatar})});
    } else {
      this.setState({emotion: Object.assign(this.state.emotion, {emotionImage: defaultAvatar})});
    }
  }

  onStatusChange(event, index, value) {
    this.setState({status: value});
  }

  onDeleteReasonChange(event, index, value) {
    this.setState({deleteReason: value});
  }

  getEmotionIconDesign(project) {
    let leadClient = project.users.find(user=>user.role.name === 'lead client');
    let leadName = '';
    if (leadClient) {
      leadName = `${leadClient.user.first_name || ''}${leadClient.user.first_name ? ' ' : ''}${leadClient.user.last_name || ''}`;
    }

    let defaultEmotion = {
      tooltip: 'Lead client ' + leadName + ' is ok with this project!',
      emotionImage: okIcon
    };
    switch (project.emotion.name) {
      case 'ok':
      {
        return defaultEmotion;
      }
        break;
      case 'happy':
      {
        return Object.assign(defaultEmotion, {
          tooltip: 'Lead client ' + leadName + ' is happy with this project!',
          emotionImage: happyIcon
        });
      }
        break;
      case 'unhappy':
      {
        return Object.assign(defaultEmotion, {
          tooltip: 'Lead client ' + leadName + ' is unhappy with this project!',
          emotionImage: unhappyIcon
        });
      }
        break;
      default:
      {
        return defaultEmotion;
      }
    }
  }

  onStatusSubmit() {
    const {projectsActions, project} = this.props;
    this.refs.statusPopover.hide();
    projectsActions.updateById(project._id, {status: this.state.status});
  }

  onStatusCancel() {
    const {project} = this.props;
    this.refs.statusPopover.hide();
    this.onStatusChange(null, null, project.status._id);
  }

  onDeleteCancel() {
    this.refs.deletePopover.hide();
    this.setState({deleteReason: head(this.deleteReasons)});
  }

  onProjectStatusChange(selectedStatus) {
    const {projectsActions, project, statuses} = this.props;
    let status = statuses.find(status=>status.name === selectedStatus);
    if (status) {
      let data = {status: status};
      if (selectedStatus === 'deleted') {
        data.deleted_reason = this.state.deleteReason;
      }
      projectsActions.updateById(project._id, data);
    }
  }

  render() {
    const {project, dndBtn=true, statusBtn=true, deleteBtn=true, restoreBtn} = this.props;
    let projectIcon = defaultIcon;
    if (project.icon) {
      projectIcon = project.icon;
    }

    let DnDElement, statusElement, deleteElement, restoreElement;
    let defaultEmotion = this.getEmotionIconDesign(project);
    if (dndBtn) {
      DnDElement = (
        <OverlayTrigger placement="top"
                        overlay={<Tooltip id="move">Drag & Drop</Tooltip>}>
          <FontIcon role="button"
                    style={{color: '#AAAAAA', fontSize: 18,width: 40, height: 40, padding: 10}}
                    className="draggable-handler glyphicon glyphicon-move"
                    onClick={()=> {console.log('click'); }}/>
        </OverlayTrigger>
      );
    }
    if (statusBtn) {
      const {statuses} = this.props;
      let statusPopover = (
        <Popover id="statusPopover" title="Project status?">
          <Row>
            <Col xs={12}>
              <SelectField autoWidth fullWidth value={this.state.status} onChange={this.onStatusChange.bind(this)}>
                {statuses.filter(status=>status.name !== 'deleted').map((status, index)=>
                  <MenuItem key={index} value={status._id}
                            primaryText={capitalize(status.name)}/>)}
              </SelectField>
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={6}>
              <FlatButton label="Change status" primary={true}
                          onClick={this.onStatusSubmit.bind(this)}/>
            </Col>
            <Col xs={12} sm={6}>
              <FlatButton label="Cancel"
                          onClick={this.onStatusCancel.bind(this)}
                          secondary={true}/>
            </Col>
          </Row>
        </Popover>
      );
      statusElement = (
        <OverlayTrigger trigger="manual" placement="left"
                        overlay={statusPopover} ref="statusPopover">
          <OverlayTrigger placement="top"
                          overlay={<Tooltip id="statusTooltip">Project Status</Tooltip>}>
            <IconButton iconStyle={{color: '#AAAAAA', fontSize: 18}}
                        style={{width: 40, height: 40, padding: 10}}
                        iconClassName="glyphicon glyphicon-info-sign"
                        onClick={()=> {this.refs.statusPopover.show(); this.onDeleteCancel(); }}/>
          </OverlayTrigger>
        </OverlayTrigger>
      );
    }

    if (deleteBtn) {
      let deletePopover = (
        <Popover id="deletePopover" title="Reason for deleting?">
          <Row>
            <Col xs={12}>
              <SelectField style={{overflow: 'hidden'}} autoWidth fullWidth value={this.state.deleteReason}
                           onChange={this.onDeleteReasonChange.bind(this)}>
                {this.deleteReasons.map((reason, index)=>
                  <MenuItem key={index} value={reason}
                            primaryText={capitalize(reason)}/>)}
              </SelectField>
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={6}>
              <FlatButton label="Delete now" primary={true}
                          onClick={()=>{this.refs.deletePopover.hide(); this.onProjectStatusChange.bind(this)('deleted'); }}/>
            </Col>
            <Col xs={12} sm={6}>
              <FlatButton label="Cancel"
                          onClick={this.onDeleteCancel.bind(this)}
                          secondary={true}/>
            </Col>
          </Row>
        </Popover>
      );
      deleteElement = (
        <OverlayTrigger trigger="manual" placement="left"
                        overlay={deletePopover} ref="deletePopover">

          <OverlayTrigger placement="top"
                          overlay={<Tooltip id="delete">Delete Project</Tooltip>}>
            <IconButton iconStyle={{color: '#AAAAAA', fontSize: 18}}
                        style={{width: 40, height: 40, padding: 10}}
                        iconClassName="glyphicon glyphicon-trash"
                        onClick={()=> {this.refs.deletePopover.show(); this.onStatusCancel(); }}/>
          </OverlayTrigger>
        </OverlayTrigger>
      );
    }

    if (restoreBtn) {
      restoreElement = (
        <OverlayTrigger placement="top"
                        overlay={<Tooltip id="restore">Restore Project</Tooltip>}>
          <IconButton iconStyle={{color: '#AAAAAA', fontSize: 18}}
                      style={{width: 40, height: 40, padding: 10}}
                      className="flip-image"
                      iconClassName="glyphicon glyphicon-repeat"
                      onClick={()=>this.onProjectStatusChange.bind(this)('working')}/>
        </OverlayTrigger>
      );
    }
    return (
      <div
        style={Object.assign({padding: 20, marginTop: 20, borderRadius: 35, border: '2px solid #29abe2'}, (this.props.style || {}))}
        className="draggable project-item">
        <Row>
          <Col xs={2}>
            <Image src={projectIcon} responsive thumbnail
                   style={{width: '100%', maxWidth: 100, marginLeft: 'auto', marginRight: 'auto', display: 'block'}}/>
          </Col>
          <Col xs={10}>
            <Row>
              <Col xs={8}>
                <div style={{fontSize: 40, lineHeight: '40px'}}>
                  <LinkContainer to={{ pathname: '/projects/' + project._id }}>
                    <span role="button" style={{fontSize: '2.5rem', fontWeight: 300}}>{last(project.scopes).name}</span>
                  </LinkContainer>
                </div>
              </Col>
              <Col xs={4} className="project-item-bar">
                <div className="text-right">
                  {DnDElement}
                  {statusElement}
                  {deleteElement}
                  {restoreElement}
                </div>
              </Col>
            </Row>
            <hr style={{border: '1px solid #CCC', marginTop: 5, marginBottom: 5}}/>
            <Row>
              <Col xs={4}>
                Unread messages
              </Col>
              <Col xs={4} className="text-center">
                <OverlayTrigger placement="top"
                                overlay={<Tooltip id="emotion">{this.state.emotion.tooltip}</Tooltip>}>
                  <Image src={this.state.emotion.emotionImage} className="avatar" style={{width: 38, height: 38}}
                         circle
                         onMouseEnter={this.onEmotionMouseEnter.bind(this)}
                         onMouseLeave={()=>this.setState({emotion: Object.assign(this.state.emotion, {emotionImage: defaultEmotion.emotionImage})})}/>
                </OverlayTrigger>
              </Col>
              <Col xs={4} className="text-right">
                Milestones
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

ProjectItemView.propTypes = {
  dndBtn: PropTypes.bool,
  statusBtn: PropTypes.bool,
  deleteBtn: PropTypes.bool,
  restoreBtn: PropTypes.bool,
  project: PropTypes.object.isRequired,
  style: PropTypes.object,
  statuses: PropTypes.array.isRequired,
  projectsActions: PropTypes.object.isRequired
};

export default ProjectItemView;
