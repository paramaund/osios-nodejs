import React, {Component, PropTypes} from 'react';
import {Navbar, Nav, NavItem, Image, Tooltip, OverlayTrigger} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import IconButton from 'material-ui/IconButton';
import debounce from 'lodash/debounce';
import projectsIcon from '../assets/img/navbar_top/projects.png';

class NavbarCollapse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navItemStyle: {
        width: 45,
        height: 45
      },
      navItems: [
        {
          id: 'projects',
          tooltip: 'Projects',
          pathname: '/projects',
          isActive: false,
          icon: projectsIcon
        }
      ]
    };

    this.animateHandler = debounce((id, value)=> {
      this.state.navItems.find(item=>item.id === id).isActive = value;
      this.forceUpdate();
    }, 200);
    this.configureNavBarAnimates(props.location);
  }

  componentWillReceiveProps(nextProps) {
    this.configureNavBarAnimates(nextProps.location);
  }

  onLogout() {
    const {authActions} = this.props;
    authActions.logout();
  }

  render() {
    const {avatar, onNavItemSelect, location} = this.props;
    return (
      <Navbar.Collapse>
        <Nav pullRight className="top-navbar-nav">
          {this.state.navItems.map((item, index)=>
            <LinkContainer key={index} to={{ pathname: item.pathname }}>
              <NavItem eventKey={index + 1} onClick={onNavItemSelect}>
                <OverlayTrigger placement="top" rootClose
                                overlay={<Tooltip id={item.id}>{item.tooltip}</Tooltip>}>
                  <Image src={item.icon} className={'animated pulse ' + (item.isActive ? 'infinite' : '')}
                         style={this.state.navItemStyle}
                         onMouseEnter={()=>this.animateHandler('projects', true)}
                         onMouseLeave={()=>this.animateHandler('projects', location.pathname === item.pathname)}/>
                </OverlayTrigger>
              </NavItem>
            </LinkContainer>
          )}
          <LinkContainer to={{ pathname: '/account' }}>
            <NavItem eventKey={1} onClick={onNavItemSelect}>
              <OverlayTrigger placement="top" rootClose
                              overlay={<Tooltip id="account">View and adjust your account settings</Tooltip>}>
                <Image src={avatar} className="avatar" style={this.state.navItemStyle} thumbnail circle/>
              </OverlayTrigger>
            </NavItem>
          </LinkContainer>
          <NavItem eventKey={2}>
            <OverlayTrigger placement="top"
                            overlay={<Tooltip id="logout">Logout</Tooltip>}>
              <IconButton
                onClick={this.onLogout.bind(this)}
                iconClassName="fa fa-sign-out"
              />
            </OverlayTrigger>
          </NavItem>
        </Nav>
      </Navbar.Collapse>
    );
  }

  configureNavBarAnimates(location) {
    this.state.navItems.forEach(item=>item.isActive = false);
    let navItem = this.state.navItems.find(item=>item.pathname === location.pathname);
    if (navItem) {
      navItem.isActive = true;
    }
  }
}

NavbarCollapse.propTypes = {
  onNavItemSelect: PropTypes.func.isRequired,
  authActions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  avatar: PropTypes.string.isRequired
};

export default NavbarCollapse;
