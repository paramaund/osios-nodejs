import React, {PropTypes, Component} from 'react';
import {Image} from 'react-bootstrap';
import spinner from '../assets/img/loading.gif';

class Spinner extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="public-spinner">
        <Image src={spinner} responsive style={{marginLeft: 'auto', marginRight: 'auto'}}/>
      </div>
    );
  }
}

export default Spinner;