import React, {Component, PropTypes} from 'react';
import {Row, Col, Image, Collapse, Well} from 'react-bootstrap';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';
import Debounce from './Debounce';
import defaultAvatar from '../assets/img/default-user.png';
import remove from 'lodash/remove';

class CreateOrUpdateModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      users: [],
      selectedCompanies: []
    };
  }

  componentWillMount() {

  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isOpen && nextProps.isOpen) {
      const {companiesActions, project} = nextProps;
      this.state.name = '';
      this.state.errorText = '';
      this.state.users = [];
      this.state.selectedCompanies = [];
      if (project._id) {
        companiesActions.fetchByProject({project: project._id});
        this.state.projectId = project._id;
      }
    }
    if (!this.props.currentRoom._id && nextProps.currentRoom._id) {
      this.state.name = nextProps.currentRoom.name;
      this.state.users = nextProps.currentRoom.users;
      this.state.selectedCompanies = nextProps.companies
        .filter(company=>company.users
          .some(user=>nextProps.currentRoom.users
            .some(user2=>user2.user._id === user._id)))
        .map(company=>company._id);
    }
  }

  onFieldChange(fieldName, value) {
    if (fieldName === 'name') {
      let name = value.trim();
      if (name !== '' && name.length > 2) {
        this.state.errorText = '';
      }
    }
    this.setState({[fieldName]: value});
  }

  onSelectUser(user, permission) {
    let existingUser = this.state.users.find(elem=>elem.user._id === user._id);
    if (existingUser) {
      remove(this.state.users, existingUser);
    }
    if (!existingUser || existingUser.permission._id !== permission._id) {
      this.state.users.push(Object.assign({}, {user, permission}));
    }
    this.forceUpdate();
  }

  onCompanySelect(companyId) {
    if (this.state.selectedCompanies.some(elem=>elem === companyId)) {
      remove(this.state.selectedCompanies, elem=>elem === companyId);
    } else {
      this.state.selectedCompanies.push(companyId);
    }
    this.forceUpdate();
  }

  onSubmit() {
    let name = this.state.name.trim();
    if (name === '' || name.length < 3) {
      this.setState({errorText: 'Field \'Room name\' is required and should be at least 3 characters.'});
      return;
    }
    const {onSubmit} = this.props;
    onSubmit({name: this.state.name, users: this.state.users, projectId: this.state.projectId});
  }

  onKeyDown(event) {
    if (event.keyCode === 13) {
      this.onSubmit();
    }
  }

  render() {
    const {onClose, isOpen, currentRoom, companies, permissions} = this.props;
    const actions = [
      <FlatButton
        tabIndex={-1}
        label="Cancel"
        primary={true}
        onClick={onClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        onClick={this.onSubmit.bind(this)}
      />
    ];

    let title = currentRoom._id ? 'Edit room' : 'Create room';

    return (
      <div>
        <Dialog
          title={title}
          actions={actions}
          modal={true}
          open={isOpen}
        >
          <Debounce time={200} handler="onChange">
            <TextField
              style={{marginBottom: 20}}
              fullWidth
              hintText="Enter Room Name"
              floatingLabelText="Room name"
              floatingLabelFixed={true}
              errorText={this.state.errorText}
              onKeyDown={this.onKeyDown.bind(this)}
              onChange={this.onFieldChange.bind(this, 'name')}
              value={this.state.name}
            />
          </Debounce>
          {companies.map((company, index)=>
            <div>
              <div role="button" className="panel panel-default"
                   style={{marginBottom: 0}}
                   onClick={this.onCompanySelect.bind(this, company._id)}>
                <div className="panel-heading" style={{fontSize: 20, fontWeight: 300}}>{company.name}</div>
              </div>
              <Collapse
                timeout={0}
                key={index}
                in={this.state.selectedCompanies.some(elem=>elem === company._id)}>
                {company.users.length > 0 ? (
                  <div>
                    <Well>
                      <div>
                        <Row>
                          <Col xs={8}>
                            <div>Users</div>
                          </Col>
                          <Col xs={4}>
                            <Row>
                              {permissions.map((permission, index)=>
                                <Col xs={6} key={index}>
                                  {permission.name}
                                </Col>
                              )}
                            </Row>
                          </Col>
                        </Row>
                        {company.users.map((user, index)=> {
                            let userName = `${user.first_name || ''}${user.first_name ? ' ' : ''}${user.last_name || ''}`;
                            if (userName.trim() === '') {
                              userName = user.email;
                            }
                            return (
                              <Row key={index}>
                                <Col xs={8}>
                                  <div>
                                    <Image src={user.avatar || defaultAvatar} responsive circle thumbnail
                                           style={{width: 45, height: 45}}/>
                                    {userName}
                                  </div>
                                </Col>
                                <Col xs={4}>
                                  <Row>
                                    {permissions.map((permission, index)=>
                                      <Col xs={6} key={index}>
                                        <Checkbox
                                          checked={this.isSelectedPermissionChecked(user, permission)}
                                          onCheck={this.onSelectUser.bind(this, user, permission)}
                                        />
                                      </Col>
                                    )}
                                  </Row>
                                </Col>
                              </Row>
                            );
                          }
                        )}
                      </div>
                    </Well>
                  </div>
                ) : (
                  <Well className="text-notification">
                    Sorry, there are no people in this company with access to this project.
                  </Well>
                )}
              </Collapse>
            </div>
          )}
        </Dialog>
      </div>
    );
  }

  isSelectedPermissionChecked(user, permission) {
    return this.state.users.some(elem=>elem.user._id === user._id && permission._id === elem.permission._id);
  }
}

CreateOrUpdateModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  companiesActions: PropTypes.object.isRequired,
  project: PropTypes.object.isRequired,
  companies: PropTypes.array.isRequired,
  permissions: PropTypes.array.isRequired,
  users: PropTypes.array,
  currentRoom: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired
};

export default CreateOrUpdateModal;
