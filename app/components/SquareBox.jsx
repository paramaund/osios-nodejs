import React, {Component, PropTypes} from 'react';
import NavigationCheck from 'material-ui/svg-icons/navigation/check';
import person from 'material-ui/svg-icons/social/person';
import android from 'material-ui/svg-icons/action/android';
import schedule from 'material-ui/svg-icons/action/schedule';
import tablet from 'material-ui/svg-icons/hardware/tablet';
import target from 'material-ui/svg-icons/device/gps-fixed';
import iphone from 'material-ui/svg-icons/hardware/phone-iphone';
import ipad from 'material-ui/svg-icons/hardware/tablet-mac';
import applewatch from 'material-ui/svg-icons/hardware/watch';
import FontIcon from 'material-ui/FontIcon';

class SquareBox extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {checked, underText, icon, onClick} = this.props;
    let SomeIcon;
    let icons = {person, android, schedule, tablet, target, iphone, ipad, applewatch};
    SomeIcon = icons[icon];
    if (!SomeIcon) {
      SomeIcon =
        <FontIcon className={'check main-ico center-block ' + icon}/>;
    } else {
      SomeIcon = <SomeIcon className="check main-ico center-block"/>;
    }
    return (
      <div onClick={onClick} className={'text-center square-option box ' + (checked ? 'checked' : '')} tabIndex={this.props.tabIndex}>
        <svg width="110" height="110">
          <line className="top" x1="0" y1="0" x2="900" y2="0" strokeWidth="30"/>
          <line className="left" x1="0" y1="110" x2="0" y2="-920" strokeWidth="30"/>
          <line className="bottom" x1="110" y1="110" x2="-600" y2="110" strokeWidth="30"/>
          <line className="right" x1="110" y1="0" x2="110" y2="1380" strokeWidth="30"/>
        </svg>
        {SomeIcon}
        <span className="undertext">{underText}</span>
        <span className="checked-icon"><NavigationCheck className="check"/></span>
      </div>
    );
  }
}

SquareBox.propTypes = {
  icon: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  tabIndex: PropTypes.number,
  underText: PropTypes.string.isRequired,
  onClick: PropTypes.func
};

export default SquareBox;
