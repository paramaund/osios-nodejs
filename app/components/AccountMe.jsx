import React, {Component, PropTypes} from 'react';
import Formsy from 'formsy-react';
import {FormsyText} from 'formsy-material-ui/lib';
import {Image, Row, Col} from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dropzone from 'react-dropzone';
import {makeRequest} from '../api/server';
import {ISBROWSER} from '../constants/index';

const TimezonePicker = ISBROWSER ? require('react-timezone').default : undefined;

class AccountMe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarFile: {},
      companyFile: {},
      dropZoneStyles: {
        maxWidth: 300,
        maxHeight: 300,
        minWidth: 100,
        minHeight: 100,
        marginLeft: 'auto',
        marginRight: 'auto'
      }
    };
  }

  componentWillMount() {
    const {currentUser} = this.props;
    if (currentUser) {
      if (currentUser.avatar) {
        this.state.avatarFile = {preview: currentUser.avatar};
      }
      if (currentUser.company_logo) {
        this.state.companyFile = {preview: currentUser.company_logo};
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.slideIndex === nextProps.slideIndex;
  }

  onDrop(files, fieldName, props) {
    files.forEach(file=> {
      // TO DO: dispatch
      let data = new FormData();
      data.append('file', file);
      if (props) {
        data.append('props', props);
      }
      makeRequest('post', '/api/uploads/crop', data).then(response=> {
        console.dir(response.data);
        this.setState({[fieldName]: {preview: response.data.url}});
      }).catch(error=> {
        console.dir(error);
      });
    });
  }

  onCompanyImageDrop(files) {
    this.onDrop(files, 'companyFile', {
      width: 300,
      height: 300
    });
  }

  onAvatarImageDrop(files) {
    this.onDrop(files, 'avatarFile');
  }

  onInvalidSubmit(values, reset, invalidate) {
    let errors = {};
    if (!values.email) {
      errors.email = 'Email field is required.';
    }
    invalidate(errors);
  }

  onSubmit(data, reset, invalidate) {
    const {usersActions} = this.props;
    usersActions.updateMe(Object.assign({}, data, {
      avatar: this.state.avatarFile.preview,
      company_logo: this.state.companyFile.preview,
      timezone: this.state.timezone
    }));
    reset(Object.assign({}, data, {
      password: '',
      password_confirm: ''
    }));
    this.refs.password.setState({value: ''});
    this.refs.password_confirm.setState({value: ''});
  }

  render() {
    const {currentUser} = this.props;
    let avatarPreview = this._renderDropZone('avatarFile');
    let companyPreview = this._renderDropZone('companyFile');
    let timezonePickerElement;
    if (TimezonePicker) {
      timezonePickerElement = (
        <Row>
          <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
            <TimezonePicker
              defaultValue={currentUser.timezone}
              onChange={timezone=>this.setState({timezone})}
              placeholder="Select You Timezone"
              style={{width: '100%'}}/>
          </Col>
        </Row>
      );
    }

    return (
      <Formsy.Form
        noValidate
        // onValid={this.onEnableButton.bind(this)}
        // onInvalid={this.onDisableButton.bind(this)}
        onValidSubmit={this.onSubmit.bind(this)}
        onInvalidSubmit={this.onInvalidSubmit.bind(this)}
        style={{marginBottom: 20, marginTop: 20}}>
        <Row>
          <Col xs={4}>
            <Dropzone onDrop={this.onAvatarImageDrop.bind(this)}
                      multiple={false} accept=".jpeg, .jpg, .png, .ico"
                      style={this.state.dropZoneStyles}>
              {avatarPreview}
            </Dropzone>
            <div>*click on image or drag & drop file to upload</div>
            <FlatButton label="REMOVE" primary={true} onClick={()=>this.setState({avatarFile: {preview: null}})}/>
          </Col>
          <Col xs={8}>
            <div style={{padding: 20, border: '1px solid #d1d2d3'}}>
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  <FormsyText
                    name="first_name"
                    validations="isWords"
                    validationError="First name may contain only letters"
                    fullWidth
                    floatingLabelFixed
                    hintText="First Name"
                    floatingLabelText="Your First Name"
                    defaultValue={currentUser.first_name}
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  <FormsyText
                    name="last_name"
                    validations="isWords"
                    validationError="Last name may contain only letters"
                    fullWidth
                    floatingLabelFixed
                    hintText="Last Name"
                    floatingLabelText="Your Last Name"
                    defaultValue={currentUser.last_name}
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  <FormsyText
                    name="company_name"
                    fullWidth
                    floatingLabelFixed
                    hintText="Company Name"
                    floatingLabelText="Your Company Name"
                    defaultValue={currentUser.company_name}
                  />
                </Col>
              </Row>
              <Row style={{marginTop: 20}}>
                <Col xs={12} sm={4} smOffset={1} md={2} mdOffset={2}>
                  Choose Your Company Logo:
                </Col>
                <Col xs={8} xsOffset={2} smOffset={0} sm={6} md={6}>
                  <Dropzone onDrop={this.onCompanyImageDrop.bind(this)}
                            multiple={false} accept=".jpeg, .jpg, .png, .ico"
                            style={this.state.dropZoneStyles}>
                    {companyPreview}
                  </Dropzone>
                  <div>*click on image or drag & drop file to upload</div>
                  <FlatButton label="REMOVE" primary={true}
                              onClick={()=>this.setState({companyFile: {preview: null}})}/>
                </Col>
              </Row>
              <Row style={{marginTop: 20}}>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  Choose Your Timezone
                </Col>
              </Row>
              {timezonePickerElement}
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  <FormsyText
                    name="phone"
                    fullWidth
                    floatingLabelFixed
                    hintText="Telephone"
                    floatingLabelText="Your Telephone Number"
                    defaultValue={currentUser.phone}
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  <FormsyText
                    name="email"
                    validations="isEmail"
                    validationError="This is not a valid email."
                    fullWidth
                    required
                    floatingLabelFixed
                    type="email"
                    hintText="Email"
                    floatingLabelText="Your Email"
                    defaultValue={currentUser.email}
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  <FormsyText
                    name="skype"
                    fullWidth
                    floatingLabelFixed
                    hintText="Skype"
                    floatingLabelText="Your Skype"
                    defaultValue={currentUser.skype}
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  <FormsyText
                    name="password"
                    ref="password"
                    fullWidth
                    floatingLabelFixed
                    type="password"
                    minLength={6}
                    hintText="Password"
                    floatingLabelText="New Password *"
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  <FormsyText
                    name="password_confirm"
                    ref="password_confirm"
                    fullWidth
                    validations="equalsField:password"
                    validationError="Passwords don't match."
                    floatingLabelFixed
                    type="password"
                    hintText="Password"
                    floatingLabelText="Repeat Password"
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  * - Leave blank for no changes
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={10} smOffset={1} md={8} mdOffset={2}>
                  <RaisedButton
                    type="submit"
                    label="Save"
                    primary={true}
                    fullWidth
                    style={{width: '100%', marginTop: 20}}
                    labelStyle={{fontWeight: 900}}
                  />
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Formsy.Form >
    );
  }

  createDefaultDropZone() {
    return (
      <div style={{border: '1px solid #d1d2d3'}}>
        <div style={this.state.dropZoneStyles} className="text-center">
          Try dropping some files here, or click to select files to upload.
        </div>
      </div>
    );
  }

  _renderDropZone(fieldName) {
    let preview = this.createDefaultDropZone();
    if (this.state[fieldName].preview) {
      preview = (
        <div style={{minHeight: 100, border: '1px solid #d1d2d3'}}>
          <Image src={this.state[fieldName].preview}
                 style={{display: 'block', marginLeft: 'auto', marginRight: 'auto'}}
                 responsive thumbnail/>
        </div>);
    }
    return preview;
  }
}

AccountMe.propTypes = {
  currentUser: PropTypes.object.isRequired,
  usersActions: PropTypes.object.isRequired,
  slideIndex: PropTypes.number.isRequired
};

export default AccountMe;
