import React, {PropTypes, Component} from 'react';
import {Row, Col, Tooltip, OverlayTrigger, Collapse} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import uniqBy from 'lodash/uniqBy';
import countBy from 'lodash/countBy';
import isNumber from 'lodash/isNumber';
import flattenDeep from 'lodash/flattenDeep';
import map from 'lodash/map';
import ProjectItemView from '../components/ProjectItemView';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import RaisedButton from 'material-ui/RaisedButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {ISBROWSER} from '../constants/index';

const Sortable = ISBROWSER ? require('sortablejs') : undefined;

class ProjectsTabPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ownWorkingOpen: true,
      otherWorkingOpen: true,
      ownNotStartedOpen: true,
      ownProblemOpen: true
    };
  }

  componentWillMount() {
    const {projectsActions, status} = this.props;
    let projects = this.getAllProjectsFromObject(this.props.projects);
    if (projects.length === 0) {
      projectsActions.fetchByParams({status, limit: 10, isOwner: true});
      projectsActions.fetchByParams({status: status !== 'active' ? status : 'working', limit: 10, isOwner: false});
      projectsActions.fetchProjectsStatistics({status});
    }
  }

  componentWillReceiveProps(nextProps) {
    const {projectsActions, status} = nextProps;
    let oldProjs = this.props.projects;
    let newProjs = nextProps.projects;
    oldProjs = this.getAllProjectsFromObject(oldProjs);
    newProjs = this.getAllProjectsFromObject(newProjs);

    let oldAndNewProjects = [...newProjs, ...oldProjs];
    if (uniqBy(oldAndNewProjects, '_id').length !== oldAndNewProjects.length / 2) {
      projectsActions.fetchProjectsStatistics({status});
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.slideIndex === nextProps.slideIndex;
  }

  onLoadMoreProjects(status, isOwner) {
    const {projectsActions} = this.props;
    let projects = this.props.projects;

    let offset = countBy(projects[status][isOwner ? 'own' : 'other'].projects, project=>project.status.name === status);
    projectsActions.fetchByParams({status, limit: 10, offset: offset.true, isOwner});
  }

  getAllProjectsFromObject(obj) {
    return flattenDeep(map(obj, (value, key)=> {
      if (['working', 'problem', 'not started'].some(elem=>elem === key)) {
        return map(value, val=>val.projects);
      }
      return value.projects;
    }));
  }

  getViewOptions() {
    const {projects, status, currentUser, total} = this.props;

    switch (status) {
      case 'active':
      {
        let ownProjects = projects.working.own;
        let otherProjects = projects.working.other;
        let ownProblemProjects = projects.problem.own;
        let ownNotStartedProjects = projects['not started'].own;
        let paymentPlan = currentUser.payment_plan || {};
        let addButtonElement = (
          <div className="text-center" style={{margin: 20}}>
            {total >= paymentPlan.projects ? (
              <h3 className="text-center">
                <i>You've used all limits of your account. Please upgrade your account to be able to add more
                  projects.</i>
              </h3>
            ) : ''}
            <OverlayTrigger placement="top"
                            overlay={<Tooltip id="addNewProject">Add New Project</Tooltip>}>
              <LinkContainer disabled={total >= paymentPlan.projects}
                             to={{ pathname: '/projects/create'}}>
                <FloatingActionButton disabled={total >= paymentPlan.projects}>
                  <ContentAdd />
                </FloatingActionButton>
              </LinkContainer>
            </OverlayTrigger>
          </div>
        );

        return {
          status: 'active',
          ownProjects: ownProjects,
          otherProjects: otherProjects,
          ownProblemProjects: ownProblemProjects,
          ownNotStartedProjects: ownNotStartedProjects,
          ownProjectsListTitle: 'Projects I created:',
          emptyMainListTitle: 'You have not added any projects yet!',
          advanceBtnOptions: {},
          otherProjectsListTitle: 'Other projects:',
          ownProblemProjectsListTitle: 'My problem projects:',
          ownNotStartedProjectsListTitle: 'My unstarted projects:',
          addButtonElement: addButtonElement
        };
      }
        break;
      case 'completed':
      {
        return {
          status: 'completed',
          ownProjects: projects.own,
          otherProjects: projects.other,
          ownProjectsListTitle: 'My completed projects:',
          emptyMainListTitle: 'You have not completed any projects yet!',
          advanceBtnOptions: {},
          secondListTitle: 'Other completed projects:'
        };
      }
        break;
      case 'deleted':
      {
        return {
          status: 'deleted',
          ownProjects: projects.own,
          otherProjects: projects.other,
          ownProjectsListTitle: 'My recently deleted projects:',
          emptyMainListTitle: 'You have not recently deleted any projects!',
          advanceBtnOptions: {dndBtn: false, statusBtn: false, deleteBtn: false, restoreBtn: true},
          otherProjectsListTitle: 'Other recently deleted projects:'
        };
      }
        break;
      default:
      {
        return {};
      }
    }
  }

  render() {
    const {newest, oldest, statuses, projectsActions} = this.props;
    let newestElement = <div>Add project</div>;
    let oldestElement = <div>Add project</div>;
    if (newest && newest._id) {
      newestElement = (
        <LinkContainer to={{ pathname: '/projects/' + newest._id }}>
          <div role="button" className="psvalue">{newest.name}</div>
        </LinkContainer>
      );
    }
    if (oldest && oldest._id) {
      oldestElement = (
        <LinkContainer to={{ pathname: '/projects/' + oldest._id }}>
          <div role="button" className="psvalue">{oldest.name}</div>
        </LinkContainer>
      );
    }
    let viewOptions = this.getViewOptions();

    let workingProjectsListElement;
    if (viewOptions.ownProjects && viewOptions.ownProjects.projects.length > 0) {
      workingProjectsListElement = (
        <div>
          <h3 style={{fontWeight: 300, fontSize: '1.5rem'}}
              role="button"
              onClick={()=>this.setState({ownWorkingOpen: !this.state.ownWorkingOpen})}
          >
            {viewOptions.ownProjectsListTitle}</h3>
          <Collapse timeout={0} in={this.state.ownWorkingOpen}>
            <div>
              <div ref={this.sortableContainersDecorator.bind(this, viewOptions.ownProjects.projects)}>
                {viewOptions.ownProjects.projects.map((project, index)=>
                  <ProjectItemView key={index} statuses={statuses} projectsActions={projectsActions}
                                   project={project} {...viewOptions.advanceBtnOptions}/>)}
              </div>
              {viewOptions.ownProjects.total > viewOptions.ownProjects.projects.length ? (
                <div className="text-center" style={{marginTop: 10}}>
                  <RaisedButton label="Load more projects" primary={true}
                                onClick={this.onLoadMoreProjects.bind(this, viewOptions.status === 'active' ? 'working' : viewOptions.status, true)}/>
                </div>
              ) : ''}
            </div>
          </Collapse>
        </div>
      );
    } else {
      workingProjectsListElement = <div className="text-notification"><i>{viewOptions.emptyMainListTitle}</i></div>;
    }
    let otherProjectsListElement;
    if (viewOptions.otherProjects && viewOptions.otherProjects.projects.length > 0) {
      otherProjectsListElement = (
        <div>
          <h3 style={{fontWeight: 300}}
              role="button"
              onClick={()=>this.setState({otherWorkingOpen: !this.state.otherWorkingOpen})}
          >{viewOptions.otherProjectsListTitle}</h3>
          <Collapse timeout={0} in={this.state.otherWorkingOpen}>
            <div>
              <div ref={this.sortableContainersDecorator.bind(this, viewOptions.otherProjects.projects)}>
                {viewOptions.otherProjects.projects.map((project, index)=>
                  <ProjectItemView key={index} project={project}
                                   statuses={statuses} style={{border: '2px solid #7f3f98'}}
                                   projectsActions={projectsActions}
                                   deleteBtn={false} statusBtn={false}/>)}
              </div>
              {viewOptions.otherProjects.total > viewOptions.otherProjects.projects.length ? (
                <div className="text-center" style={{marginTop: 10}}>
                  <RaisedButton label="Load more projects" primary={true}
                                onClick={this.onLoadMoreProjects.bind(this, viewOptions.status === 'active' ? 'working' : viewOptions.status, false)}/>
                </div>
              ) : ''}
            </div>
          </Collapse>
        </div>
      );
    }

    let ownProblemProjectsListElement;
    if (viewOptions.ownProblemProjects && viewOptions.ownProblemProjects.projects.length > 0) {
      ownProblemProjectsListElement = (
        <div>
          <h3 style={{fontWeight: 300}}
              role="button"
              onClick={()=>this.setState({ownProblemOpen: !this.state.ownProblemOpen})}
          >{viewOptions.ownProblemProjectsListTitle}</h3>
          <Collapse timeout={0} in={this.state.ownProblemOpen}>
            <div>
              <div ref={this.sortableContainersDecorator.bind(this, viewOptions.ownProblemProjects.projects)}>
                {viewOptions.ownProblemProjects.projects.map((project, index)=>
                  <ProjectItemView key={index} project={project}
                                   statuses={statuses}
                                   projectsActions={projectsActions}/>)}
              </div>
              {viewOptions.ownProblemProjects.total > viewOptions.ownProblemProjects.projects.length ? (
                <div className="text-center" style={{marginTop: 10}}>
                  <RaisedButton label="Load more projects" primary={true}
                                onClick={this.onLoadMoreProjects.bind(this, viewOptions.status === 'active' ? 'problem' : viewOptions.status, true)}/>
                </div>
              ) : ''}
            </div>
          </Collapse>
        </div>
      );
    }
    let ownNotStartedProjectsElement;
    if (viewOptions.ownNotStartedProjects && viewOptions.ownNotStartedProjects.projects.length > 0) {
      ownNotStartedProjectsElement = (
        <div>
          <h3 style={{fontWeight: 300}}
              role="button"
              onClick={()=>this.setState({ownNotStartedOpen: !this.state.ownNotStartedOpen})}
          >{viewOptions.ownNotStartedProjectsListTitle}</h3>
          <Collapse timeout={0} in={this.state.ownNotStartedOpen}>
            <div>
              <div ref={this.sortableContainersDecorator.bind(this, viewOptions.ownNotStartedProjects.projects)}>
                {viewOptions.ownNotStartedProjects.projects.map((project, index)=>
                  <ProjectItemView key={index} project={project}
                                   statuses={statuses}
                                   projectsActions={projectsActions}/>)}
              </div>
              {viewOptions.ownNotStartedProjects.total > viewOptions.ownNotStartedProjects.projects.length ? (
                <div className="text-center" style={{marginTop: 10}}>
                  <RaisedButton label="Load more projects" primary={true}
                                onClick={this.onLoadMoreProjects.bind(this, viewOptions.status === 'active' ? 'not started' : viewOptions.status, true)}/>
                </div>
              ) : ''}
            </div>
          </Collapse>
        </div>
      );
    }

    return (
      <Row style={{marginBottom: 20, marginTop: 20}}>
        <Col xs={3}>
          <h3 style={{fontWeight: 300, fontSize: '1.5rem'}}>Project Shortcuts:</h3>
          <div style={{padding: 20, border: '1px solid #d1d2d3'}} className="project-shortcuts">
            <div className="form-group">
              <div className="pshead"><b>Newest:</b></div>
              {newestElement}
            </div>
            <div className="form-group">
              <div className="pshead"><b>Oldest:</b></div>
              {oldestElement}
            </div>
            <div className="form-group">
              <div className="pshead"><b>Most Recent Message:</b></div>
              <div className="psvalue">project</div>
            </div>
          </div>
        </Col>
        <Col xs={9}>
          {workingProjectsListElement}
          {viewOptions.addButtonElement}
          {otherProjectsListElement}
          {ownProblemProjectsListElement}
          {ownNotStartedProjectsElement}
        </Col>
      </Row>
    );
  }

  sortableContainersDecorator(projects, componentBackingInstance) {
    let projectsIdsOrder = projects.map(project=>project._id);
    const {projectsActions} = this.props;
    // check if backing instance not null
    if (componentBackingInstance) {
      let options = {
        sort: true,
        handle: '.draggable-handler', // Restricts sort start click/touch to the specified element
        draggable: '.draggable',
        onEnd: event=> {
          console.log(event.oldIndex + ' ' + event.newIndex);
          let oldIndex = event.oldIndex;
          let newIndex = event.newIndex;
          if (!isNumber(oldIndex) || !isNumber(newIndex) || oldIndex === newIndex) {
            return;
          }
          if (newIndex > oldIndex) {
            let temp = projectsIdsOrder[oldIndex];
            while (oldIndex < newIndex) {
              projectsIdsOrder[oldIndex] = projectsIdsOrder[oldIndex + 1];
              oldIndex++;
            }
            projectsIdsOrder[newIndex] = temp;
          } else {
            let temp = projectsIdsOrder[oldIndex];
            while (oldIndex > newIndex) {
              projectsIdsOrder[oldIndex] = projectsIdsOrder[oldIndex - 1];
              oldIndex--;
            }
            projectsIdsOrder[newIndex] = temp;
          }
          projectsActions.changeOrder({
            projects: projectsIdsOrder.map((projectId, index)=> {
              return {
                order: index, project: projectId
              };
            })
          });
        }
      };
      if (Sortable) {
        Sortable.create(componentBackingInstance, options);
      }
    }
  }
}

ProjectsTabPage.propTypes = {
  newest: PropTypes.object.isRequired,
  oldest: PropTypes.object.isRequired,
  slideIndex: PropTypes.number.isRequired,
  status: PropTypes.string.isRequired,
  projects: PropTypes.array.isRequired,
  statuses: PropTypes.array.isRequired,
  total: PropTypes.number.isRequired,
  projectsActions: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired
};


export default ProjectsTabPage;
