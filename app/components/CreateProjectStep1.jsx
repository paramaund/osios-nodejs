import React, {PropTypes, Component} from 'react';
import {Row, Col, Image, Collapse, Modal, OverlayTrigger, Popover} from 'react-bootstrap';
import SquareBox from '../components/SquareBox';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import ContentAdd from 'material-ui/svg-icons/content/add';
import capitalize from 'lodash/capitalize';
import remove from 'lodash/remove';
import head from 'lodash/head';
import last from 'lodash/last';
import difference from 'lodash/difference';
import snakeCase from 'lodash/snakeCase';
import camelCase from 'lodash/camelCase';
import defaultAvatar from '../assets/img/default-user.png';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import NavigationCheck from 'material-ui/svg-icons/navigation/check';
import Debounce from './Debounce';

class CreateProjectStep1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newUser: {},
      options: [
        {
          name: 'Complete now',
          icon: 'target',
          key: 'now'
        },
        {
          name: 'Send to client',
          icon: 'person',
          key: 'send'
        },
        {
          name: 'Complete later',
          icon: 'schedule',
          key: 'later'
        }
      ]
    };
  }

  componentWillReceiveProps(nextProps) {
    const {userSuccess, usersActions} = nextProps;
    if (userSuccess) {
      this.state.addNewUserModalOpen = false;
      this.state.newUser = {};
      usersActions.confirmSuccess();
    }
    let diff = head(difference(nextProps.allCompanies, this.props.allCompanies));
    if (diff) {
      this.state.newUser.company = diff;
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextState.addNewUserModalOpen && !nextState.newUser.company) {
      nextState.newUser.company = head(nextProps.allCompanies);
    }
  }

  onDeviceSelect(device) {
    const {currentProject, projectsActions} = this.props;
    let scope = last(currentProject.scopes);
    if (scope.devices.some(elem=>elem._id === device._id)) {
      let devicesCopy = [...scope.devices];
      remove(devicesCopy, elem=>elem._id === device._id);
      projectsActions.currentProjectScopeFieldChange({devices: [...devicesCopy]});
    } else {
      projectsActions.currentProjectScopeFieldChange({devices: [...scope.devices, device]});
    }
  }

  onPlatformSelect(platform) {
    const {currentProject, projectsActions} = this.props;
    let scope = last(currentProject.scopes);
    if (scope.platforms.some(elem=>elem._id === platform._id)) {
      let platformsCopy = [...scope.platforms];
      let devicesCopy = [...scope.devices];
      remove(platformsCopy, elem=>elem._id === platform._id);
      console.dir(platformsCopy)
      remove(devicesCopy, elem=>elem.platform._id === platform._id);
      console.dir(devicesCopy)
      projectsActions.currentProjectScopeFieldChange({
        devices: devicesCopy,
        platforms: platformsCopy
      });
    } else {
      projectsActions.currentProjectScopeFieldChange({platforms: [...scope.platforms, platform]});
    }
  }

  onOptionsChange(option) {
    const {options, onFieldChange} = this.props;
    if (options.some(elem=>elem.key === option.key)) {
      let optionsCopy = [...options];
      remove(optionsCopy, elem=>elem.key === option.key);
      onFieldChange('options', [...optionsCopy]);
    } else {
      onFieldChange('options', [option]);
    }
  }

  onAddNewCompany(event) {
    event.preventDefault();
    event.stopPropagation();
    let name = this.state.newCompanyName.trim();
    if (!name && name.length < 3) {
      return;
    }

    const {companiesActions} = this.props;
    companiesActions.create({name: name});
    this.state.newCompanyName = '';
    this.state.addNewCompanyOpen = false;
  }

  onCancelNewUserModal() {
    this.setState({newUser: {}, addNewUserModalOpen: false});
  }

  onSubmitNewUserModal(event) {
    event.preventDefault();
    if (!this.state.newUser.company) {
      this.setState({newUser: Object.assign({}, this.state.newUser, {companyError: 'Company is required!'})});
      return;
    }
    const {usersActions} = this.props;
    usersActions.create(Object.assign({}, this.state.newUser, {isSuperUser: true}));
  }

  onNewUserFieldChange(fieldName, value) {
    if (fieldName === 'company') {
      if (value === 0) {
        return;
      }
      this.state.newUser.companyError = '';
    }
    this.setState({newUser: Object.assign({}, this.state.newUser, {[fieldName]: value})});
  }

  onUserSelectChange(user, role) {
    const {projectsActions, currentProject:{users}} = this.props;
    let usersCopy = [...users];
    if (usersCopy.some(elem=>elem.user._id === user._id)) {
      remove(usersCopy, elem=>elem.user._id === user._id);
    } else {
      remove(usersCopy, elem=>elem.role.name === role);
      usersCopy.push({user, role: {name: role}});
    }
    this.refs[camelCase(role) + 'UserList'].hide();
    projectsActions.currentProjectFieldChange({users: usersCopy});
  }

  getDeviceIcon(device) {
    switch (device.key) {
      case 'phone':
      {
        return 'iphone';
      }
        break;
      case 'tablet':
      {
        return 'tablet';
      }
        break;
      case 'iphone':
      {
        return 'iphone';
      }
        break;
      case 'iphone6':
      {
        return 'iphone';
      }
        break;
      case 'ipad1':
      {
        return 'ipad';
      }
        break;
      case 'ipad2':
      {
        return 'tablet';
      }
        break;
      case 'applewatch':
      {
        return 'applewatch';
      }
        break;
      default:
      {
        return 'iphone';
      }
    }
  }

  getLeadUserElement(role) {
    const {currentProject: {users}} = this.props;
    let userElement;
    let user = users.find(elem=>elem.role.name === role);
    let name = '';
    if (user) {
      name = `${user.user.first_name || ''}${user.user.first_name ? ' ' : ''}${user.user.last_name || ''}`;
      if (name === '') {
        name = user.user.email;
      }
      userElement = (
        <div className="inline-block">
          <div className="face-block">
            <Image className="round-portrait" responsive src={user.user.avatar || defaultAvatar} alt="avatar"/>
          </div>
          <div className="human-block">
            <span className="human-name">
              {name}
            </span>
          </div>
        </div>
      );
    }
    return userElement;
  }

  getUserListElements(role) {
    const {allUsers, currentProject: {users}} = this.props;
    let results = allUsers;
    if (role) {
      let someUser = users.find(user=>user.role.name === (role !== 'lead developer' ? 'lead developer' : 'lead client'));
      if (someUser) {
        results = allUsers.filter(user=>user._id !== someUser.user._id);
      }
    }
    return results.map((user, index)=> {
      let name = '';
      name = `${user.first_name || ''}${user.first_name ? ' ' : ''}${user.last_name || ''}`;
      if (name === '') {
        name = user.email;
      }
      let checked = {};
      if (users.some(elem=>elem.user._id === user._id)) {
        checked = {
          rightIcon: <NavigationCheck className="nav-check"/>
        };
      }
      return (
        <ListItem
          key={index}
          primaryText={name}
          onClick={()=>this.onUserSelectChange.bind(this)(user, role)}
          leftAvatar={<Avatar src={user.avatar || defaultAvatar} />}
          {...checked}/>
      );
    });
  }

  getUserListOverlay(role) {
    return (
      <Popover id={snakeCase(role)} style={{maxHeight: 360, overflowY: 'auto'}}>
        <List>
          <ListItem
            primaryText={'Add new user'} style={{textAlign: 'center'}}
            onClick={()=> {this.refs[camelCase(role) + 'UserList'].hide(); this.setState({addNewUserModalOpen: true}); }}
          />
          {this.getUserListElements.bind(this)(role)}

        </List>
      </Popover>
    );
  }

  render() {
    const {currentProject, projectsActions, platforms, devices, options, allCompanies, isNew} = this.props;
    let scope = last(currentProject.scopes);

    let leadClientElement = this.getLeadUserElement('lead client');
    let leadDeveloperElement = this.getLeadUserElement('lead developer');

    let devicesElement;
    if (scope.platforms.length > 0) {
      devicesElement = scope.platforms.map(platform=>
        devices.filter(device=>device.platform._id === platform._id).map((device, index)=>
          <SquareBox key={index} icon={this.getDeviceIcon(device)}
                     checked={scope.devices.some(elem=>elem._id === device._id) ? true : false}
                     onClick={()=>this.onDeviceSelect.bind(this)(device)}
                     underText={device.name}/>)
      );
    } else {
      devicesElement = <div>You have to choose project platform.</div>;
    }

    let advancedElements;
    if (head(options).key === 'now') {
      advancedElements = (
        <div>
          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Select project platforms:
            </Col>
            <Col xs={12} sm={8} md={6}>
              {platforms.map((platform, index)=>
                <SquareBox key={index} icon={platform.name === 'ios' ? 'fa fa-apple' : platform.name}
                           checked={scope.platforms.some(elem=>elem._id === platform._id) ? true : false}
                           underText={platform.name === 'ios' ? 'iOS' : capitalize(platform.name)}
                           onClick={()=>this.onPlatformSelect.bind(this)(platform)}/>)}
            </Col>
          </Row>

          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Select project devices:
            </Col>
            <Col xs={12} sm={8} md={6}>
              {devicesElement}
            </Col>
          </Row>
        </div>
      );
    }

    const actions = [
      <FlatButton
        tabIndex={-1}
        label="Cancel"
        primary={true}
        onClick={this.onCancelNewUserModal.bind(this)}
      />,
      <FlatButton
        label="Add new User"
        type="submit"
        primary={true}
      />
    ];
    return (
      <Row>
        <Col xs={10} xsOffset={1} sm={12} smOffset={0}>
          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Select project name:*
            </Col>
            <Col xs={12} sm={8} md={6}>
              <Debounce time={200} handler="onChange">
                <input className="dashed-input" type="text" placeholder="Enter project name"
                       value={scope.name}
                       onChange={value=>projectsActions.currentProjectScopeFieldChange({name: value})}
                />
              </Debounce>
            </Col>
          </Row>
          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Select lead client:*
            </Col>
            <Col xs={12} sm={8} md={6}>
              <div className="">
                {leadClientElement}
                <OverlayTrigger
                  trigger="click"
                  placement="right"
                  ref="leadClientUserList"
                  rootClose
                  overlay={this.getUserListOverlay.bind(this)('lead client')}>
                  <div className="plus-block">
                    <a className="round-portrait dashed">
                      <ContentAdd/>
                    </a>
                  </div>
                </OverlayTrigger>
              </div>
            </Col>
          </Row>

          <Modal backdrop="static" show={this.state.addNewUserModalOpen}
                 onHide={()=>this.setState({ addNewUserModalOpen: false })}>
            <form onSubmit={this.onSubmitNewUserModal.bind(this)}>
              <Modal.Header>
                <Modal.Title>Add new user</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Row>
                  <Col xs={12}>
                    <div style={{marginTop: 20}}>Select existing company</div>
                    <SelectField value={this.state.newUser.company}
                                 fullWidth required
                                 errorText={this.state.newUser.companyError}
                                 onChange={(event, index, value)=>this.onNewUserFieldChange.bind(this, 'company')(value)}>
                      {allCompanies.length > 0 ? allCompanies.map((company, index)=>
                        <MenuItem key={index} value={company} primaryText={company.name}/>
                      ) : <MenuItem value={0} primaryText={'There are no companies yet!'}/>}
                    </SelectField>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <a role="button" onClick={()=>this.setState({addNewCompanyOpen: !this.state.addNewCompanyOpen})}>
                      Add new company
                    </a>
                    <Collapse in={this.state.addNewCompanyOpen} timeout={0}>
                      <div style={{padding: 20, border: '1px solid #d1d2d3'}}>
                        <div>Create a new company if you haven't found one you want in the list above</div>
                        <Row>
                          <form onSubmit={this.onAddNewCompany.bind(this)}>
                            <Col xs={12} sm={6}>
                              <Debounce time={200} handler="onChange">
                                <TextField
                                  fullWidth
                                  hintText="Enter Company name"
                                  floatingLabelText="Company name"
                                  floatingLabelFixed={true}
                                  onChange={value=> this.setState({newCompanyName: value})}
                                  value={this.state.newCompanyName}
                                  required
                                  minLength={3}
                                />
                              </Debounce>
                            </Col>
                            <Col xs={12} sm={6}>
                              <RaisedButton label="Add new company" type="submit" primary={true} fullWidth
                                            style={{width: '100%', marginTop: 20}}
                                            disabled={!this.state.newCompanyName || this.state.newCompanyName.trim().length < 3}
                              />
                            </Col>
                          </form>
                        </Row>
                      </div>
                    </Collapse>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <Debounce time={200} handler="onChange">
                      <TextField
                        fullWidth
                        hintText="Enter first name"
                        floatingLabelText="First name"
                        floatingLabelFixed={true}
                        onChange={value=>this.onNewUserFieldChange.bind(this, 'first_name')(value)}
                        value={this.state.newUser.first_name}
                        required
                        minLength={3}
                      />
                    </Debounce>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <Debounce time={200} handler="onChange">
                      <TextField
                        fullWidth
                        hintText="Enter last name"
                        floatingLabelText="Last name"
                        floatingLabelFixed={true}
                        onChange={value=>this.onNewUserFieldChange.bind(this, 'last_name')(value)}
                        value={this.state.newUser.last_name}
                        required
                        minLength={3}
                      />
                    </Debounce>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <Debounce time={200} handler="onChange">
                      <TextField
                        type="email"
                        fullWidth
                        hintText="Enter email"
                        floatingLabelText="Email"
                        floatingLabelFixed={true}
                        onChange={value=>this.onNewUserFieldChange.bind(this, 'email')(value)}
                        value={this.state.newUser.email}
                        required
                        minLength={3}
                      />
                    </Debounce>
                  </Col>
                </Row>
              </Modal.Body>
              <Modal.Footer>
                {actions}
              </Modal.Footer>
            </form>
          </Modal>

          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Select lead developer:*
            </Col>
            <Col xs={12} sm={8} md={6}>
              <div className="">
                {leadDeveloperElement}
                <OverlayTrigger
                  trigger="click"
                  placement="right"
                  ref="leadDeveloperUserList"
                  rootClose
                  overlay={this.getUserListOverlay.bind(this)('lead developer')}>
                  <div className="plus-block">
                    <a className="round-portrait dashed">
                      <ContentAdd/>
                    </a>
                  </div>
                </OverlayTrigger>
              </div>
            </Col>
          </Row>
          {isNew ?
            (
              <Row style={{marginBottom: 20}}>
                <Col xs={12} sm={4} md={6}>
                  Select project brief option:*
                </Col>
                <Col xs={12} sm={8} md={6}>
                  {this.state.options.map((option, index)=>
                    <SquareBox key={index} icon={option.icon}
                               checked={options.some(elem=>elem.name === option.name)}
                               underText={option.name}
                               onClick={()=>this.onOptionsChange.bind(this)(option)}/>
                  )}
                </Col>
              </Row>
            ) : ''}
          {advancedElements}
        </Col>
      </Row>
    );
  }
}

CreateProjectStep1.propTypes = {
  userSuccess: PropTypes.bool.isRequired,
  companiesActions: PropTypes.object.isRequired,
  usersActions: PropTypes.object.isRequired,
  projectsActions: PropTypes.object.isRequired,
  allCompanies: PropTypes.array.isRequired,
  allUsers: PropTypes.array.isRequired,
  platforms: PropTypes.array.isRequired,
  devices: PropTypes.array.isRequired,
  options: PropTypes.array.isRequired,
  onFieldChange: PropTypes.func.isRequired,
  onSaveScope: PropTypes.func.isRequired,
  isNew: PropTypes.bool.isRequired,
  currentProject: PropTypes.object.isRequired
};


export default CreateProjectStep1;
