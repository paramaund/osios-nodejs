import React, {Component, PropTypes} from 'react';
import {Row, Col} from 'react-bootstrap';
import debounce from 'lodash/debounce';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import remove from 'lodash/remove';

class AccountNotifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {notifications: []},
      notifications: [
        {key: -14, title: '14 Days Before'},
        {key: -7, title: '7 Days Before'},
        {key: -3, title: '3 Days Before'},
        {key: -1, title: '1 Day Before'},
        {key: 0, title: 'On The Day Of'},
        {key: 1, title: '1 Day After'},
        {key: 3, title: '3 Days After'},
        {key: 7, title: '7 Days After'},
        {key: 14, title: '14 Days After'}
      ]
    };

    this.userFieldDebounceWrapper = debounce((value, index)=> {
      if (value) {
        this.state.user.notifications.push(this.state.notifications[index].key);
      } else {
        remove(this.state.user.notifications, el=>el === this.state.notifications[index].key);
      }
      this.forceUpdate();
    });
    this.submitDebounceWrapper = debounce(this.onSubmit.bind(this), 200);
  }

  componentWillMount() {
    const {currentUser} = this.props;
    if (currentUser) {
      this.state.user = Object.assign({}, currentUser);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {currentUser} = nextProps;
    if (currentUser) {
      this.state.user = Object.assign({}, currentUser, {notifications: this.state.user.notifications});
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.slideIndex === nextProps.slideIndex;
  }

  onSubmit() {
    const {usersActions} = this.props;
    usersActions.updateMe(Object.assign({}, this.state.user));
  }

  render() {
    const {currentUser} = this.props;
    return (
      <div style={{marginTop: 20}}>
        <div style={{padding: 20, border: '1px solid #d1d2d3'}}>
          <form onSubmit={event => { event.preventDefault(); this.submitDebounceWrapper(); }}>
            <Row>
              <Col xs={12} sm={6}>
                <a role="button"><h3>Notifications</h3></a>
              </Col>
              <Col xs={12} sm={6}>
                {this.state.notifications.map((notification, index)=>
                  <Row key={index}>
                    <Col xs={12}>
                      <Checkbox
                        label={notification.title}
                        labelStyle={{fontWeight: 500, fontSize: 16}}
                        checked={this.state.user.notifications.some(el=>el === this.state.notifications[index].key)}
                        onCheck={(event, isInputChecked)=>this.userFieldDebounceWrapper(isInputChecked, index)}
                      />
                    </Col>
                  </Row>)}
              </Col>
            </Row>
            <Row style={{marginTop: 20}}>
              <Col xs={12} sm={8} smOffset={2} className="text-center">
                Decide how many notifications and at what intervals they are sent.
                Notifications are only sent based on a milestone that has not been completed.
              </Col>
            </Row>
            <Row>
              <Col xs={12} className="text-center">
                <RaisedButton
                  type="submit"
                  label="Save"
                  primary={true}
                  style={{marginTop: 20}}
                  labelStyle={{fontWeight: 900}}
                />
              </Col>
            </Row>
          </form>
        </div>
      </div>
    );
  }
}

AccountNotifications.propTypes = {
  slideIndex: PropTypes.number.isRequired,
  currentUser: PropTypes.object.isRequired,
  usersActions: PropTypes.object.isRequired
};

export default AccountNotifications;
