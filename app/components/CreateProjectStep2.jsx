import React, {PropTypes, Component} from 'react';
import {Row, Col, Image} from 'react-bootstrap';
import {SketchPicker} from 'react-color';
import Dropzone from 'react-dropzone';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import last from 'lodash/last';
import Debounce from './Debounce';
import defaultImagePreview from '../assets/img/NoPreviewGraphic.png';

class CreateProjectStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      removeFileShow: []
    };
  }

  componentWillReceiveProps(nextProps) {

  }

  onSelectColorChange(color) {
    const {projectsActions, currentProject} = this.props;
    let scope = last(currentProject.scopes);
    let colors = [...scope.colors];
    colors[this.state.selectedColorIndex] = color.hex;
    projectsActions.currentProjectScopeFieldChange({colors: colors});
  }

  getColorPickerElement(scope) {
    let elem;
    if (this.state.showColorPicker) {
      elem = (
        <div tabIndex={-1}
             style={{position: 'fixed', top: 0, left: 0, width: '100%', height: '100%', zIndex: 1000, backgroundColor: 'transparent', overflowY: 'auto'}}
             onClick={()=>this.setState({showColorPicker: false})}>
          <div style={{position: 'absolute', top: '30%', left: '40%'}}
               onClick={event=>event.stopPropagation()}>
            <SketchPicker
              color={ scope.colors[this.state.selectedColorIndex] }
              onChangeComplete={this.onSelectColorChange.bind(this)}
            />
          </div>
        </div>
      );
    }
    return elem;
  }

  onFileDrop(files) {
    const {currentProject, projectsActions} = this.props;
    let scope = last(currentProject.scopes);
    let currentFilesCount = scope.files.length;
    if (currentFilesCount >= 5) {
      return;
    }
    let canBeUploadedCount = 5 - currentFilesCount;
    let results = files.slice(0, canBeUploadedCount);

    let data = new FormData();
    results.forEach(file=>data.append('files', file));

    projectsActions.currentProjectFileUpload(data);
  }

  onRemoveFileRequest(index) {
    const {currentProject, projectsActions} = this.props;
    let scope = last(currentProject.scopes);
    let files = [...scope.files];
    files.splice(index, 1);
    projectsActions.currentProjectScopeFieldChange({files: files});
  }

  render() {
    const {currentProject, projectsActions} = this.props;
    let scope = last(currentProject.scopes);
    let colorPickerElement = this.getColorPickerElement.bind(this)(scope);
    let filesPreview = this._renderDropZone(scope);
    return (
      <Row>
        {colorPickerElement}
        <Col xs={10} xsOffset={1} sm={12} smOffset={0}>
          <Row className="text-center">
            <h1 style={{fontWeight: 300}}>{scope.name}</h1>
          </Row>
          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Project description:*
            </Col>
            <Col xs={12} sm={8} md={6}>
              <Debounce time={200} handler="onChange">
              <textarea className="dashed-input" placeholder="Enter details about project's requirements"
                        value={scope.description} rows={4} style={{resize: 'none'}}
                        onChange={value=>projectsActions.currentProjectScopeFieldChange({description: value})}
              />
              </Debounce>
            </Col>
          </Row>

          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              The intended users:
            </Col>
            <Col xs={12} sm={8} md={6}>
              <Debounce time={200} handler="onChange">
              <textarea className="dashed-input" placeholder="Enter details about project's intended users"
                        value={scope.intended_users} rows={4} style={{resize: 'none'}}
                        onChange={value=>projectsActions.currentProjectScopeFieldChange({intended_users: value})}
              />
              </Debounce>
            </Col>
          </Row>

          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Design direction:
            </Col>
            <Col xs={12} sm={8} md={6}>
              <Debounce time={200} handler="onChange">
              <textarea className="dashed-input" placeholder="Enter information to guide the project's design needs"
                        value={scope.design_direction} rows={4} style={{resize: 'none'}}
                        onChange={value=>projectsActions.currentProjectScopeFieldChange({design_direction: value})}
              />
              </Debounce>
            </Col>
          </Row>
          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Desired colors:
            </Col>
            <Col xs={12} sm={8} md={6}>
              <Row>
                {scope.colors.map((color, index)=>
                  <Col key={index} xs={6} md={3}>
                    <div className="dashed-colorpicker">
                      <div className="colorpicker-container">
                        <input type="button" className="colorpicker dash" style={{backgroundColor: color}}
                               value={color}
                               onClick={()=>this.setState({selectedColorIndex: index, showColorPicker: true})}/>
                        <span className="" role="button"
                              onClick={()=>this.setState({selectedColorIndex: index, showColorPicker: true})}>
                          Select colour
                        </span>
                      </div>
                      <div className="hex-container">
                        <span id="hexShow" className="">{color}</span>
                      </div>
                      <div className="hex-container" role="button" style={{cursor: 'pointer'}}
                           onClick={()=>projectsActions.currentProjectScopeFieldChange({colors: [...scope.colors.slice(0, index), ...scope.colors.slice(index + 1)]})}>
                        <span id="hexShow2">Delete</span>
                      </div>
                    </div>
                  </Col>
                )}
                <Col xs={6} md={3} className="text-center">
                  <div className="add-color" role="button"
                       onClick={()=>projectsActions.currentProjectScopeFieldChange({colors: [...scope.colors, '#A0A1A3']})}>
                    <FloatingActionButton
                      mini={true} style={{marginTop: '40%'}}
                    >
                      <ContentAdd />
                    </FloatingActionButton>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Design fonts:
            </Col>
            <Col xs={12} sm={8} md={6}>
              <Debounce time={200} handler="onChange">
              <textarea className="dashed-input" placeholder="Enter any font requirements"
                        value={scope.fonts} rows={4} style={{resize: 'none'}}
                        onChange={value=>projectsActions.currentProjectScopeFieldChange({fonts: value})}
              />
              </Debounce>
            </Col>
          </Row>
          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Technical/feature requirements:
            </Col>
            <Col xs={12} sm={8} md={6}>
              <Debounce time={200} handler="onChange">
              <textarea className="dashed-input" placeholder="Enter information to guide the project's design needs"
                        value={scope.tech_requirements} rows={4} style={{resize: 'none'}}
                        onChange={value=>projectsActions.currentProjectScopeFieldChange({tech_requirements: value})}
              />
              </Debounce>
            </Col>
          </Row>
          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Additional notes:
            </Col>
            <Col xs={12} sm={8} md={6}>
              <Debounce time={200} handler="onChange">
              <textarea className="dashed-input" placeholder="Enter information about additional needs"
                        value={scope.notes} rows={4} style={{resize: 'none'}}
                        onChange={value=>projectsActions.currentProjectScopeFieldChange({notes: value})}
              />
              </Debounce>
            </Col>
          </Row>
          <Row style={{marginBottom: 20}}>
            <Col xs={12} sm={4} md={6}>
              Add file/s:
            </Col>
            <Col xs={12} sm={8} md={6}>
              <Dropzone onDrop={this.onFileDrop.bind(this)} className="dashed-input"
                        multiple={true}
                        style={{height: 'auto', width: 'auto', minHeight: 200, minWidth: 200}}>
                <Row>
                  {filesPreview}
                </Row>
              </Dropzone>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }

  _renderDropZone(scope) {
    let imageExtentions = ['jpg', 'jpeg', 'png', 'svg', 'gif', 'ico', 'bmp', 'pjpeg'];
    let preview = <div style={{fontSize: 16}}>Try dropping some files here, or click to select files to upload.</div>;
    if (scope.files.length > 0) {
      preview = scope.files.map((file, index)=> {
        let imagePreview = file.url;
        let fileExt = imagePreview.split('.').pop();

        if (imageExtentions.every(ext=>ext !== fileExt)) {
          imagePreview = defaultImagePreview;
        }
        return (
          <Col xs={4} style={{marginBottom: 10}}>
            <div key={index} style={{position: 'relative', overflow: 'hidden', border: '1px solid #d1d2d3'}}
                   onClick={event=>event.stopPropagation()}
                   className="file-preview">
              <Image src={imagePreview} alt={file.name}
                     style={{width: '100%', fontSize: 18, minHeight: 100}}
                     responsive thumbnail/>
              <div
                style={{color: 'black', width: '100%', position:'absolute', bottom:0, textAlign: 'center', fontSize: 12}}>
                {file.name}
              </div>
              <div
                className="file-remove-backdrop"
                onClick={()=>this.onRemoveFileRequest.bind(this)(index)}>
                <div>remove</div>
              </div>
            </div>
          </Col>
        );
      });
    }
    return preview;
  }
}

CreateProjectStep2.propTypes = {
  projectsActions: PropTypes.object.isRequired,
  onSaveScope: PropTypes.func.isRequired,
  currentProject: PropTypes.object.isRequired
};


export default CreateProjectStep2;
