import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Helmet from 'react-helmet';

import config from '../helmetConfig';

if (__DEV__) {
  config.link = config.link.filter(l => l.rel !== 'stylesheet');
}

class Meta extends React.Component {
  render() {
    return (
      <Helmet
        title="OSIOS"
        meta={config.meta}
        link={config.link}
      />
    );
  }
}

ReactDOMServer.renderToString(<Meta />);
let header = Helmet.rewind();

export default header;
