import React, {PropTypes, Component} from 'react';
import {Row, Col, Image} from 'react-bootstrap';
import LinearProgress from 'material-ui/LinearProgress';
import step3background from '../assets/img/shutterstock_176983268_1200px.jpg';

class CreateProjectStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(nextProps) {

  }

  render() {
    const {progress} = this.props;

    return (
      <Row>
        <Image src={step3background} className="backdrop" responsive/>
        <div className="backdrop">
          <div className="create-project-end">
            <h1 className="text-center" style={{color: 'black'}}>Your project is being processed.</h1>
            <LinearProgress mode="determinate" value={progress}
                            className="create-project-progress-bar-end"/>
          </div>
        </div>
      </Row>
    );
  }

}

CreateProjectStep3.propTypes = {
  progress: PropTypes.number.isRequired
};


export default CreateProjectStep3;
