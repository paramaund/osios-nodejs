import React, {Component, PropTypes} from 'react';
import Formsy from 'formsy-react';
import {FormsyText, FormsySelect, FormsyCheckbox} from 'formsy-material-ui/lib';
import {Row, Col} from 'react-bootstrap';
import moment from 'moment';
import isUndefined from 'lodash/isUndefined';
import forOwn from 'lodash/forOwn';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import MenuItem from 'material-ui/MenuItem';

Formsy.addValidationRule('min', (values, value, minValue)=> {
  return value >= minValue;
});
Formsy.addValidationRule('max', (values, value, maxValue)=> {
  return value <= maxValue;
});

class AccountFinance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      open: false,
      selectedPlan: {}
    };
  }

  componentWillMount() {
    const {currentUser, paymentPlansActions, selectedPlan, plans} = this.props;
    if (currentUser) {
      this.state.user = Object.assign({}, currentUser);
      this.state.selectedPlan = Object.assign({}, selectedPlan ? plans.find(plan=>plan.key === selectedPlan) : currentUser.payment_plan);
    }
    if (paymentPlansActions) {
      paymentPlansActions.fetch();
    }
  }

  componentWillReceiveProps(nextProps) {
    const {currentUser, selectedPlan, plans} = nextProps;
    if (currentUser) {
      this.state.user = Object.assign({}, currentUser);
      if (this.state.selectedPlanKey !== nextProps.selectedPlan) {
        this.state.selectedPlan = Object.assign({}, selectedPlan ? plans.find(plan=>plan.key === selectedPlan) : currentUser.payment_plan);
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.slideIndex === nextProps.slideIndex || this.state.selectedPlanKey !== nextProps.selectedPlan;
  }

  componentWillUpdate(nextProps, nextState) {
    this.state.selectedPlanKey = nextProps.selectedPlan;
  }

  onPlanSelect(value) {
    const {plans} = this.props;
    this.props.onPlanSelect(1);
    this.setState({selectedPlan: Object.assign(this.state.selectedPlan, plans.find(plan=>plan.key === value))});
  }

  onEnableButton() {
    this.setState({
      canSubmit: true
    });
  }

  onDisableButton() {
    this.setState({
      canSubmit: false
    });
  }

  onSubmit(data, reset, invalidate) {
    const {subscribeActions} = this.props;
    subscribeActions.create(data);
    reset();
    forOwn(this.refs, value=> {
      value.setState({value: ''});
    });
  }

  onInvalidSubmit(data, reset, invalidate) {
    let errors = {};
    forOwn(data, (value, key)=> {
      if (isUndefined(value) || value === '') {
        errors[key] = `${key} field is required.`;
      }
      if (key === 'confirm' && !key) {
        errors[key] = `You have to agree to the terms and conditions.`;
      }
    });
    invalidate(errors);
  }

  onOpen() {
    this.setState({open: true});
  }

  onClose() {
    this.setState({open: false});
  }

  onDeleteAccount() {
    const {usersActions, authActions} = this.props;
    usersActions.deleteMe();
    authActions.logout();
  }

  render() {
    const {currentUser, plans} = this.props;
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        // keyboardFocused={true}
        onClick={this.onClose.bind(this)}
      />,
      <FlatButton
        label="Delete"
        primary={true}
        onClick={this.onDeleteAccount.bind(this)}
      />
    ];

    let minYear = Number(moment().subtract(10, 'years').format('YY'));
    let maxYear = Number(moment().add(10, 'years').format('YY'));

    return (
      <div>
        <Row style={{marginTop: 20}}>
          <Col xs={12} sm={4}>
            <h1 style={{fontSize: '2.5rem'}}>Subscription</h1>
          </Col>
          <Col xs={12} sm={8}>
            <div className="text-center" style={{padding: 20, border: '1px solid #d1d2d3'}}>
              <Formsy.Form
                noValidate
                onValid={this.onEnableButton.bind(this)}
                onInvalid={this.onDisableButton.bind(this)}
                onValidSubmit={this.onSubmit.bind(this)}
                onInvalidSubmit={this.onInvalidSubmit.bind(this)}
                style={{marginBottom: 20}}>
                <Row>
                  <Col xs={4}>
                    <FormsySelect
                      name="payment_plan"
                      required
                      value={this.state.selectedPlan.key}
                      fullWidth
                      onChange={(event, index, value)=>this.setState({selectedPlan: value})}
                    >
                      {plans.map((elem, index)=>
                        <MenuItem key={index} value={elem.key} primaryText={elem.name}/>
                      )}
                    </FormsySelect>
                  </Col>
                  <Col xs={8}>
                    <FormsyCheckbox
                      name="tax"
                      defaultValue={false}
                      label="I am in the EU (VAT will be added to your subscription)."
                      labelStyle={{fontWeight: 500, fontSize: 16}}
                      style={{width: '100%'}}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col xs={12} xsOffset={0}>
                    <FormsyText
                      name="card"
                      ref="card"
                      fullWidth
                      required
                      hintText="Card Number"
                      floatingLabelText="Your Card Number"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col xs={4}>
                    <FormsyText
                      name="month"
                      ref="month"
                      fullWidth
                      required
                      type="number"
                      validations="min:1,max:12"
                      validationErrors={{min: 'Min month value is 1.', max: 'Max month value is 12'}}
                      hintText="mm"
                      floatingLabelText="Month"
                    />
                  </Col>
                  <Col xs={4}>
                    <FormsyText
                      name="year"
                      ref="year"
                      fullWidth
                      required
                      type="number"
                      validations={{min: minYear, max: maxYear}}
                      validationErrors={{min: `Min year value is ${minYear}`, max: `Max year value is ${maxYear}`}}
                      hintText="YY"
                      floatingLabelText="Year"
                    />
                  </Col>
                  <Col xs={4}>
                    <FormsyText
                      name="cvc"
                      ref="cvc"
                      fullWidth
                      required
                      type="number"
                      validations="min:100,max:9999"
                      validationError="Invalid cvc number"
                      hintText="cvc"
                      floatingLabelText="CVC"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col xs={12} xsOffset={0}>
                    <FormsyCheckbox
                      name="confirm"
                      validations="isTrue"
                      label="I agree to the terms and conditions."
                      labelStyle={{fontWeight: 500, fontSize: 16}}
                      style={{marginTop: 20}}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col xs={12} xsOffset={0}>
                    <RaisedButton
                      type="submit"
                      label="Subscribe"
                      disabled={!this.state.canSubmit}
                      primary={true}
                      style={{marginTop: 20}}
                      labelStyle={{fontWeight: 900}}
                    />
                  </Col>
                </Row>
              </Formsy.Form>
            </div>
          </Col>
        </Row>
        <hr/>
        <Row style={{marginTop: 20}}>
          <Col xs={12} sm={4}>
            <h1 style={{fontSize: '2.5rem'}}>Delete Account</h1>
          </Col>
          <Col xs={12} sm={8}>
            <div className="text-center" style={{padding: 20, border: '1px solid #d1d2d3'}}>
              <div>
                <span>You can cancel your membership at any time. </span>
                <a role="button" onClick={this.onOpen.bind(this)}>Click here to cancel your account. </a>
                <span>All projects, files and messages will be deleted immediately.</span></div>
            </div>
          </Col>
        </Row>
        <Dialog
          title="Delete"
          actions={actions}
          modal={true}
          open={this.state.open}
        >
          Are you sure you want to delete your account?
        </Dialog>
      </div>
    );
  }
}

AccountFinance.propTypes = {
  slideIndex: PropTypes.number.isRequired,
  onPlanSelect: PropTypes.func.isRequired,
  selectedPlan: PropTypes.string.isRequired,
  plans: PropTypes.array.isRequired,
  currentUser: PropTypes.object.isRequired,
  paymentPlansActions: PropTypes.object.isRequired,
  subscribeActions: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired,
  usersActions: PropTypes.object.isRequired
};

export default AccountFinance;
