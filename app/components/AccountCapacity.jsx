import React, {Component, PropTypes} from 'react';
import {Row, Col} from 'react-bootstrap';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';

class AccountCapacity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}
    };
  }

  componentWillMount() {
    const {currentUser} = this.props;
    if (currentUser) {
      this.state.user = Object.assign({}, currentUser);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {currentUser} = nextProps;
    if (currentUser) {
      this.state.user = Object.assign({}, currentUser);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.slideIndex === nextProps.slideIndex;
  }

  onPlanSelect(tabNumber, key) {
    this.props.onPlanSelect(tabNumber, key);
  }

  onSubmit() {
    const {usersActions} = this.props;
    usersActions.updateMe(Object.assign({}, this.state.user));
  }

  getPlanButtonStyle(plan) {
    const {currentUser} = this.props;
    if (!currentUser.payment_plan) {
      return false;
    }
    return currentUser.payment_plan.price < plan.price ? false : (currentUser.payment_plan.key !== plan.key);
  }

  getLabelForPlanButton(plan) {
    const {currentUser} = this.props;
    if (!currentUser.payment_plan) {
      return 'Change';
    }
    return currentUser.payment_plan.price < plan.price ? 'Change' : (currentUser.payment_plan.key === plan.key ? 'Current' : 'Upgrade');
  }

  render() {
    const {currentUser, plans, statistics} = this.props;
    return (
      <div>
        <Row style={{marginTop: 20}}>
          <Col xs={12}>
            <div style={{padding: 20, border: '1px solid #d1d2d3'}}>
              <Row>
                <Col xs={12}>
                  <Table>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                      <TableRow>
                        <TableHeaderColumn style={{textAlign: 'center', fontWeight: 900}}>
                          Total Projects
                        </TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center', fontWeight: 900}}>
                          Total Collaborators
                        </TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center', fontWeight: 900}}>
                          Total Attachments
                        </TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center', fontWeight: 900}}>
                          Total Storage
                        </TableHeaderColumn>
                        <TableHeaderColumn style={{textAlign: 'center', fontWeight: 900}}>
                          Total Messages
                        </TableHeaderColumn>
                      </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                      <TableRow>
                        <TableRowColumn style={{textAlign: 'center'}}>{statistics.projects}</TableRowColumn>
                        <TableRowColumn style={{textAlign: 'center'}}>{statistics.collaborators}</TableRowColumn>
                        <TableRowColumn style={{textAlign: 'center'}}>0</TableRowColumn>
                        <TableRowColumn style={{textAlign: 'center'}}>0</TableRowColumn>
                        <TableRowColumn style={{textAlign: 'center'}}>0</TableRowColumn>
                      </TableRow>
                    </TableBody>
                  </Table>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
        <Row style={{marginTop: 20}}>
          {plans.map((plan, index)=>
            <Col xs={12} sm={4} key={index}>
              <div style={{padding: 20, border: '1px solid #d1d2d3'}} className="text-center">
                <h2>{plan.name}</h2>
                <h3>Price:
                  <small> {plan.price}</small>
                </h3>
                <h3>Projects:
                  <small> {plan.projects}</small>
                </h3>
                <h3>Storage space:
                  <small> {plan.storage_space} Gb</small>
                </h3>
                <RaisedButton
                  label={this.getLabelForPlanButton.bind(this)(plan)}
                  primary={this.getPlanButtonStyle.bind(this)(plan)}
                  onClick={this.onPlanSelect.bind(this, 1, plan.key)}/>
              </div>
            </Col>)}
        </Row>
      </div>
    );
  }
}

AccountCapacity.propTypes = {
  slideIndex: PropTypes.number.isRequired,
  currentUser: PropTypes.object.isRequired,
  plans: PropTypes.array.isRequired,
  onPlanSelect: PropTypes.func.isRequired,
  statistics: PropTypes.object.isRequired,
  // usersActions: PropTypes.object.isRequired
};

export default AccountCapacity;
