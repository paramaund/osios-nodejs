import React, {Component, PropTypes} from 'react';
import {debounce} from 'lodash';

const DEFAULT_TIME = 200;

class Debounce extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const {children} = this.props;
    this.state.value = children.props.value;
  }

  componentWillReceiveProps(nextProps) {
    const {children} = nextProps;
    this.state.value = children.props.value;
  }

  onChangeHandle(event) {
    const {children, handler, time} = this.props;
    if (!this.debounce) {
      this.debounce = debounce(ev=>children.props[handler](ev), time ? time : DEFAULT_TIME);
    }
    this.setState({ value: event.target.value });
    this.debounce(event.target.value);
  }

  render() {
    const {children, handler} = this.props;

    let child = Object.assign({}, children, {
      props: Object.assign({}, children.props, {
        [handler]: this.onChangeHandle.bind(this),
        value: this.state.value
      })
    });
    return (
      <div>
        {child}
      </div>
    );
  }
}

Debounce.propTypes = {
  children: PropTypes.object.isRequired,
  time: PropTypes.number,
  handler: PropTypes.string.isRequired
};

export default Debounce;
