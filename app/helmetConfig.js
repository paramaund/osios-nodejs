// import {APP_PREFIX} from 'constants/index';
import favicon from 'assets/img/favicon.png';
const config = {
  link: [
    {'rel': 'stylesheet', 'href': '/assets/styles/main.css'},

    // Add to homescreen for Chrome on Android
    {rel: 'shortcut icon', href: favicon},
    // { rel: 'icon', sizes: '192x192', href: mobilecon}, // TODO: not work, set also for browsers
    // Add to homescreen for Safari on IOS
    {rel: 'apple-touch-icon', /*sizes: '152x152',*/ href: favicon}
    // SEO: If your mobile URL is different from the desktop URL,
    // add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones
    // { 'rel': 'canonical', 'href': 'http://www.example.com/' }
  ],
  meta: [
    {'charset': 'utf-8'},
    {'http-equiv': 'X-UA-Compatible', 'content': 'IE=edge'},
    {'name': 'description', 'content': 'OSIOS application'},
    {'name': 'viewport', 'content': 'width=device-width, initial-scale=1'},
    {'name': 'mobile-web-app-capable', 'content': 'yes'},
    {'name': 'apple-mobile-web-app-capable', 'content': 'yes'},
    {'name': 'apple-mobile-web-app-status-bar-style', 'content': 'black'},
    {'name': 'apple-mobile-web-app-title', 'content': 'OSIOS'},
    // {'name': 'msapplication-TileColor', 'content': '#3372DF'}
  ]
};

export default config;
