import React, {PropTypes, Component} from 'react';
import {Row, Col, Image, Tooltip, OverlayTrigger} from 'react-bootstrap';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {browserHistory} from 'react-router';
import * as roomsSimpleActions from '../actions/rooms';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import last from 'lodash/last';
import {ISBROWSER} from '../constants/index';
import {moxtra as moxtraSecrets} from '../../server/config/secrets';

class RoomPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 0
    };
    this.tabs = [
      {title: 'Chat'},
      {title: 'Files'},
      {title: 'To-Do'},
      {title: 'Meet'}
    ];
  }

  componentWillMount() {
    const {roomsActions, currentUser, params:{roomId}} = this.props;
    roomsActions.fetchById(roomId);
    if (ISBROWSER) {
      const script = document.createElement('script');

      script.src = 'https://www.moxtra.com/api/js/moxtra-latest.js';
      script.async = true;
      script.onload = ()=> {
        if (currentUser._id) {
          var e = this.init(currentUser);console.dir(e)
          Moxtra.init(e);
        }
      };
      document.body.appendChild(script);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.roomId !== nextProps.params.roomId) {
      const {roomsActions, params:{roomId}} = nextProps;
      roomsActions.fetchById(roomId);
    }
    if (!this.props.currentUser._id && nextProps.currentUser._id) {
      if (Moxtra) {
        Moxtra.init(this.init(nextProps.currentUser));
      }
    }
  }

  componentWillUpdate(nextProps, nextState) {

  }

  onSelectedRoomChange(event, index, value) {
    const {project} = this.props;
    browserHistory.push(`/projects/${project._id}/rooms/${value}`);
  }

  onTabChange(value) {
    this.setState({
      slideIndex: value
    });
  }

  render() {
    const {rooms, currentRoom} = this.props;

    return (
      <div style={{padding: 20, border: '1px solid #d1d2d3'}}>
        <Row>
          <Col xs={12}>
            Room:
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={6}>
            <SelectField
              autoWidth
              value={currentRoom._id}
              onChange={this.onSelectedRoomChange.bind(this)}
            >
              {rooms.map((room, index)=> {
                let roomName = currentRoom._id === room._id ? 'Current' : room.name;
                return (<MenuItem key={index} value={room._id} primaryText={roomName}/>);
              })}
            </SelectField>
          </Col>
        </Row>
        <Row>
          <hr style={{marginTop: 5}}/>
        </Row>
        <Row>
          <Tabs
            style={{borderBottom: '1px solid lightgrey'}}
            tabItemContainerStyle={{backgroundColor: 'transparent'}}
            inkBarStyle={{backgroundColor: '#29abe2'}}
            onChange={this.onTabChange.bind(this)}
            value={this.state.slideIndex}>
            {this.tabs.map((tab, index)=> {
              let isActive = this.isActiveSlide.bind(this, index)();
              return (
                <Tab key={index}
                     className="account-tab"
                     style={{color: isActive ? '#29abe2' : 'grey', fontWeight: isActive ? 'bold' : 'normal', padding: '5px 0 10px'}}
                     label={tab.title} value={index}/>
              );
            })}
          </Tabs>
          <SwipeableViews
            slideStyle={{overflow: 'hidden', marginTop: 20}} containerStyle={{height: 'auto !important'}}
            index={this.state.slideIndex}
            onChangeIndex={this.onTabChange.bind(this)}>
            <Col xs={12}>
              <div>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aperiam cumque dolorum
                eveniet id minus nobis odio officiis omnis perferendis, quasi, reiciendis sed sequi, similique soluta
                suscipit vel veniam.
              </div>
            </Col>
            <Col xs={12}>
              <div>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aperiam cumque dolorum
                eveniet id minus nobis odio officiis omnis perferendis, quasi, reiciendis sed sequi, similique soluta
                suscipit vel veniam.
              </div>
            </Col>
            <Col xs={12}>
              <div>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aperiam cumque dolorum
                eveniet id minus nobis odio officiis omnis perferendis, quasi, reiciendis sed sequi, similique soluta
                suscipit vel veniam.
              </div>
            </Col>
            <Col xs={12}>
              <div>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aperiam cumque dolorum
                eveniet id minus nobis odio officiis omnis perferendis, quasi, reiciendis sed sequi, similique soluta
                suscipit vel veniam.
              </div>
            </Col>
          </SwipeableViews>
        </Row>
      </div>
    );
  }

  isActiveSlide(index) {
    return this.state.slideIndex === index;
  }

  init(currentUser) {
    return {
      mode: moxtraSecrets.mode,
      client_id: moxtraSecrets.clientId,
      access_token: currentUser.moxtraAccessToken,
      invalid_token: event=> {
        alert('Access Token expired for session id: ' + event.session_id);
      }
    };
  }
}

RoomPage.propTypes = {
  currentRoom: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
  roomsActions: PropTypes.object.isRequired,
  project: PropTypes.object.isRequired,
  // permissions: PropTypes.array.isRequired,
  // companies: PropTypes.array.isRequired,
  // success: PropTypes.bool.isRequired,
  params: PropTypes.object.isRequired,
  rooms: PropTypes.array.isRequired
};

function select(state) {
  return {
    // success: state.rooms.success,
    rooms: state.rooms.rooms,
    currentRoom: state.rooms.currentRoom,
    currentUser: state.users.currentUser,
    // companies: state.companies.companies,
    // permissions: state.rooms.permissions
  };
}

function mapDispatchToProps(dispatch) {
  return {
    roomsActions: bindActionCreators(roomsSimpleActions, dispatch)
  };
}

export default connect(select, mapDispatchToProps)(RoomPage);
