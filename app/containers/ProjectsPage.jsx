import React, {PropTypes, Component} from 'react';
import {Row, Col} from 'react-bootstrap';
import {connect} from 'react-redux';
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import {bindActionCreators} from 'redux';
import Spinner from '../components/Spinner';
import * as projects from '../actions/projects';
import ProjectsTabPage from '../components/ProjectsTabPage';

class ProjectsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: {
        active: [],
        completed: [],
        deleted: []
      },
      slideIndex: 0
    };
    this.tabs = [
      {title: 'All active projects'},
      {title: 'Completed projects'},
      {title: 'Recently deleted projects'}
    ];
  }

  componentWillMount() {
    const {projectsActions} = this.props;
    projectsActions.fetchStatuses();
  }

  componentWillReceiveProps(nextProps) {
    const {projectsState:{projects}} = nextProps;
    // this.state.projects.active = projects
    //   .filter(project=>project.status.name !== 'completed' && project.status.name !== 'deleted');
    // this.state.projects.completed = projects.filter(project=>project.status.name === 'completed');
    // this.state.projects.deleted = projects.filter(project=>project.status.name === 'deleted');
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return this.props.requestsInProcess === nextProps.requestsInProcess && nextProps.requestsInProcess === 0;
  // }

  componentWillUpdate(nextProps, nextState) {
  }

  onTabChange(value) {
    this.setState({
      slideIndex: value
    });
  }

  render() {
    const {
      projectsState: {
        activeStatistics,
        completedStatistics,
        deletedStatistics,
        statuses,
        total,
        projects
      }, projectsActions, currentUser, requestsInProcess
    } = this.props;

    let spinnerElement;
    if (requestsInProcess > 0) {
      spinnerElement = (
        <Spinner/>
      );
    }

    return (
      <Row>
        <Tabs
          tabItemContainerStyle={{backgroundColor: 'transparent'}}
          inkBarStyle={{backgroundColor: '#29abe2'}}
          onChange={this.onTabChange.bind(this)}
          value={this.state.slideIndex}>
          {this.tabs.map((tab, index)=> {
            let isActive = this.isActiveSlide.bind(this, index)();
            return (
              <Tab key={index}
                   className="account-tab"
                   style={{color: isActive ? '#29abe2' : 'grey', padding: '5px 0', fontWeight: isActive ? 'bold' : 'normal'}}
                   label={tab.title} value={index}/>
            );
          })}
        </Tabs>
        <SwipeableViews
          slideStyle={{overflow: 'hidden'}} containerStyle={{height: 'auto !important'}}
          index={this.state.slideIndex}
          onChangeIndex={this.onTabChange.bind(this)}>
          <Col xs={12}>
            <ProjectsTabPage slideIndex={this.state.slideIndex} projects={projects.active}
                             currentUser={currentUser} total={total}
                             newest={activeStatistics.newest} oldest={activeStatistics.oldest} statuses={statuses}
                             projectsActions={projectsActions} status="active"/>
          </Col>
          <Col xs={12}>
            <ProjectsTabPage slideIndex={this.state.slideIndex} projects={projects.completed}
                             currentUser={currentUser} total={total}
                             newest={completedStatistics.newest} oldest={completedStatistics.oldest} statuses={statuses}
                             projectsActions={projectsActions} status="completed"/>
          </Col>
          <Col xs={12}>
            <ProjectsTabPage slideIndex={this.state.slideIndex} projects={projects.deleted}
                             currentUser={currentUser} total={total}
                             newest={deletedStatistics.newest} oldest={deletedStatistics.oldest} statuses={statuses}
                             projectsActions={projectsActions} status="deleted"/>
          </Col>
        </SwipeableViews>
        {spinnerElement}
      </Row>
    );
  }

  isActiveSlide(index) {
    return this.state.slideIndex === index;
  }
}

ProjectsPage.propTypes = {
  requestsInProcess: PropTypes.number.isRequired,
  projectsState: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
  projectsActions: PropTypes.object.isRequired
};

function select(state) {
  return {
    requestsInProcess: state.info.requestsInProcess,
    projectsState: state.projects,
    currentUser: state.users.currentUser
  };
}

function mapDispatchToProps(dispatch) {
  return {
    projectsActions: bindActionCreators(projects, dispatch)
  };
}

export default connect(select, mapDispatchToProps)(ProjectsPage);
