import React, {PropTypes, Component} from 'react';
import {Row, Grid} from 'react-bootstrap';
import {connect} from 'react-redux';
import Snackbar from 'material-ui/Snackbar';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import NavigationPublic from '../components/NavigationPublic';
import '../assets/styles/main';

const osiosTheme = {
  userAgent: 'all',
  fontFamily: 'Myriad Pro, sans-serif',
  palette: {
    primary1Color: '#29abe2',
    textColor: '#414042'
  }
};

class AppPublic extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {children, message, dispatch} = this.props;
    const muiTheme = getMuiTheme(Object.assign({}, lightBaseTheme, osiosTheme));

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <Grid fluid>
          <Row>
            <NavigationPublic/>
          </Row>
          <Row style={{paddingTop:20}}>
            <Grid>
              {children}
            </Grid>
            <Snackbar
              open={message!==''}
              message={message}
              autoHideDuration={4000}
              onRequestClose={()=>dispatch({type:'RESET_MESSAGE'})}
            />
          </Row>
        </Grid>
      </MuiThemeProvider>
    );
  }
}

AppPublic.propTypes = {
  children: PropTypes.object,
  message: PropTypes.string.isRequired
};

function select(state) {
  return {
    message: state.info.message
  };
}

export default connect(select)(AppPublic);
