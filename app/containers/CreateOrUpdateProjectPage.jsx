import React, {PropTypes, Component} from 'react';
import {Row, Col, Image} from 'react-bootstrap';
import {connect} from 'react-redux';
import CreateProjectStep1 from '../components/CreateProjectStep1';
import CreateProjectStep2 from '../components/CreateProjectStep2';
import CreateProjectStep3 from '../components/CreateProjectStep3';
import folderIcon from '../assets/img/folder-icon.png';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import LinearProgress from 'material-ui/LinearProgress';
import {bindActionCreators} from 'redux';
import * as projects from '../actions/projects';
import * as platforms from '../actions/platforms';
import * as devices from '../actions/devices';
import * as users from '../actions/users';
import * as companies from '../actions/companies';
import {browserHistory} from 'react-router';
import Spinner from '../components/Spinner';
import last from 'lodash/last';
import head from 'lodash/head';


class CreateOrUpdateProjectPage extends Component {
  constructor(props) {
    super(props);
    this.defaultBriefOption = {
      name: 'Complete now',
      icon: 'target',
      key: 'now'
    };
    this.state = {
      progress: 0,
      isEditing: false,
      options: [this.defaultBriefOption],
      completed: 0,
      stepIndex: 0
    };
  }

  componentWillMount() {
    const {devicesActions, platformsActions, projectsActions, usersActions, companiesActions, params:{id}} = this.props;

    projectsActions.currentProjectFieldChange({users: []});
    projectsActions.currentProjectFieldChange({
      scopes: [
        {
          name: '',
          description: '',
          devices: [],
          colors: [],
          platforms: [],
          files: []
        }
      ]
    });
    platformsActions.fetch();
    devicesActions.fetch();
    usersActions.fetch();
    companiesActions.fetch();
    if (id) {
      projectsActions.fetchById(id);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {currentProject, projectSuccess, requestsInProcess, currentUser, projectsActions, params:{id}} = nextProps;
    if (this.state.progress >= 100 && requestsInProcess === 0) {
      if (!projectSuccess) {
        this.setState({stepIndex: head(this.state.options).key === 'now' ? 1 : 0});
      } else {
        setTimeout(()=>browserHistory.push('/projects'), 1000);
      }
    }
    let users = [...currentProject.users];
    if (!users.find(user=>user.role.name === 'lead developer')) {
      users.push(
        {
          user: currentUser,
          role: {name: 'lead developer'}
        }
      );
      projectsActions.currentProjectFieldChange({users: users});
    }
    if (id && nextProps.requestsInProcess === 0 && !this.state.isEditing && currentProject._id) {
      let scope = Object.assign({}, last(currentProject.scopes) || {
          name: '',
          description: '',
          devices: [],
          colors: [],
          platforms: [],
          files: []
        }, {isNew: true});
      this.state.isEditing = true;
      projectsActions.currentProjectFieldChange({scopes: [...currentProject.scopes, scope]});
    }
    if (!id && !this.state.isEditing) {
      let scope = Object.assign({}, last(currentProject.scopes) || {
          name: '',
          description: '',
          devices: [],
          colors: [],
          platforms: [],
          files: []
        }, {isNew: true});
      this.state.isEditing = true;
      projectsActions.currentProjectFieldChange({scopes: [scope]});
    }
  }

  componentWillUpdate(nextProps, nextState) {
    const {currentProject, params:{id}} = nextProps;
    this.updateCompletedField(currentProject, !id);
  }

  onHandlePrev() {
    if (this.state.stepIndex === 0) {
      return;
    }
    this.setState({stepIndex: this.state.stepIndex - 1});
  }

  onHandleNext() {
    if (this.state.stepIndex > 1) {
      return;
    }
    this.setState({stepIndex: this.state.stepIndex + 1});
  }

  onSaveScope() {
    const {currentProject, projectsActions, params:{id}} = this.props;
    const options = {
      progress: progressEvent=> {
        let progressValue = progressEvent.loaded / progressEvent.total * 100;
        this.setState({progress: progressValue});
      }
    };
    this.setState({stepIndex: 2});
    if (!id) {
      projectsActions.create(Object.assign(currentProject, {option: head(this.state.options)}), options);
    } else {
      projectsActions.updateById(id, currentProject, options);
    }
  }

  onFieldChange(fieldName, value) {
    if (fieldName === 'options' && value.length === 0) {
      value.push(this.defaultBriefOption);
    }
    this.setState({[fieldName]: value});
  }

  getStepContent(index) {
    const {platforms, devices, allUsers, allCompanies, companiesActions, usersActions, userSuccess, projectsActions, currentProject, params:{id}} = this.props;
    switch (index) {
      case 0:
      {
        return (
          <CreateProjectStep1 projectsActions={projectsActions}
                              isNew={!id}
                              currentProject={currentProject}
                              onSaveScope={this.onSaveScope.bind(this)}
                              onFieldChange={this.onFieldChange.bind(this)}
                              userSuccess={userSuccess}
                              platforms={platforms}
                              devices={devices}
                              allUsers={allUsers}
                              allCompanies={allCompanies}
                              options={this.state.options}
                              companiesActions={companiesActions}
                              usersActions={usersActions}
          />);
      }
      case 1:
      {
        return (
          <CreateProjectStep2 projectsActions={projectsActions}
                              currentProject={currentProject}
                              onSaveScope={this.onSaveScope.bind(this)}
          />);
      }
      case 2:
      {
        return (
          <CreateProjectStep3 progress={this.state.progress}/>
        );
      }
      default:
      {
        return '';
      }
    }
  }

  render() {
    const {requestsInProcess, currentProject, params:{id}} = this.props;
    let spinnerElement;
    if (requestsInProcess > 0) {
      spinnerElement = (
        <Spinner/>
      );
    }
    return (
      <Row style={{marginTop: 20, marginBottom: 20}}>
        <Col xs={10} xsOffset={1} sm={12} smOffset={0}>
          <Row style={{marginBottom: 40}}>
            <div className="text-center"><Image src={folderIcon}/></div>
            <div className="text-center" style={{color: '#A0A1A3'}}>
              This project scope will be visible to everyone. It can be edited any time.
            </div>
          </Row>
          <Row>
            <LinearProgress className="create-project-progress-bar" mode="determinate" value={this.state.completed}
                            style={{marginBottom: 40}}/>
          </Row>
        </Col>
        <div>
          {this.getStepContent.bind(this)(this.state.stepIndex)}
          {this.state.stepIndex <= 1 ? (
            <div className="text-center">
              <FlatButton
                label="Cancel" tabIndex={-1}
                onClick={()=>browserHistory.goBack()}
                style={{marginRight: 20}}
              />
              <FlatButton
                label="Back" tabIndex={-1}
                disabled={this.state.stepIndex === 0}
                onClick={this.onHandlePrev.bind(this)}
                style={{marginRight: 20}}
              />
              <RaisedButton
                label={this.state.stepIndex >= 1 || head(this.state.options).key !== 'now' ? 'Save scope' : 'Next'}
                primary={true}
                disabled={!this.isAllRequestedFieldsCompleted(currentProject, !id)}
                onClick={this.state.stepIndex >= 1 || head(this.state.options).key !== 'now' ? this.onSaveScope.bind(this) : this.onHandleNext.bind(this)}
              />
            </div>) : ''}
        </div>
        {spinnerElement}
      </Row>
    );
  }

  isAllRequestedFieldsCompleted(currentProject, isNew) {
    let scope = last(currentProject.scopes);
    let projectName = scope.name.trim();
    let defaultRequiredFields = projectName && projectName.length >= 3 &&
      currentProject.users.find(user=>user.role.name === 'lead developer') &&
      currentProject.users.find(user=>user.role.name === 'lead client');
    switch (true) {
      case isNew && head(this.state.options).key === 'now' && this.state.stepIndex === 0:
      {
        return defaultRequiredFields &&
          scope.platforms.length > 0 && scope.devices.length > 0;
      }
      case isNew && head(this.state.options).key === 'now' && this.state.stepIndex === 1:
      {
        return defaultRequiredFields &&
          scope.platforms.length > 0 && scope.devices.length > 0 &&
          scope.description && scope.description.trim().length >= 3;
      }
      case isNew && head(this.state.options).key !== 'now':
      {
        return defaultRequiredFields;
      }
      case !isNew && this.state.stepIndex === 0:
      {
        return defaultRequiredFields &&
          scope.platforms.length > 0 && scope.devices.length > 0;
      }
      case !isNew && this.state.stepIndex === 1:
      {
        return defaultRequiredFields &&
          scope.platforms.length > 0 && scope.devices.length > 0 &&
          scope.description && scope.description.trim().length >= 3;
      }
      default:
      {
        return false;
      }
    }
  }

  updateCompletedField(currentProject, isNew) {
    this.state.completed = 0;
    let scope = last(currentProject.scopes);
    let step1 = 50 / (isNew ? 6 : 5);
    if (this.state.options.length > 0 && isNew) {
      this.state.completed += step1;
    }
    if (scope.devices.length > 0) {
      this.state.completed += step1;
    }
    if (scope.platforms.length > 0) {
      this.state.completed += step1;
    }
    let projectName = scope.name.trim();
    if (projectName && projectName.length >= 3) {
      this.state.completed += step1;
    }
    if (currentProject.users.find(user=>user.role.name === 'lead developer')) {
      this.state.completed += step1;
    }
    if (currentProject.users.find(user=>user.role.name === 'lead client')) {
      this.state.completed += step1;
    }
    let projectDescription = scope.description.trim();
    if (projectDescription && projectDescription.length >= 3) {
      this.state.completed += 50;
    }
  }
}

CreateOrUpdateProjectPage.propTypes = {
  userSuccess: PropTypes.bool.isRequired,
  projectSuccess: PropTypes.bool.isRequired,
  allCompanies: PropTypes.array.isRequired,
  currentUser: PropTypes.object.isRequired,
  currentProject: PropTypes.object.isRequired,
  projectsActions: PropTypes.object.isRequired,
  platformsActions: PropTypes.object.isRequired,
  devicesActions: PropTypes.object.isRequired,
  usersActions: PropTypes.object.isRequired,
  companiesActions: PropTypes.object.isRequired,
  allUsers: PropTypes.array.isRequired,
  devices: PropTypes.array.isRequired,
  platforms: PropTypes.array.isRequired,
  requestsInProcess: PropTypes.number.isRequired,
  params: PropTypes.string
};

function select(state) {
  return {
    userSuccess: state.users.success,
    projectSuccess: state.projects.success,
    allCompanies: state.companies.companies,
    currentUser: state.users.currentUser,
    currentProject: state.projects.currentProject,
    allUsers: state.users.allUsers,
    platforms: state.platforms.platforms,
    devices: state.devices.devices,
    requestsInProcess: state.info.requestsInProcess
  };
}

function mapDispatchToProps(dispatch) {
  return {
    projectsActions: bindActionCreators(projects, dispatch),
    platformsActions: bindActionCreators(platforms, dispatch),
    devicesActions: bindActionCreators(devices, dispatch),
    usersActions: bindActionCreators(users, dispatch),
    companiesActions: bindActionCreators(companies, dispatch)
  };
}

export default connect(select, mapDispatchToProps)(CreateOrUpdateProjectPage);
