import React, { PropTypes, Component } from 'react';
import { Row, Col, Image } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import debounce from 'lodash/debounce';
import * as users from '../actions/users';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';
import Debounce from '../components/Debounce';
import girl from '../assets/img/girl.png';
import spinner from '../assets/img/ajax-loader.gif';

class SignUpPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isSuperUser: false
    };
    this.submitDebounceWrapper = debounce(this.onSubmit.bind(this), 200);
  }

  componentWillUpdate(nextProps, nextState) {
  }

  onFieldChange(value, fieldName) {
    this.setState({[fieldName]: value});
  }

  onSubmit() {
    const {usersActions} = this.props;
    usersActions.create({
      email: this.state.email,
      password: this.state.password,
      isSuperUser: this.state.isSuperUser,
      card: this.state.card,
      month: this.state.month,
      year: this.state.year,
      cvc: this.state.cvc,
      tax: this.state.tax,
      plan: 'TestFreeTrial'
    });
  }

  render() {
    const {requestsInProcess} = this.props;
    let spinnerElement;
    if (requestsInProcess > 0) {
      spinnerElement = (<Image src={spinner} responsive style={{marginLeft: 'auto', marginRight: 'auto'}}/>);
    }
    return (
      <Row style={{marginBottom: 20}}>
        <Col xs={4} className="text-right">
          <Image src={girl} responsive style={{marginLeft: 'auto', marginTop: -40}}/>
        </Col>
        <Col xs={8}>
          <div style={{padding: 20, border: '1px solid #d1d2d3'}}>
            <h1 className="login-header-text">
              Effortless Collaboration</h1>
            <h1 className="login-small-header-text">
              <div>Don't get left on the outside.</div>
              <div>Sign up below...</div>
            </h1>
            <Row style={{marginTop: 20}}>
              <Col xs={12} sm={6} smOffset={3}>
                <form onSubmit={event => { event.preventDefault(); this.submitDebounceWrapper(); }}
                      style={{marginBottom: 20}}>
                  <Debounce time={200} handler="onChange">
                    <TextField
                      fullWidth
                      required
                      type="email"
                      hintText="Email"
                      floatingLabelText="Enter Your Email"
                      onChange={value=>this.onFieldChange(value, 'email')}
                      value={this.state.email}
                    />
                  </Debounce>
                  <Debounce time={200} handler="onChange">
                    <TextField
                      fullWidth
                      required
                      minLength="6"
                      hintText="Password"
                      floatingLabelText="Enter Your Password"
                      onChange={value=>this.onFieldChange(value, 'password')}
                      type="password"
                      value={this.state.password}
                    />
                  </Debounce>
                  <Toggle
                    label="I'm a superuser!"
                    labelPosition="right"
                    onToggle={event=>this.onFieldChange(event.target.checked, 'isSuperUser')}
                    style={{marginTop: 20}}
                  />
                  <Debounce time={200} handler="onChange">
                    <TextField
                      fullWidth
                      required={!this.state.isSuperUser}
                      hintText="Card Number"
                      floatingLabelText="Enter Your Card Number"
                      onChange={value=>this.onFieldChange(value, 'card')}
                      value={this.state.card}
                    />
                  </Debounce>
                  <Row>
                    <Col xs={4}>
                      <Debounce time={200} handler="onChange">
                        <TextField
                          fullWidth
                          required={!this.state.isSuperUser}
                          type="number"
                          min={1}
                          max={12}
                          hintText="mm"
                          floatingLabelText="Month"
                          onChange={value=>this.onFieldChange(value, 'month')}
                          value={this.state.month}
                        />
                      </Debounce>
                    </Col>
                    <Col xs={4}>
                      <Debounce time={200} handler="onChange">
                        <TextField
                          fullWidth
                          required={!this.state.isSuperUser}
                          type="number"
                          min={moment().subtract(10, 'years').format('YY')}
                          max={moment().add(10, 'years').format('YY')}
                          hintText="YY"
                          floatingLabelText="Year"
                          onChange={value=>this.onFieldChange(value, 'year')}
                          value={this.state.year}
                        />
                      </Debounce>
                    </Col>
                    <Col xs={4}>
                      <Debounce time={200} handler="onChange">
                        <TextField
                          fullWidth
                          required={!this.state.isSuperUser}
                          type="number"
                          min={100}
                          max={9999}
                          hintText="cvc"
                          floatingLabelText="CVC"
                          onChange={value=>this.onFieldChange(value, 'cvc')}
                          value={this.state.cvc}
                        />
                      </Debounce>
                    </Col>
                  </Row>
                  <Checkbox
                    label="Based in the EU?"
                    onCheck={event=>this.onFieldChange(event.target.checked, 'tax')}
                    style={{marginTop: 20}}
                  />
                  <RaisedButton
                    type="submit"
                    label="Signup"
                    primary={true}
                    fullWidth
                    style={{width: '100%', marginTop: 20}}
                    labelStyle={{fontWeight: 900}}
                  />
                </form>
                {spinnerElement}
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    );
  }
}

SignUpPage.propTypes = {
  usersActions: PropTypes.object.isRequired,
  requestsInProcess: PropTypes.number.isRequired,
  users: PropTypes.object.isRequired
};

function select(state) {
  return {
    users: state.users,
    requestsInProcess: state.info.requestsInProcess
  };
}

function mapDispatchToProps(dispatch) {
  return {
    usersActions: bindActionCreators(users, dispatch)
  };
}

export default connect(select, mapDispatchToProps)(SignUpPage);
