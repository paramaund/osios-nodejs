import React from 'react';

/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */
const Dashboard = props => <div>
  <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, iste vel. A accusantium consequuntur error
    explicabo fugit minus nam omnis saepe sequi, veniam! Dicta doloribus eum facilis ipsa odit provident.
  </div>
  <div>Accusantium architecto aspernatur atque, culpa dolores enim eveniet id ipsa labore laborum libero modi natus
    numquam odit pariatur quasi quibusdam quos ratione repellat reprehenderit similique tempora ullam. Accusamus
    doloremque, enim?
  </div>
  <div>Consectetur consequuntur, dolore doloribus enim ipsa itaque iusto molestias nihil praesentium quia quibusdam quos
    ratione rerum sit temporibus velit veritatis vero. Aliquid architecto, eveniet iusto modi non quidem quod
    voluptatem.
  </div>
  <div>Asperiores delectus dicta dignissimos dolorem dolores eius error ex hic ipsa itaque iure molestias mollitia nemo
    possimus quasi quis quo ratione reiciendis rem repellendus, repudiandae sint suscipit velit veritatis voluptatibus!
  </div>
  <div>Accusamus atque consequatur deserunt, ea eaque eius, id in iure, modi nemo possimus quasi reiciendis repellat
    repudiandae tempora ullam veniam voluptas? Debitis deleniti deserunt fugiat nulla praesentium quia repellendus
    suscipit.
  </div>
  <div>Ab accusantium asperiores atque autem blanditiis consequatur consequuntur eos est eveniet excepturi
    exercitationem fugiat, fugit in incidunt iure laboriosam modi molestias, mollitia odio officia quam quis soluta
    tempore temporibus veritatis.
  </div>
  <div>Accusantium alias cumque cupiditate dicta dignissimos eum excepturi fugiat inventore, ipsa ipsam ipsum iure
    maxime natus neque perferendis, praesentium provident quaerat quas quidem repellendus reprehenderit sit tempora unde
    vel voluptas.
  </div>
  <div>Alias cumque dolorum ea laudantium minus nesciunt sequi, sit voluptatem. Aspernatur atque cupiditate eius esse
    eum exercitationem, laboriosam magni mollitia non possimus quas, voluptatibus? Deleniti distinctio, est. Labore,
    molestiae recusandae?
  </div>
  <div>Consequuntur distinctio ex, libero minus molestiae nihil. Culpa dolor quas vero. Cum impedit ipsam laudantium
    velit. Aliquid aperiam asperiores atque consectetur dolorem earum, eius et magni minima nemo vitae voluptatum.
  </div>
  <div>Ea eaque est, exercitationem facilis harum id incidunt inventore ipsa ipsum soluta voluptatem voluptatibus. At
    corporis culpa explicabo, illo laboriosam nesciunt quidem veniam! Maxime molestiae reprehenderit similique sunt
    totam voluptatum?
  </div>
  <div>Ad atque eius in incidunt iusto, mollitia nobis, porro, quae quasi reiciendis totam vitae voluptates?
    Consequuntur debitis deleniti doloremque dolores ducimus hic, libero molestias, nisi nostrum officiis quae tempora
    voluptates.
  </div>
  <div>Accusamus ad aliquam aspernatur autem deleniti dolor, dolore eligendi et expedita illum ipsa ipsam iste libero
    nobis nulla odio officiis perspiciatis possimus praesentium quis quo sed ut vel veniam veritatis?
  </div>
  <div>Blanditiis eveniet fuga iste, iusto neque omnis perspiciatis praesentium reiciendis. Accusamus accusantium amet
    harum id illo, impedit iste magni, maxime modi officia perferendis quae quidem saepe. Eos illum minus voluptate.
  </div>
  <div>Ab culpa dicta dolorem earum impedit, incidunt ipsum odio rem tempora voluptatum? Accusamus aliquam asperiores
    aut blanditiis consequatur, culpa deleniti dolorem ea eligendi excepturi iste molestiae possimus quo soluta velit.
  </div>
  <div>Aliquam at non odio optio quis quos? Atque exercitationem explicabo facere fuga fugit in ipsa, modi, non pariatur
    quibusdam quo similique velit. A atque deserunt est, eveniet numquam perferendis quas.
  </div>
  <div>Assumenda blanditiis cupiditate dolorem dolores doloribus, earum enim exercitationem expedita facilis hic in,
    incidunt inventore itaque laboriosam magnam minima molestias nemo neque nihil odio quisquam quos repudiandae
    sapiente tempore, temporibus!
  </div>
  <div>Ab amet aut consequatur deleniti doloremque ducimus ea, enim laborum maiores numquam odio porro quibusdam
    repellat reprehenderit tenetur. Aliquid dicta dolorem itaque molestias nam placeat reiciendis repellendus temporibus
    veritatis! Ad.
  </div>
  <div>Beatae eligendi labore magni maiores placeat quae, quam recusandae rerum tempora ut! Distinctio dolore facere
    labore nisi? Consequuntur fugiat ipsa maiores natus numquam quasi sapiente tempore veniam? At, illo, non.
  </div>
  <div>A accusantium at cum eos error eum eveniet facere illum inventore, iste laudantium nostrum nulla numquam odio,
    odit officia quaerat quas qui similique suscipit. Possimus, quo, unde? Enim, nesciunt placeat?
  </div>
  <div>Aspernatur assumenda blanditiis deleniti provident, qui veritatis! Amet beatae cupiditate ducimus eaque incidunt
    ipsa ipsum itaque laborum maxime minima molestias nemo nulla officiis sed similique sit, sunt tempore velit
    voluptate.
  </div>
  <div>Culpa dignissimos dolores facilis fuga iste iure laboriosam laudantium maxime necessitatibus optio placeat quas
    quia, sint sit tenetur. Deleniti dignissimos est et molestias, numquam officiis perspiciatis provident qui quis
    voluptates?
  </div>
  <div>Accusantium assumenda atque consectetur cumque dolore eaque earum, enim est fuga fugiat incidunt ipsum labore
    magnam maxime molestias mollitia nesciunt nisi nostrum, odio optio praesentium quaerat quia ratione rem voluptates!
  </div>
  <div>Aliquid aperiam dicta hic laborum libero placeat sed! Ab animi dolor dolorum labore nesciunt quidem, sequi sint
    tempora! Esse magni perferendis quos voluptates? A aspernatur enim illum minus ut vero?
  </div>
  <div>A ab amet corporis earum, facere ipsum itaque iure labore, magni nam officia recusandae tenetur ut voluptate
    voluptatibus! Accusantium aut blanditiis dolor dolorem doloribus eum maxime provident unde vero voluptatem?
  </div>
  <div>Accusantium aliquid aut blanditiis consectetur culpa deleniti dolorum esse eum illum impedit inventore nam nihil
    perspiciatis praesentium provident quae, quibusdam vel! Cumque fugiat optio perferendis? Iste nulla pariatur
    similique suscipit.
  </div>
  <div>Impedit, laborum repellat. Cum dicta maxime sed voluptatum. Ab eius eligendi facilis incidunt minus molestias
    repellat! Alias, aliquid eaque, earum enim, et ipsum magni nihil officia praesentium quibusdam tempore voluptates?
  </div>
  <div>Amet distinctio, dolore est hic iusto natus numquam, provident, recusandae rem reprehenderit rerum sint
    temporibus vel! Assumenda beatae cum maiores, nesciunt nihil nobis quod sit veniam voluptatem voluptatum! Deserunt,
    suscipit.
  </div>
  <div>Eveniet illo nihil unde. Ab at corporis dolorum facilis id illo ipsam iste labore maiores minus mollitia nihil
    optio perferendis, praesentium quis quos tempora vitae voluptatum. Fugit modi odit quas?
  </div>
  <div>Accusantium aliquam aliquid aspernatur at, dolorem dolorum ducimus facilis labore natus officiis qui sapiente
    sint tenetur. Accusamus, consequatur doloremque est laborum magni, molestias necessitatibus quis quos ratione rem
    tempore tenetur!
  </div>
  <div>Cumque debitis expedita hic natus obcaecati officia quia ratione veritatis? Atque deleniti deserunt distinctio
    ea, et excepturi exercitationem illum iste natus nesciunt nulla officia officiis omnis pariatur, sequi similique
    vero.
  </div>
  <div>Autem corporis delectus deserunt dicta dolore, eius et expedita fugiat incidunt iusto libero nostrum omnis qui
    quidem rerum saepe sequi, vel! At dolorem doloremque, et neque odit quia tempore voluptatibus.
  </div>
  <div>Ad, adipisci amet dolore exercitationem explicabo laborum minus molestias nobis quis ratione. Doloremque
    doloribus, maiores modi reiciendis similique soluta veniam. Ad delectus fugit modi molestias porro quia reiciendis
    reprehenderit, voluptates!
  </div>
  <div>Fugit harum incidunt, laudantium, modi non perspiciatis possimus repellendus reprehenderit sed tempora temporibus
    veritatis voluptatum. Adipisci blanditiis dolorum eligendi, fugit illum, ipsam maxime officia optio pariatur
    perspiciatis ut veniam, voluptatum?
  </div>
  <div>Assumenda dolorem hic illo incidunt minima odio possimus repellat unde vel. Aspernatur, dignissimos eligendi fuga
    libero minus non velit voluptas. Amet consequatur corporis, dolor dolorum expedita explicabo nostrum quas ratione!
  </div>
  <div>Architecto doloribus expedita optio voluptatibus? Beatae corporis dolorem, doloribus eaque earum et fuga, maiores
    ratione repellendus sed, soluta temporibus velit. Debitis earum, eius expedita facere illo quia quo recusandae
    sequi.
  </div>
  <div>At, consectetur distinctio ex, itaque nemo odio pariatur perferendis quia quidem rem rerum, voluptatibus. A alias
    animi cum, error et illo neque nisi ratione repellendus saepe, sit veritatis voluptatem voluptatum?
  </div>
  <div>Alias aliquam aliquid aperiam consequatur delectus dolor dolore eaque, eius et explicabo harum itaque iure
    mollitia quibusdam quod, repellat repellendus sapiente sed, similique ullam ut veritatis vero voluptates
    voluptatibus voluptatum!
  </div>
  <div>Aliquid animi, beatae deleniti dignissimos enim error et facilis ipsa nemo, nisi odit praesentium recusandae
    voluptatibus? Culpa delectus est explicabo minima, molestiae non numquam porro possimus ratione repellendus sit
    ullam!
  </div>
  <div>Accusantium ad architecto deserunt eos error et facere hic iure laudantium maiores odit possimus quis
    reprehenderit, rerum sequi ullam veniam vero. Amet assumenda consequatur itaque minima molestias nobis perspiciatis
    suscipit.
  </div>
  <div>Atque dicta earum eos fuga iure iusto natus numquam obcaecati officia quas quis reprehenderit rerum sit suscipit
    tempore veritatis, vitae! Consequuntur eveniet fugiat ipsa ipsum itaque neque temporibus ullam veniam!
  </div>Welcome to the Dashboard. Stay tuned...</div>;

export default Dashboard;
