import React, {PropTypes, Component} from 'react';
import {Row, Col, Image} from 'react-bootstrap';
import {connect} from 'react-redux';
import profileImageOff from '../assets/img/account_tabs/account-me-off.png';
import connectImageOff from '../assets/img/account_tabs/account-connect-off.png';
import financeImageOff from '../assets/img/account_tabs/account-finance-off.png';
import projectsImageOff from '../assets/img/account_tabs/account-projects-off.png';
import notificationsImageOff from '../assets/img/account_tabs/account-notifications-off.png';
import capacityImageOff from '../assets/img/account_tabs/account-capacity-off.png';
import profileImageOn from '../assets/img/account_tabs/account-me-on.png';
import connectImageOn from '../assets/img/account_tabs/account-connect-on.png';
import financeImageOn from '../assets/img/account_tabs/account-finance-on.png';
import projectsImageOn from '../assets/img/account_tabs/account-projects-on.png';
import notificationsImageOn from '../assets/img/account_tabs/account-notifications-on.png';
import capacityImageOn from '../assets/img/account_tabs/account-capacity-on.png';
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import AccountMe from '../components/AccountMe';
import AccountConnect from '../components/AccountConnect';
import AccountFinance from '../components/AccountFinance';
import AccountProjects from '../components/AccountProjects';
import AccountNotifications from '../components/AccountNotifications';
import AccountCapacity from '../components/AccountCapacity';
import Spinner from '../components/Spinner';
import {bindActionCreators} from 'redux';
import * as users from '../actions/users';
import * as messages from '../actions/messages';
import * as subscribe from '../actions/subscribe';
import * as paymentPlans from '../actions/paymentPlans';
import * as auth from '../actions/auth';

class AccountPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      slideIndex: 0,
      selectedPlanKey: ''
    };
    this.tabs = [
      {title: 'Me', on: profileImageOn, off: profileImageOff},
      {title: 'Finance', on: financeImageOn, off: financeImageOff},
      {title: 'Projects', on: projectsImageOn, off: projectsImageOff},
      {title: 'Notifications', on: notificationsImageOn, off: notificationsImageOff},
      {title: 'Capacity', on: capacityImageOn, off: capacityImageOff},
      {title: 'Connect', on: connectImageOn, off: connectImageOff}];
  }

  componentWillMount() {
    const {usersActions} = this.props;
    usersActions.getStatisticsMe();
  }

  componentWillUpdate(nextProps, nextState) {
  }

  onTabChange(value) {
    this.setState({
      slideIndex: value
    });
  }

  onPlanSelect(tabNumber, selectedPlan) {
    this.state.selectedPlanKey = selectedPlan;
    this.onTabChange(tabNumber);
  }

  render() {
    const {
      usersState: {currentUser, statistics},
      paymentPlans:{plans},
      requestsInProcess,
      usersActions,
      messagesActions,
      paymentPlansActions,
      authActions,
      subscribeActions
    } = this.props;
    let spinnerElement;
    if (requestsInProcess > 0) {
      spinnerElement = (
        <Spinner/>
      );
    }
    return (
      <Row>
        <Tabs
          tabItemContainerStyle={{backgroundColor: 'transparent'}}
          inkBarStyle={{backgroundColor: '#29abe2'}}
          onChange={this.onTabChange.bind(this)}
          value={this.state.slideIndex}>
          {this.tabs.map((tab, index)=> {
            let isActive = this.isActiveSlide.bind(this, index)();
            return (
              <Tab key={index}
                   icon={<Col xs={12} sm={8} xsHidden><Image src={isActive ? tab.on : tab.off} className="center-block" responsive/></Col>}
                   className="account-tab"
                   style={{fontSize: '0.9rem', color: isActive ? '#29abe2' : 'grey', fontWeight: isActive ? 'bold' : 'normal'}}
                   label={tab.title} value={index}/>
            );
          })}
        </Tabs>
        <SwipeableViews
          slideStyle={{overflow: 'hidden'}} containerStyle={{height: 'auto !important'}}
          index={this.state.slideIndex}
          onChangeIndex={this.onTabChange.bind(this)}>
          <Col xs={12}>
            <AccountMe slideIndex={this.state.slideIndex} currentUser={currentUser} usersActions={usersActions}/>
          </Col>
          <Col xs={12}>
            <AccountFinance slideIndex={this.state.slideIndex} currentUser={currentUser}
                            selectedPlan={this.state.selectedPlanKey}
                            onPlanSelect={this.onPlanSelect.bind(this)}
                            plans={plans}
                            paymentPlansActions={paymentPlansActions}
                            subscribeActions={subscribeActions}
                            authActions={authActions}
                            usersActions={usersActions}/>

          </Col>
          <Col xs={12}>
            <AccountProjects slideIndex={this.state.slideIndex} currentUser={currentUser}/>
          </Col>
          <Col xs={12}>
            <AccountNotifications slideIndex={this.state.slideIndex} currentUser={currentUser}
                                  usersActions={usersActions}/>
          </Col>
          <Col xs={12}>
            <AccountCapacity slideIndex={this.state.slideIndex} onPlanSelect={this.onPlanSelect.bind(this)}
                             plans={plans}
                             statistics={statistics || {}}
                             currentUser={currentUser}/>
          </Col>
          <Col xs={12}>
            <AccountConnect slideIndex={this.state.slideIndex} currentUser={currentUser}
                            messagesActions={messagesActions}/>
          </Col>
        </SwipeableViews>
        {spinnerElement}
      </Row>
    );
  }

  isActiveSlide(index) {
    return this.state.slideIndex === index;
  }

}

AccountPage.propTypes = {
  paymentPlans: PropTypes.object.isRequired,
  requestsInProcess: PropTypes.number.isRequired,
  usersState: PropTypes.object.isRequired,
  usersActions: PropTypes.object.isRequired,
  messagesActions: PropTypes.object.isRequired,
  paymentPlansActions: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired,
  subscribeActions: PropTypes.object.isRequired
};

function select(state) {
  return {
    requestsInProcess: state.info.requestsInProcess,
    usersState: state.users,
    paymentPlans: state.paymentPlans
  };
}

function mapDispatchToProps(dispatch) {
  return {
    usersActions: bindActionCreators(users, dispatch),
    messagesActions: bindActionCreators(messages, dispatch),
    paymentPlansActions: bindActionCreators(paymentPlans, dispatch),
    subscribeActions: bindActionCreators(subscribe, dispatch),
    authActions: bindActionCreators(auth, dispatch)
  };
}

export default connect(select, mapDispatchToProps)(AccountPage);
