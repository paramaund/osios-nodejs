import React, {PropTypes, Component} from 'react';
import {Row, Col, Image, Tooltip, OverlayTrigger} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import EditorEdit from 'material-ui/svg-icons/editor/mode-edit';
import last from 'lodash/last';
import head from 'lodash/head';
import round from 'lodash/round';
import isString from 'lodash/isString';
import isObject from 'lodash/isObject';
import isArray from 'lodash/isArray';
import forIn from 'lodash/forIn';
import isUndefined from 'lodash/isUndefined';
import defaultAvatar from '../assets/img/default-user.png';
import defaultImagePreview from '../assets/img/NoPreviewGraphic.png';

class ScopePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedScope: {
        files: [],
        colors: [],
        platforms: [],
        devices: []
      },
      prevScope: {}
    };
  }

  componentWillMount() {
    if (!this.state.selectedScope._id) {
      this.state.selectedScope = last(this.props.project.scopes);
    }
    let currentScopeIndex = this.props.project.scopes.findIndex(scope=>scope._id === this.state.selectedScope._id);
    if (currentScopeIndex > 0) {
      this.state.prevScope = this.props.project.scopes[currentScopeIndex - 1];
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.selectedScope._id) {
      this.state.selectedScope = last(nextProps.project.scopes);
    }
  }

  componentWillUpdate(nextProps, nextState) {
    let currentScopeIndex = nextProps.project.scopes.findIndex(scope=>scope._id === nextState.selectedScope._id);
    if (currentScopeIndex > 0) {
      nextState.prevScope = nextProps.project.scopes[currentScopeIndex - 1];
    }
  }

  onSelectedScopeChange(event, index, value) {
    this.setState({selectedScope: value, prevScope: index !== 0 ? this.props.project.scopes[index] : {}});
  }

  getNumberEnd(number) {
    switch (number) {
      case 1:
      {
        return 'st';
      }
      case 22:
      case 32:
      case 2:
      {
        return 'nd';
      }
      case 23:
      case 33:
      case 3:
      {
        return 'rd';
      }
      default:
      {
        return 'st';
      }
    }
  }

  render() {
    const {project, currentUser} = this.props;
    let userName = `${currentUser.first_name || ''}${currentUser.first_name ? ' ' : ''}${currentUser.last_name || ''}`;
    if (userName.trim() === '') {
      userName = currentUser.email;
    }
    let lastScope = last(project.scopes);
    let firstScope = head(project.scopes);
    let imageExtentions = ['jpg', 'jpeg', 'png', 'svg', 'gif', 'ico', 'bmp', 'pjpeg'];
    let changes = this.getScopeChanges();
    return (
      <div style={{padding: 20, border: '1px solid #d1d2d3'}}>
        <Row>
          <Col xs={12}>
            Brief Version:
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={6}>
            <SelectField autoWidth value={this.state.selectedScope} onChange={this.onSelectedScopeChange.bind(this)}>
              {project.scopes.map((scope, index)=> {
                let scopeName = firstScope && firstScope._id === scope._id ? 'Original' : (lastScope && lastScope._id === scope._id ? 'Current' : scope.created_at);
                return (<MenuItem key={index} value={scope} primaryText={scopeName}/>);
              })}
            </SelectField>
          </Col>
          <Col xs={12} sm={6} className="text-right">
            <Image
              src={currentUser.avatar || defaultAvatar}
              style={{width: 45, height: 45, marginRight: 10}}
              responsive
              circle
              thumbnail/>
            <span style={{fontSize: 20, fontWeight: 300}}>{userName}</span>
          </Col>
        </Row>
        <Row>
          <hr style={{marginTop: 5}}/>
        </Row>
        <Row>
          <Col xs={12} sm={10}>
            <h1 style={{margin: 0, fontWeight: 300}}>App Description</h1>
          </Col>
          <Col xs={12} sm={2} className="text-right">
            <LinkContainer to={{ pathname: `/projects/${project._id}/edit` }}>
              <OverlayTrigger placement="top" rootClose
                              overlay={<Tooltip id={project._id}>Edit project scope</Tooltip>}>
                <FloatingActionButton secondary={true}>
                  <EditorEdit/>
                </FloatingActionButton>
              </OverlayTrigger>
            </LinkContainer>
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>Select the app platform this is for: </b>
            {this.state.selectedScope.platforms.map((platform, index)=>(
              <span key={index} style={{backgroundColor: changes.platforms[index] ? 'yellow' : ''}}
              >{platform.name}, </span>
            ))}
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>Select the app device this is for: </b>
            {this.state.selectedScope.devices.map((device, index)=>(
              <span key={index} style={{backgroundColor: changes.devices[index] ? 'yellow' : ''}}
              >{device.name}, </span>
            ))}
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>App
              Description: </b>
          </Col>
          <Col xs={12} style={{backgroundColor: changes.description ? 'yellow' : ''}}>
            {this.state.selectedScope.description}
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>App user: </b>
          </Col>
          <Col xs={12} style={{backgroundColor: changes.intended_users ? 'yellow' : ''}}>
            {this.state.selectedScope.intended_users}
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>App Branding Colours: </b>
          </Col>
          <Col xs={12}>
            {this.state.selectedScope.colors.map((color, index)=>(
              <Row key={index} style={{marginTop: 10, backgroundColor: changes.colors[index] ? 'yellow' : ''}}>
                <Col xs={2} md={1}>
                  {(index + 1) + this.getNumberEnd(index + 1)}
                </Col>
                <Col xs={3} style={{backgroundColor: color}}>
                  &nbsp;
                </Col>
                <Col xs={7} md={8}>
                  {color}
                </Col>
              </Row>
            ))}
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>Fonts Desired: </b>
            <span style={{backgroundColor: changes.fonts ? 'yellow' : ''}}
            >{this.state.selectedScope.fonts}</span>
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>More Design Direction: </b>
          </Col>
          <Col xs={12} style={{backgroundColor: changes.design_direction ? 'yellow' : ''}}>
            {this.state.selectedScope.design_direction}
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>Up To 5 Design Reference Files (JPG, PNG, PDF & PSD allowed): </b>
          </Col>
          <Col xs={12}>
            {this.state.selectedScope.files.map((file, index)=> {
              let preview = file.url;
              let fileExt = preview.split('.').pop();

              if (imageExtentions.every(ext=>ext !== fileExt)) {
                preview = defaultImagePreview;
              }
              return (
                <Row key={index} style={{marginTop: 10, backgroundColor: changes.files[index] ? 'yellow' : ''}}>
                  <Col xs={3} sm={4} md={2}>
                    <a href={file.url} style={{display: 'block'}}>
                      <Image src={preview} alt={file.name} style={{width: '100%'}} responsive thumbnail/>
                    </a>
                  </Col>
                  <Col xs={9} sm={8} md={8}>
                    <div><b>Filename: </b>{file.name}</div>
                    <div><b>Size: </b>{round(file.size / 1024 / 1024, 2)} MB</div>
                  </Col>
                </Row>
              );
            })}
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>Technical requirements: </b>
          </Col>
          <Col xs={12} style={{backgroundColor: changes.tech_requirements ? 'yellow' : ''}}>
            {this.state.selectedScope.tech_requirements}
          </Col>
        </Row>
        <Row style={{fontSize: 16, marginTop: 10}}>
          <Col xs={12}>
            <b>Any questions or additional requirements should be listed below: </b>
          </Col>
          <Col xs={12} style={{backgroundColor: changes.notes ? 'yellow' : ''}}>
            {this.state.selectedScope.notes}
          </Col>
        </Row>
      </div>
    );
  }

  getScopeChanges() {
    if (!this.state.prevScope._id) {
      return {files: [], devices: [], platforms: [], colors: []};
    }
    let changes = {};
    forIn(this.state.selectedScope, (value, key)=> {
      if (isString(value)) {
        let isUndef = isUndefined(this.state.prevScope[key]);
        changes[key] = (isUndef && value !== '') || (!isUndef && value !== this.state.prevScope[key]);
      }
      if (isArray(value)) {
        changes[key] = [];
        value.forEach(elem=> {
          if (isString(elem)) {
            changes[key].push(this.state.prevScope[key].every(el=>el !== elem));
          }
          if (isObject(elem)) {
            changes[key].push(this.state.prevScope[key].every(el=>el._id !== elem._id));
          }
        });
      }
    });
    return changes;
  }
}

ScopePage.propTypes = {
  currentUser: PropTypes.object.isRequired,
  project: PropTypes.object.isRequired
};

export default ScopePage;
