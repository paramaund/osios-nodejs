import React, {PropTypes, Component} from 'react';
import Formsy from 'formsy-react';
import {FormsyText} from 'formsy-material-ui/lib';
import {Row, Col, Image} from 'react-bootstrap';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import forOwn from 'lodash/forOwn';
import * as auth from '../actions/auth';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import girl from '../assets/img/girl.png';
import spinner from '../assets/img/ajax-loader.gif';

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      canSubmit: false
    };
  }

  componentWillUpdate(nextProps, nextState) {
  }

  componentDidMount() {
    setTimeout(()=>forOwn(this.refs, elem=> {
      elem.setValue(elem.muiComponent.input.value);
    }), 500);
  }

  onFieldChange(value, fieldName) {
    this.setState({[fieldName]: value});
  }

  onEnableButton() {
    this.setState({
      canSubmit: true
    });
  }

  onDisableButton() {
    this.setState({
      canSubmit: false
    });
  }

  onSubmit(data) {
    const {authActions} = this.props;
    authActions.login(data);
  }

  onInvalidSubmit(values, reset, invalidate) {
    let errors = {};
    forOwn(values, (value, key)=> {
      if (!value) {
        errors[key] = `${key} field is required.`;
      }
    });
    invalidate(errors);
  }

  render() {
    const {requestsInProcess} = this.props;
    let spinnerElement;
    if (requestsInProcess > 0) {
      spinnerElement = (
        <Image src={spinner} responsive style={{marginLeft: 'auto', marginRight: 'auto'}}/>
      );
    }
    return (
      <Row style={{marginBottom: 20}}>
        <Col xs={4} className="text-right">
          <Image src={girl} responsive style={{marginLeft: 'auto', marginTop: -40}}/>
        </Col>
        <Col xs={8}>
          <div style={{padding: 20, border: '1px solid #d1d2d3'}}>
            <h1 className="login-header-text">
              Effortless Collaboration</h1>
            <h1 className="login-small-header-text">
              <div>Don't get left on the outside.</div>
              <div>Log in below...</div>
            </h1>
            <Row style={{marginTop: 20}}>
              <Col xs={12} sm={6} smOffset={3}>
                <Formsy.Form
                  noValidate
                  // onValid={this.onEnableButton.bind(this)}
                  // onInvalid={this.onDisableButton.bind(this)}
                  onValidSubmit={this.onSubmit.bind(this)}
                  onInvalidSubmit={this.onInvalidSubmit.bind(this)}
                  style={{marginBottom: 20}}
                >
                  <FormsyText
                    name="email"
                    validations={{isEmail: true}}
                    validationErrors={{isEmail: 'This is not a valid email.'}}
                    required
                    fullWidth
                    ref="email"
                    type="email"
                    hintText="Email"
                    floatingLabelText="Your Signup Email"
                  />
                  <FormsyText
                    name="password"
                    validations={{minLength: 6}}
                    validationErrors={{minLength: 'Password should be at least 6 characters.'}}
                    required
                    fullWidth
                    ref="password"
                    hintText="Password"
                    floatingLabelText="Your OSIOS Password"
                    type="password"
                  />
                  <RaisedButton
                    type="submit"
                    primary={true}
                    fullWidth
                    // disabled={!this.state.canSubmit}
                    style={{width: '100%', marginTop: 20, boxShadow: 'inherit'}}
                  >
                    <div
                      style={{display: 'flex', justifyContent: 'space-between', paddingLeft: 10, paddingRight: 10, color: 'white'}}>
                      <label htmlFor="" style={{ fontWeight: 900, paddingTop: 2}}
                             className="text-uppercase">Login</label>
                      <FontIcon color="white" style={{paddingTop: 4}}
                                className="glyphicon glyphicon-chevron-right loginBtnIcon"/>
                    </div>
                  </RaisedButton>
                </Formsy.Form>
                {spinnerElement}
              </Col>
            </Row>
            <Row className="text-center" style={{marginTop: 20}}>
              <span>Not signed up yet? <a href="/pricing">Signup here.</a></span>
            </Row>
          </div>
        </Col>
      </Row>
    );
  }
}

LoginPage.propTypes = {
  authActions: PropTypes.object.isRequired,
  requestsInProcess: PropTypes.number.isRequired
};

function select(state) {
  return {
    requestsInProcess: state.info.requestsInProcess
  };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(auth, dispatch)
  };
}

export default connect(select, mapDispatchToProps)(LoginPage);
