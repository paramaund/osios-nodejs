import React, {PropTypes, Component} from 'react';
import {Row, Col, Image} from 'react-bootstrap';
import {connect} from 'react-redux';
import ProjectSideBar from '../containers/ProjectSideBar';
import Spinner from '../components/Spinner';
import {bindActionCreators} from 'redux';
import * as projectsActions from '../actions/projects';

class ProjectPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const {projectsActions, params:{id}} = this.props;
    projectsActions.fetchById(id);
    // projectsActions.fetchByParams({status: 'active', limit: 100});
  }

  componentWillReceiveProps(nextProps) {
    const {projectsActions} = nextProps;
    if (this.props.params.id !== nextProps.params.id) {
      projectsActions.fetchById(nextProps.params.id);
    }
    if (!this.props.currentProject._id && nextProps.currentProject._id) {
      let status = nextProps.currentProject.status.name;
      var projects = nextProps.projects;
      if (['working', 'problem', 'not started'].some(elem=>elem === status)) {
        projects = projects.active;
      }
      if (projects[status].own.projects.length + projects[status].other.projects.length === 0) {
        projectsActions.fetchByParams({status: status, limit: 100, isOwner: true});
        projectsActions.fetchByParams({status: status, limit: 100, isOwner: false});
      }
    }
  }

  componentWillUpdate(nextProps, nextState) {
  }

  render() {
    const {children, requestsInProcess, currentProject, currentUser, projects, location} = this.props;
    let projectsByStatus = [];
    if (currentProject.status) {
      let status = currentProject.status.name;
      projectsByStatus = projects;
      if (['working', 'problem', 'not started'].some(elem=>elem === status)) {
        projectsByStatus = projectsByStatus.active;
      }
      projectsByStatus = [...projectsByStatus[status].own.projects, ...projectsByStatus[status].other.projects];
    }

    let spinnerElement;
    if (requestsInProcess > 0) {
      spinnerElement = (
        <Spinner/>
      );
    }
    return (
      <Row style={{marginBottom: 20, display: 'flex'}}>
        <Col xs={4} style={{display: 'flex'}}>
          <ProjectSideBar
            location={location}
            project={currentProject}
            projects={projectsByStatus}/>
        </Col>
        <Col xs={8}>
          {React.cloneElement(children, {project: currentProject, currentUser: currentUser})}
        </Col>
        {spinnerElement}
      </Row>
    );
  }
}

ProjectPage.propTypes = {
  children: PropTypes.object,
  location: PropTypes.object.isRequired,
  requestsInProcess: PropTypes.number.isRequired,
  params: PropTypes.object.isRequired,
  currentProject: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
  projects: PropTypes.array.isRequired,
  projectsActions: PropTypes.object.isRequired
};

function select(state) {
  return {
    currentProject: state.projects.currentProject,
    currentUser: state.users.currentUser,
    projects: state.projects.projects,
    location: state.location,
    requestsInProcess: state.info.requestsInProcess
  };
}

function mapDispatchToProps(dispatch) {
  return {
    projectsActions: bindActionCreators(projectsActions, dispatch)
  };
}

export default connect(select, mapDispatchToProps)(ProjectPage);
