import React, {PropTypes, Component} from 'react';
import {Row, Col, Image, Tooltip, OverlayTrigger} from 'react-bootstrap';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as roomsSimpleActions from '../actions/rooms';
import * as companiesSimpleActions from '../actions/companies';
import RoomItemView from '../components/RoomItemView';
import CreateOrUpdateRoomModal from '../components/CreateOrUpdateRoomModal';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {ISBROWSER} from '../constants/index';
import isNumber from 'lodash/isNumber';

const Sortable = ISBROWSER ? require('sortablejs') : undefined;

class RoomsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false
    };
  }

  componentWillMount() {
    const {roomsActions, project} = this.props;
    roomsActions.fetchByProject({project: project._id});
    roomsActions.fetchPermissions();
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.project._id && nextProps.project._id) {
      const {roomsActions, project} = nextProps;
      roomsActions.fetchByProject({project: project._id});
    }
    if (!this.props.success && nextProps.success) {
      this.state.openModal = false;
      const {roomsActions} = nextProps;
      roomsActions.confirmSuccess();
    }
  }

  componentWillUpdate(nextProps, nextState) {

  }

  onRoomCreateSubmit(data) {
    const {roomsActions} = this.props;
    roomsActions.create(data);
  }

  render() {
    const {rooms, currentRoom, roomsActions, companiesActions, project, permissions, companies} = this.props;
    let roomsElement;
    if (rooms.length > 0) {
      roomsElement = (
        <div ref={this.sortableContainersDecorator.bind(this, rooms)}>
          {rooms.map((room, index)=>
            <RoomItemView key={index} room={room}
                          projectId={project._id}
                          roomsActions={roomsActions}/>
          )}
        </div>
      );
    } else {
      roomsElement = (
        <div className="text-notification"><i>You have not added any rooms yet!</i></div>
      );
    }
    return (
      <div>
        {roomsElement}
        <div className="text-center" style={{margin: 20}}>
          <OverlayTrigger placement="top"
                          overlay={<Tooltip id="addNewRoom">Add New Room</Tooltip>}>
            <FloatingActionButton onClick={()=>this.setState({openModal: true})}>
              <ContentAdd />
            </FloatingActionButton>
          </OverlayTrigger>
        </div>
        <CreateOrUpdateRoomModal
          isOpen={this.state.openModal}
          onClose={()=>this.setState({openModal: false})}
          onSubmit={this.onRoomCreateSubmit.bind(this)}
          companiesActions={companiesActions}
          companies={companies}
          permissions={permissions}
          project={project}
          currentRoom={currentRoom}
          roomsActions={roomsActions}/>
      </div>
    );
  }

  sortableContainersDecorator(rooms, componentBackingInstance) {
    let roomsIdsOrder = rooms.map(room=>room._id);
    const {roomsActions} = this.props;
    // check if backing instance not null
    if (componentBackingInstance) {
      let options = {
        sort: true,
        handle: '.draggable-handler', // Restricts sort start click/touch to the specified element
        draggable: '.draggable',
        onEnd: event=> {
          console.log(event.oldIndex + ' ' + event.newIndex);
          let oldIndex = event.oldIndex;
          let newIndex = event.newIndex;
          if (!isNumber(oldIndex) || !isNumber(newIndex) || oldIndex === newIndex) {
            return;
          }
          if (newIndex > oldIndex) {
            let temp = roomsIdsOrder[oldIndex];
            while (oldIndex < newIndex) {
              roomsIdsOrder[oldIndex] = roomsIdsOrder[oldIndex + 1];
              oldIndex++;
            }
            roomsIdsOrder[newIndex] = temp;
          } else {
            let temp = roomsIdsOrder[oldIndex];
            while (oldIndex > newIndex) {
              roomsIdsOrder[oldIndex] = roomsIdsOrder[oldIndex - 1];
              oldIndex--;
            }
            roomsIdsOrder[newIndex] = temp;
          }
          roomsActions.changeOrder({
            rooms: roomsIdsOrder.map((roomId, index)=> {
              return {
                order: index, room: roomId
              };
            })
          });
        }
      };
      if (Sortable) {
        Sortable.create(componentBackingInstance, options);
      }
    }
  }
}

RoomsPage.propTypes = {
  currentRoom: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
  roomsActions: PropTypes.object.isRequired,
  companiesActions: PropTypes.object.isRequired,
  project: PropTypes.object.isRequired,
  permissions: PropTypes.array.isRequired,
  companies: PropTypes.array.isRequired,
  success: PropTypes.bool.isRequired,
  rooms: PropTypes.array.isRequired
};

function select(state) {
  return {
    success: state.rooms.success,
    rooms: state.rooms.rooms,
    currentRoom: state.rooms.currentRoom,
    currentUser: state.users.currentUser,
    companies: state.companies.companies,
    permissions: state.rooms.permissions
  };
}

function mapDispatchToProps(dispatch) {
  return {
    roomsActions: bindActionCreators(roomsSimpleActions, dispatch),
    companiesActions: bindActionCreators(companiesSimpleActions, dispatch)
  };
}

export default connect(select, mapDispatchToProps)(RoomsPage);

