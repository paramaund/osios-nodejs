import React, {PropTypes, Component} from 'react';
import {Row, Col, Image, Tooltip, OverlayTrigger} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {browserHistory} from 'react-router';
import defaultAvatar from '../assets/img/default-user.png';
import defaultIcon from '../assets/img/thumbnail.png';
import targetIcon from '../assets/img/navbar_inner/menu-brief.png';
import chatIcon from '../assets/img/navbar_inner/menu-chat.png';
import last from 'lodash/last';
import uniqBy from 'lodash/uniqBy';

class ProjectSideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navItems: [
        {
          title: 'Scope',
          pathname: '/scope',
          isActive: false,
          icon: targetIcon
        },
        {
          title: 'Chat',
          pathname: '/rooms',
          isActive: false,
          icon: chatIcon
        }
      ]
    };
    this.configureNavBarAnimates(props.location);
  }

  componentWillMount() {

  }

  componentWillReceiveProps(nextProps) {
    this.configureNavBarAnimates(nextProps.location);
  }

  componentWillUpdate(nextProps, nextState) {

  }

  onProjectSelectChange(event, index, value) {
    browserHistory.push('/projects/' + value);
  }

  render() {
    const {project, projects} = this.props;
    let projectIcon = defaultIcon;
    if (project.icon) {
      projectIcon = project.icon;
    }
    return (
      <Row style={{display: 'flex'}}>
        <Col xs={8}>
          <Row>
            <Col xs={12}>
              <Image src={projectIcon} responsive thumbnail/>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <SelectField
                fullWidth
                autoWidth
                style={{overflow: 'hidden'}}
                value={project._id}
                onChange={this.onProjectSelectChange.bind(this)}>
                {projects.map((project, index)=>
                  <MenuItem
                    key={index}
                    value={project._id}
                    primaryText={last(project.scopes).name}/>
                )}
              </SelectField>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <h2 style={{fontWeight: 300}}>Notified</h2>
            </Col>
            <Col xs={12}>
              {uniqBy(project.users, 'user._id').map((user, index)=> {
                let userName = `${user.user.first_name || ''}${user.user.first_name ? ' ' : ''}${user.user.last_name || ''}`;
                if (userName.trim() === '') {
                  userName = user.user.email;
                }
                return (
                  <OverlayTrigger key={index} placement="top" rootClose
                                  overlay={<Tooltip id={user._id}>{userName}</Tooltip>}>
                    <Image src={user.user.avatar || defaultAvatar} responsive circle thumbnail
                           style={{width: 45, height: 45}}/>
                  </OverlayTrigger>
                );
              })}
            </Col>
            <Col xs={12} style={{marginTop: 10}}>
              All people connected to this project will be notified of any changes to the brief.
            </Col>
          </Row>
        </Col>
        <Col xs={4} style={{display: 'flex'}}>
          <div style={{backgroundColor: '#808184', flex: 1}}>
            {this.state.navItems.map((nav, index)=> {
                let element = (
                  <Row
                    className={'center-block inner-nav-item ' + (nav.isActive ? 'active' : '')}>
                    <Image src={nav.icon} className="icon" responsive/>
                    <div className="text">{nav.title}</div>
                  </Row>
                );
                if (project._id) {
                  element = (
                    <LinkContainer
                      key={index}
                      to={{ pathname: `/projects/${project._id}${nav.pathname}` }}
                      role="button">
                      {element}
                    </LinkContainer>
                  );
                }
                return element;
              }
            )}
          </div>
        </Col>
      </Row>
    );
  }

  configureNavBarAnimates(location) {
    this.state.navItems.forEach(item=>item.isActive = false);
    let navItem = this.state.navItems.find(item=>location.pathname.indexOf(item.pathname) !== -1);
    if (navItem) {
      navItem.isActive = true;
    }
  }
}

ProjectSideBar.propTypes = {
  project: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  projects: PropTypes.array.isRequired
};

export default ProjectSideBar;
