import * as companies from '../actions/companies';

const initialState = {
  companies: []
};

export default (state = initialState, action = {}) => {
  const {payload} = action;
  switch (action.type) {
    case companies.fetch.success.getType():
      return Object.assign({}, state, {
        companies: [...payload.companies]
      });
    case companies.create.success.getType():
      return Object.assign({}, state, {
        companies: [...state.companies, payload.company]
      });
    default:
      return state;
  }
};
