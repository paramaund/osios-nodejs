import {combineReducers} from 'redux';
import auth from './auth';
import users from './users';
import projects from './projects';
import location from './location';
import paymentPlans from './paymentPlans';
import platforms from './platforms';
import devices from './devices';
import companies from './companies';
import info from './info';
import rooms from './rooms';

// Combine reducers with routeReducer which keeps track of
// router state
const rootReducer = combineReducers({
  auth,
  users,
  projects,
  location,
  paymentPlans,
  platforms,
  devices,
  companies,
  info,
  rooms
});

export default rootReducer;
