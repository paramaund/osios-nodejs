import * as devices from '../actions/devices';

const initialState = {
  devices: []
};

export default (state = initialState, action = {}) => {
  const {payload} = action;
  switch (action.type) {
    case devices.fetch.success.getType():
      return Object.assign({}, state, {
        devices: [...payload.devices]
      });
    default:
      return state;
  }
};
