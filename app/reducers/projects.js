import * as projects from '../actions/projects';
import * as auth from '../actions/auth';
import remove from 'lodash/remove';
import uniqBy from 'lodash/uniqBy';
import last from 'lodash/last';
import forOwn from 'lodash/forOwn';
import cloneDeep from 'lodash/cloneDeep';

const initialState = {
  success: false,
  total: 0,
  currentProject: {
    users: [],
    scopes: [
      {
        name: '',
        description: '',
        colors: [],
        devices: [],
        platforms: [],
        files: []
      }
    ]
  },
  projects: {
    active: {
      working: {
        own: {
          total: 0,
          projects: []
        },
        other: {
          total: 0,
          projects: []
        }
      },
      problem: {
        own: {
          total: 0,
          projects: []
        },
        other: {
          total: 0,
          projects: []
        }
      },
      'not started': {
        own: {
          total: 0,
          projects: []
        },
        other: {
          total: 0,
          projects: []
        }
      }
    },
    completed: {
      own: {
        total: 0,
        projects: []
      },
      other: {
        total: 0,
        projects: []
      }
    },
    deleted: {
      own: {
        total: 0,
        projects: []
      },
      other: {
        total: 0,
        projects: []
      }
    }
  },
  activeStatistics: {
    newest: {},
    oldest: {}
  },
  completedStatistics: {
    newest: {},
    oldest: {}
  },
  deletedStatistics: {
    newest: {},
    oldest: {}
  },
  statuses: []
};

function getOldProjectOrNull(val, newProj) {
  let project = val.own.projects.find(proj=>proj._id === newProj._id);
  if (project) {
    if (project.status._id !== newProj.status._id) {
      remove(val.own.projects, project);
      val.own.total--;
      return project;
    } else {
      let index = val.own.projects.indexOf(project);
      val.own.projects[index] = Object.assign({}, newProj);
      return null;
    }
  }
  return null;
}

export default (state = initialState, action = {}) => {
  const {payload} = action;
  switch (action.type) {
    case auth.loginSuccess.getType():
      return cloneDeep(initialState);

    case projects.currentProjectFieldChange.getType():
      return Object.assign({}, state, {
        currentProject: Object.assign({}, state.currentProject, payload)
      });
    case projects.currentProjectScopeFieldChange.getType():
    {
      let scopes = [...state.currentProject.scopes];
      let scope = Object.assign({}, last(scopes), payload);
      scopes.pop();
      scopes.push(scope);
      return Object.assign({}, state, {
        currentProject: Object.assign({}, state.currentProject, {
          scopes: [...scopes]
        })
      });
    }
    case projects.create.request.getType():
      return Object.assign({}, state, {
        success: false
      });
    case projects.create.success.getType():
      return Object.assign({}, state, {
        success: true,
        currentProject: Object.assign({}, initialState.currentProject)
      });

    case projects.fetch.success.getType():
    {
      let projectsChanges = cloneDeep(state.projects);
      payload.forEach(elem=> {
        let data = projectsChanges;
        if (elem.status !== 'completed' && elem.status !== 'deleted') {
          data = projectsChanges.active;
        }
        data[elem.status][elem.isOwner ? 'own' : 'other'].projects = uniqBy([...data[elem.status][elem.isOwner ? 'own' : 'other'].projects, ...elem.projects], '_id');
        data[elem.status][elem.isOwner ? 'own' : 'other'].total = elem.totalCount;
      });
      return Object.assign({}, state, {
        projects: projectsChanges
      });
    }
    case projects.fetchStatuses.success.getType():
      return Object.assign({}, state, {
        statuses: [...payload.statuses]
      });

    case projects.fetchStatistics.success.getType():
      return Object.assign({}, state, {
        total: payload.total,
        [payload.status + 'Statistics']: Object.assign({}, payload.statistics)
      });

    case projects.update.request.getType():
      return Object.assign({}, state, {
        success: false
      });
    case projects.update.success.getType():
    {
      let data = cloneDeep(state.projects);
      let oldProject;
      forOwn(data, (value, key)=> {
        if (key !== 'completed' && key !== 'deleted') {
          forOwn(value, val=> {
            if (oldProject) {
              return;
            }
            oldProject = getOldProjectOrNull(val, payload.project);
          });
        } else {
          if (oldProject) {
            return;
          }
          oldProject = getOldProjectOrNull(value, payload.project);
        }
      });

      if (oldProject) {
        let result = data;
        let status = payload.project.status.name;
        if (['working', 'problem', 'not started'].some(elem=>elem === status)) {
          result = result.active;
        }
        result[status].own.projects.push(payload.project);
        result[status].own.total++;
      }

      return Object.assign({}, state, {
        projects: data,
        success: true
      });
    }

    case projects.fetchOne.request.getType():
      return Object.assign({}, state, {
        currentProject: Object.assign({}, initialState.currentProject)
      });
    case projects.fetchOne.success.getType():
      return Object.assign({}, state, {
        currentProject: Object.assign({}, payload.project)
      });

    case projects.currentProjectFileUpload.success.getType():
    {
      let scopes = [...state.currentProject.scopes];
      let scope = last(scopes);
      scope.files = [...scope.files, ...payload.files];
      return Object.assign({}, state, {
        currentProject: Object.assign({}, state.currentProject, {scopes: scopes})
      });
    }
    default:
      return state;
  }
};
