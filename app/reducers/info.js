import {ISBROWSER} from '../constants/index';
import * as projects from '../actions/projects';

const initialState = {
  isWaiting: false,
  requestsInProcess: 0,
  message: ''
};

export default (state = initialState, action = {}) => {
  const {payload={}} = action;
  const avoidRequestsInProcess =
    action.type === projects.create.request.getType() ||
    // action.type === projects.update.request.getType() ||
    action.type === projects.create.success.getType() ||
    // action.type === projects.update.success.getType() ||
    action.type === projects.create.failure.getType();
    // action.type === projects.update.failure.getType();
  switch (true) {
    case action.type === 'RESET_MESSAGE':
      return Object.assign({}, state, {
        message: ''
      });
    case action.type.endsWith('REQUEST'):
      return Object.assign({}, state, {
        isWaiting: true,
        requestsInProcess: ISBROWSER && !avoidRequestsInProcess ? state.requestsInProcess + 1 : 0,
        message: ''
      });
    case action.type.endsWith('SUCCESS') && action.type !== 'CONFIRM_SUCCESS':
      return Object.assign({}, state, {
        isWaiting: false,
        requestsInProcess: ISBROWSER && !avoidRequestsInProcess ? state.requestsInProcess - 1 : 0,
        message: payload.message || ''
      });
    case action.type.endsWith('FAILURE'):
      return Object.assign({}, state, {
        isWaiting: false,
        requestsInProcess: ISBROWSER && !avoidRequestsInProcess ? state.requestsInProcess - 1 : 0,
        message: payload.message || ''
      });

    default:
      return state;
  }
};
