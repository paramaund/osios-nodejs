import * as rooms from '../actions/rooms';

const initialState = {
  currentRoom: {},
  rooms: [],
  permissions: [],
  success: false
};

export default (state = initialState, action = {}) => {
  const {payload} = action;
  switch (action.type) {
    case rooms.confirmSuccess.getType():
      return Object.assign({}, state, {
        success: false
      });
    case rooms.create.request.getType():
      return Object.assign({}, state, {
        success: false
      });
    case rooms.create.success.getType():
      return Object.assign({}, state, {
        success: true,
        rooms: [...state.rooms, payload.room]
      });
    case rooms.fetch.success.getType():
      return Object.assign({}, state, {
        rooms: [...payload.rooms]
      });
    case rooms.fetchPermissions.success.getType():
      return Object.assign({}, state, {
        permissions: [...payload.permissions]
      });
    case rooms.remove.success.getType():
      return Object.assign({}, state, {
        rooms: state.rooms.filter(room=>room._id !== payload.id)
      });
    case rooms.fetchOne.success.getType():
      return Object.assign({}, state, {
        currentRoom: Object.assign({}, payload.room)
      });
    default:
      return state;
  }
};
