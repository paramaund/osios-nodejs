import * as platforms from '../actions/platforms';

const initialState = {
  platforms: []
};

export default (state = initialState, action = {}) => {
  const {payload} = action;
  switch (action.type) {
    case platforms.fetch.success.getType():
      return Object.assign({}, state, {
        platforms: [...payload.platforms]
      });
    default:
      return state;
  }
};
