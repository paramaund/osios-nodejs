import * as auth from '../actions/auth';

const initialState = {
  authenticated: false
};

export default (state = initialState, action = {})=> {
  const {payload} = action;
  switch (action.type) {
    case auth.loginSuccess.getType():
      return Object.assign({}, state, {
        authenticated: true
      });
    case auth.loginFailure.getType():
      return Object.assign({}, state, {
        authenticated: false
      });
    case auth.logoutSuccess.getType():
      return Object.assign({}, state, {
        authenticated: false
      });
    case auth.logoutFailure.getType():
      return Object.assign({}, state, {
        authenticated: true
      });
    default:
      return state;
  }
};
