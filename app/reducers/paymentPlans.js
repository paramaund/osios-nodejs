import * as paymentPlans from '../actions/paymentPlans';

const initialState = {
  plans: []
};

export default (state = initialState, action = {}) => {
  const {payload} = action;
  switch (action.type) {
    case paymentPlans.fetch.success.getType():
      return Object.assign({}, state, {
        plans: [...payload.plans]
      });
    default:
      return state;
  }
};
