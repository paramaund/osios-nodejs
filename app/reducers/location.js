const initialState = {
  pathname: ''
};

export default (state = initialState, action = {}) => {
  const {payload} = action;
  switch (action.type) {
    case '@@router/UPDATE_LOCATION':
      return Object.assign({}, state, {
        pathname: payload.pathname
      });
    default:
      return state;
  }
};
