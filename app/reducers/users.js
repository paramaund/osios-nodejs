import * as users from '../actions/users';
import * as subscribe from '../actions/subscribe';

const initialState = {
  // rendering on server
};

export default (state = initialState, action = {}) => {
  const {payload} = action;
  switch (action.type) {
    case users.confirmSuccess.getType():
      return Object.assign({}, state, {
        success: false
      });
    case users.createSuccess.getType():
      return Object.assign({}, state, {
        success: true,
        allUsers: [...state.allUsers, payload.user]
      });
    case users.createFailure.getType():
      return Object.assign({}, state, {
        success: false
      });

    case users.fetchMeInfo.success.getType():
      return Object.assign({}, state, {
        currentUser: Object.assign({}, payload.user)
      });

    case users.updateMe.request.getType():
      return Object.assign({}, state, {
        currentUser: Object.assign({}, initialState.currentUser)
      });
    case users.updateMe.success.getType():
      return Object.assign({}, state, {
        currentUser: Object.assign({}, state.currentUser, payload.user)
      });

    case subscribe.create.success.getType():
      return Object.assign({}, state, {
        currentUser: Object.assign({}, state.currentUser, payload.user)
      });

    case users.getStatisticsMe.success.getType():
      return Object.assign({}, state, {
        statistics: Object.assign({}, state.statistics, payload.statistics)
      });

    case users.fetch.success.getType():
      return Object.assign({}, state, {
        allUsers: [...payload.allUsers]
      });
    
    default:
      return state;
  }
};
