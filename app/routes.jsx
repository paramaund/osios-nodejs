import React from 'react';
import {Route, IndexRedirect} from 'react-router';
import async from 'async';
import {makeRequest} from './api/server';
import Dashboard from './containers/Dashboard';
import App from './containers/App';
import LoginPage from './containers/LoginPage';
import SignUpPage from './containers/SignUpPage';
import AccountPage from './containers/AccountPage';
import ProjectPage from './containers/ProjectPage';
import ScopePage from './containers/ScopePage';
import RoomsPage from './containers/RoomsPage';
import RoomPage from './containers/RoomPage';
import ProjectsPage from './containers/ProjectsPage';
import CreateOrUpdateProjectPage from './containers/CreateOrUpdateProjectPage';

/*
 * @param {Redux Store}
 * We require store as an argument here because we wish to get
 * state from the store after it has been authenticated.
 */
export default (store, req) => {
  const requireAuth = (nextState, replace, callback) => {
    const {auth: {authenticated}} = store.getState();
    if (!authenticated) {
      replace({
        pathname: '/login',
        state: {nextPathname: nextState.location.pathname}
      });
    }
    callback();
  };

  const redirectAuth = (nextState, replace, callback) => {
    const {auth: {authenticated}} = store.getState();
    if (authenticated) {
      replace({
        pathname: '/projects'
      });
    }
    callback();
  };

  const requireAccountLimits = (nextState, replace, callback) => {
    if (req) {
      const user = req.user;
      if (user.payment_plan.projects && user.payment_plan.projects <= user.totalProjects) {
        replace({
          pathname: '/projects'
        });
      }
    }
    callback();
  };

  return (
    <Route component="div">
      <Route path="/" component={App} onEnter={requireAuth}>
        <IndexRedirect to="projects"/>
        <Route path="projects" component={ProjectsPage}/>
        <Route path="projects/create" component={CreateOrUpdateProjectPage} onEnter={requireAccountLimits}/>
        <Route path="projects/:id/edit" component={CreateOrUpdateProjectPage}/>
        <Route path="projects/:id" component={ProjectPage}>
          <IndexRedirect to="rooms"/>
          <Route path="scope" component={ScopePage}/>
          <Route path="rooms" component={RoomsPage}/>
          <Route path="rooms/:roomId" component={RoomPage}/>
          <Route path="design" component={Dashboard}/>
          <Route path="track" component={Dashboard}/>
        </Route>
        <Route path="account" component={AccountPage}/>
      </Route>
      <Route path="/" component={App}>
        <Route path="login" component={LoginPage} onEnter={redirectAuth}/>
        <Route path="pricing" component={SignUpPage} onEnter={redirectAuth}/>
      </Route>
      <Route path="admin" component={Dashboard}>
        <IndexRedirect to="dashboard"/>
        <Route path="dashboard" component={Dashboard}/>
      </Route>
    </Route>
  );
};

