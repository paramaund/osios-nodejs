const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');
let hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';

let assetsPath = path.join(__dirname, '..', 'public', 'assets');
let publicPath = '/assets/';

let commonLoaders = [
  {
    /*
     * TC39 categorises proposals for babel in 4 stages
     * Read more http://babeljs.io/docs/usage/experimental/
     */
    test: /\.js$|\.jsx$/,
    loaders: ['babel'],
    include: path.join(__dirname, '..', 'app'),
    exclude: /node_modules/
  },
  {test: /\.json$/, loader: 'json-loader'},
  {test: /\.(eot|woff|woff2|ttf|svg|png|gif)([\?]?.*)$/, loader: 'url-loader'},
  {test: /\.jpg$/, loader: 'file-loader'},
  {
    test: /\.scss$/,
    loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass?includePaths[]='
      + encodeURIComponent(path.resolve(__dirname, '..', 'app', 'assets', 'styles')))
  }
];

module.exports = [
  {
    // The configuration for the client
    name: 'browser',
    // A SourceMap is emitted.
    devtool: 'source-map',
    context: path.join(__dirname, '..', 'app'),
    entry: {
      app: ['babel-polyfill', './client', hotMiddlewareScript]
    },
    output: {
      // The output directory as absolute path
      path: assetsPath,
      // The filename of the entry chunk as relative path inside the output.path directory
      filename: '[name].js',
      // The output path from the view of the Javascript
      publicPath: publicPath

    },

    module: {
      preLoaders: [{
        test: /\.js$|\.jsx$/,
        exclude: /node_modules/,
        loaders: ['eslint']
      }],
      loaders: commonLoaders
    },
    resolve: {
      extensions: ['', '.js', '.jsx', '.scss'],
      modulesDirectories: [
        'app', 'node_modules'
      ]
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      // Order the modules and chunks by occurrence.
      // This saves space, because often referenced modules
      // and chunks get smaller ids.
      new webpack.optimize.OccurenceOrderPlugin(),
      // extract inline css from modules into separate files
      new ExtractTextPlugin('styles/main.css'),
      new webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false
        }
      }),
      new webpack.DefinePlugin({
        __TEST__: JSON.stringify(JSON.parse(process.env.TEST_ENV || 'false')),
        __DEV__: false,
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      })
    ]
  }, {
    // The configuration for the server-side rendering
    name: 'server-side rendering',
    context: path.join(__dirname, '..', 'app'),
    entry: {
      app: './server'
    },
    target: 'node',
    output: {
      // The output directory as absolute path
      path: assetsPath,
      // The filename of the entry chunk as relative path inside the output.path directory
      filename: '[name].server.js',
      // The output path from the view of the Javascript
      publicPath: publicPath,
      libraryTarget: 'commonjs2'
    },
    module: {
      loaders: commonLoaders
    },
    resolve: {
      extensions: ['', '.js', '.jsx', '.scss'],
      modulesDirectories: [
        'app', 'node_modules'
      ]
    },
    plugins: [
      // Order the modules and chunks by occurrence.
      // This saves space, because often referenced modules
      // and chunks get smaller ids.
      new webpack.optimize.OccurenceOrderPlugin(),
      // extract inline css from modules into separate files
      new ExtractTextPlugin('styles/main.css'),
      new webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false
        }
      }),
      new webpack.DefinePlugin({
        __TEST__: JSON.stringify(JSON.parse(process.env.TEST_ENV || 'false')),
        __DEV__: false,
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      })
    ]
  }
];
