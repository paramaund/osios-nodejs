const path = require('path');
const webpack = require('webpack');
let assetsPath = path.join(__dirname, '..', 'public', 'assets');
let hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';

let commonLoaders = [
  {
    /*
     * TC39 categorises proposals for babel in 4 stages
     * Read more http://babeljs.io/docs/usage/experimental/
     */
    test: /\.js$|\.jsx$/,
    loaders: ['babel'],
    include: path.join(__dirname, '..', 'app'),
    exclude: /node_modules/
  },
  {test: /\.(eot|woff|woff2|ttf|svg|png|gif)([\?]?.*)$/, loader: 'url-loader'},
  {test: /\.jpg$/, loader: 'file-loader'},
  {test: /\.json/, loader: 'json-loader'},
  {test: /\.html$/, loader: 'html-loader'}
];

module.exports = {
  // eval - Each module is executed with eval and //@ sourceURL.
  devtool: 'eval',
  // The configuration for the client
  name: 'browser',
  context: path.join(__dirname, '..', 'app'),
  // Multiple entry with hot loader
  // https://github.com/glenjamin/webpack-hot-middleware/blob/master/example/webpack.config.multientry.js
  entry: {
    app: ['babel-polyfill', './client', hotMiddlewareScript]
  },
  output: {
    // The output directory as absolute path
    path: assetsPath,
    // The filename of the entry chunk as relative path inside the output.path directory
    filename: '[name].js',
    // The output path from the view of the Javascript
    publicPath: '/assets/'
  },
  module: {
    loaders: commonLoaders.concat([
      {
        test: /\.scss$/,
        loader: 'style!css' +
        '!sass?sourceMap&outputStyle=expanded' +
        '&includePaths[]=' + encodeURIComponent(path.resolve(__dirname, '..', 'app', 'assets', 'styles'))
      }
    ])
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.scss'],
    modulesDirectories: [
      'app', 'node_modules'
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      __TEST__: JSON.stringify(JSON.parse(process.env.TEST_ENV || 'false')),
      __DEV__: true
    })
  ]
};
